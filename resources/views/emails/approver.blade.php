<table align="center" width="600" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;text-align:left;vertical-align:top;padding:0">
    <tbody>
    <tr>
        <td>
            Dear {{$user->name}},
        </td>
    </tr>
    <tr>
        <td>Your approval is requested in the following link. On the linked page, you will see relevant details as well as options to approve or decline it</td>
    </tr>
    <tr>
        <td><a href="{{route('order-request.approval',['id' => $id])}}">Approve / Decline</a></td>
    </tr>
    </tbody>
</table>
