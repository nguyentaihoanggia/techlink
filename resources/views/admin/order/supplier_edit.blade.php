@extends('admin.layouts.app')
@section('content')
<style>
.progressbar-wrapper {
    background: transparent;
    width: 100%;
    padding-top: 10px;
    padding-bottom: 5px;
}

.progressbar li {
    list-style-type: none;
    width: 50%;
    float: left;
    font-size: 15px;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    color: #7d7d7d;
}
.progressbar li.active {
    color: #404040;
    font-weight: bold;  
}
.progressbar li:before {
    width: 50px;
    height: 50px;
    content: "";
    line-height: 46px;
    border: 3px solid #7d7d7d;
    display: block;
    text-align: center;
    margin: 0 auto 3px auto;
    border-radius: 50%;
    position: relative;
    z-index: 2;
    background-color: #fff;
}
.progressbar li:after {
    width: 100%;
    height: 3px;
    content: '';
    position: absolute;
    background-color: #7d7d7d;
    top: 25px;
    left: -50%;
    z-index: 0;
}
.progressbar li:first-child:after {
    content: none;
}
.progressbar li.active:before {
    border-color: #55b776;
    background: green;
}
.progressbar li.active + li:after {
    background-color: #55b776;
}
.progressbar li.active:before {
    background: #7ff5a6  url(user.svg) no-repeat center center;
    background-size: 60%;
}
.progressbar li::before {
    background: #fff url(user.svg) no-repeat center center;
    background-size: 60%;
}
.progressbar {
    counter-reset: step;
}
.progressbar li:before {
    content: counter(step);
    counter-increment: step; 
    font-size: 17px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Nhà cung cấp
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
            <li><a href="#">Order</a></li>
            <li class="active">Detail</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
        <!-- <div class="row">
            <div class="col-xs-12">
                <a class="btn btn-success pull-right" @click="submit()" ><i class="fa fa-check"></i> Create</a>
            </div>
        </div> -->
        <div class="row" style="margin-top: 10px">
            <div class="col-xs-12" >
                <div class="container">
                    <div class="progressbar-wrapper">
                          <ul class="progressbar">
                              <li :class="{active : step == 1}">Step 1</li>
                              <li :class="{active : step == 2}">Step 2</li>
                          </ul>
                    </div>
                </div>
            </div>
            <template v-if="step == 1">
                <div class="col-xs-12"  style="margin-top: 20px">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <section class="box box-order" style="min-height: 200px;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Thông tin đơn hàng</h3>
                                </div>
                                <div class="box-body" >
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th></th>
                                                <th>Mã sản phẩm</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Số lượng</th>
                                                <th>Đơn vị</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in details">
                                                <td>@{{ index+1 }}</td>
                                                <td>
                                                    <div class="box-item-image">
                                                        <img v-if="!item.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                        <img v-else :src="item.path" width="100%" alt="">
                                                    </div>
                                                </td>
                                                <td>@{{ item.product_id }}</td>
                                                <td>@{{ item.product_name }}</td>
                                                <td>@{{ item.quantity }}</td>
                                                <td>@{{ item.unit }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                        <div class="col-xs-12 col-md-6" >
                            <section class="box box-order" style="min-height: 200px;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Chọn nhà cung cấp </h3>
                                </div>
                                <div class="box-body" >
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label for="">Người kiểm duyệt</label>
                                            <select2 :options="users" style="width: 100%"  class="form-control" :search="true" v-model="approver" placeholder="Chọn nhân viên">
                                            </select2>
                                        </div>
                                    </div>
                                    <div class="row " style="margin-top: 20px">
                                        <div class="col-xs-12">
                                            <a class="btn-sm btn-success pull-right" @click="openModalSuplier"><i class="fa fa-plus"></i> Thêm nhà cung cấp</a>
                                        </div>
                                        <div class="col-xs-12 mt-10">
                                            
                                            <table class="table table-hover table-bordered ">
                                                <thead>
                                                    <tr>
                                                        <th>STT</th>
                                                        <th>Mã nhà cung cấp</th>
                                                        <th>Tên nhà cung cấp</th>
                                                        <th>SĐT</th>
                                                        <th>Đia chỉ</th>
                                                        <th>Fax</th>
                                                        <th>Email</th>
                                                        <th>Người liên hệ</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <template v-if="suppliers">
                                                        <tr v-for="item, index in suppliers">
                                                            <td>@{{ index + 1 }}</td>
                                                            <td>@{{ item.id }}</td>
                                                            <td>@{{ item.supplier_name }}</td>
                                                            <td>@{{ item.phone1 }}</td>
                                                            <td>@{{ item.address1 }}</td>
                                                            <td>@{{ item.fax }}</td>
                                                            <td>@{{ item.email }}</td>
                                                            <td>@{{ item.contact_person }}</td>
                                                            <td>
                                                                <a class="text-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </template>
                                                    <template v-else>
                                                        <tr>
                                                            <td colspan="9" class="text-center">KHÔNG CÓ DỮ LIỆU</td>
                                                        </tr>
                                                    </template>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success" @click="next">Next <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </template>
            <template v-if="step == 2">
                <div class="col-xs-12"  style="margin-top: 10px">
                    <div class="row" style="margin-top: 10px" v-for="item,index in suppliers">
                        <div class="col-xs-12"  >
                            <section class="box box-order" style="min-height: 200px;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Nhà cung cấp @{{index + 1}}: <b>@{{ item.supplier_name }}</b>  </h3>
                                </div>
                                <div class="box-body" >
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th></th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Ngày hẹn giao</th>
                                                    <th>Số lượng</th>
                                                    <th>Ngày bộ phận cần có</th>
                                                    <th>Đơn giá</th>
                                                    <th>Đơn vị</th>
                                                    <th>Ngày cần sử dụng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="detail,i_d in item.details">
                                                    <td>@{{ i_d + 1 }}</td>
                                                    <td>
                                                        <div class="box-item-image">
                                                            <img v-if="!detail.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                            <img v-else :src="detail.path" width="100%" alt="">
                                                        </div>
                                                    </td>
                                                    <td>@{{ detail.product_id }}</td>
                                                    <td>@{{ detail.product_name }}</td>
                                                    <td>
                                                        <input type="date" class="form-control" v-model="detail.appointment_date" >
                                                    </td>
                                                    <td>@{{ detail.erp.quantity | money }}</td>
                                                    <td>
                                                        <input type="date" class="form-control" v-model="detail.required_date" >
                                                    </td>
                                                    <td style="width: 200px">
                                                        <money class="form-control" v-model="detail.price"></money>
                                                    </td>
                                                    <td>@{{ detail.erp.unit }}</td>
                                                    <td>
                                                        @{{ getDate(detail.date) }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-default" @click="back"><i class="fa fa-angle-double-left"></i> Quay lại </a>
                            <a class="btn btn-success" @click="submit">Chỉnh sửa <i class="fa fa-check"></i></a>
                        </div>
                    </div>
                </div>
            </template>
        </div>
        <div class="modal fade" id="modal-supplier" style="display: none;">
            <table-supplier v-model="suppliers"></table-supplier>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('resources/js/table-supplier.js')}}"></script>
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                step: 1,
                request : <?php echo json_encode($order_request); ?>,
                users : <?php echo json_encode($users); ?>,
                suppliers: <?php echo json_encode($suppliers) ?>,
                details : <?php echo json_encode($details); ?>,
                approver: <?php echo json_encode($approver) ?>,
                tab: null
            },
            methods:{
                getDate: function(value){
                    if(value){
                        var date = value.substr(-2);
                        var month = value.substr(4,2);
                        var year = value.substr(0,4);
                        return date+'/'+month+'/'+year;
                    }
                    return '';
                },
                openModalSuplier: function(){
                    $("#modal-supplier").modal('show');
                },
                next() {
                    if(this.suppliers.length === 0){
                        helper.showNotification('Chưa chọn nhà cung cấp', 'danger');
                        return;
                    }
                    if(this.approver == ''){
                        helper.showNotification('Chưa chọn người phê duyệt', 'danger');
                        return;
                    }
                    var details = this.details;
                    this.suppliers.map(function(item){
                        if(typeof item.details == 'undefined'){
                            item.details = []
                            details.forEach(function(detail){
                                item.details.push({
                                    detail_id: detail.id,
                                    product_id: detail.product_id,
                                    path: detail.path,
                                    product_name: detail.product_name,
                                    price: 0,
                                    quantity: detail.quantity,
                                    unit: detail.unit,
                                    appointment_date: '',
                                    required_date: detail.required_date
                                })
                            })    
                        }
                        return item
                    })
                    this.step = 2;
                },
                back() {
                    this.step = 1;
                },
                submit: function(){
                    var vm = this;
                    $.post('/admin/order-request/'+vm.request.id+'/supplier/edit', {
                        suppliers: vm.suppliers,
                        approver: vm.approver
                    } ,function(res){
                        if(res.success){
                            location.href = '/admin/notify'
                        }else{
                            var message = res.message ? res.message : 'Something went wrong'
                            helper.showNotification(message,'danger')
                        }
                    })  
                },
                removeSupplier: function(index){
                    r = confirm('Xóa nhà cung cấp này')
                    if(!r) return
                    this.suppliers.splice(index,1);
                }
                // total_quantity: function(details){
                //     var total = 0;
                //     details.forEach(function(item){
                //         total += item.quantity;
                //     })
                //     return total;
                // },
                // total_money: function(details){
                //     var total = 0;
                //     details.forEach(function(item){
                //         total += (item.price*item.quantity);
                //     })
                //     return total;
                // },
            },
            computed:{
                // data_approvers: function(){
                //     var vm = this;
                //     return this.users.filter(function(item){
                //         return vm.approvers.indexOf(String(item.id)) >=0;
                //     })
                // },
            },
            mounted(){
                this.suppliers.map(function(supplier){
                    supplier.details.map(function(item){
                        item.path = item.request_detail.path;
                        item.detail_id = item.order_request_detail_id;
                        item.product_id = item.request_detail.product_id;
                        item.product_name = item.request_detail.product_name;
                        item.original_price = item.request_detail.price;
                        item.quantity = item.request_detail.quantity;
                        return item;
                    })
                    return supplier;
                })
            }   
        })
    </script>
@endsection

