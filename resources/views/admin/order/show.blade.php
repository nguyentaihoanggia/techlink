@extends('admin.layouts.app')
@section('content')
<style>
    .time-label>span{
        min-width: 80px;
        text-align: center;
    }
    .timeline-edit-button{
        background: transparent !important; 
        box-shadow: none !important;
        border: 0;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Order</a></li>
            <li class="active">Detail</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
        <div class="row">
            <div class="col-md-12">
                <ul class="timeline">
                    <li class="time-label">
                        <span class="bg-gray">
                        Created 
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-plus bg-blue"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> @{{ request.created_at | dd-mm-yyyy }}</span>
                            <h3 class="timeline-header"><a href="#">@{{ request.user.name }}</a> </h3>
                            <div class="timeline-body">
                                Tạo mới phiếu YCMH: <strong>@{{ request.code }}</strong>
                            </div>
                        </div>
                    </li>
                    <li>
                        <i class="fa fa-edit bg-blue"></i>
                        <div class="timeline-item timeline-edit-button">
                            <button class="btn btn-primary" v-if="request.status == 'returned1'" type="button" @click="goToEdit('request')">
                                <i class="fa fa-edit"></i> Chỉnh sửa
                            </button>
                            <button class="btn btn-default" v-else type="button" @click="goToDetail('request')">
                                <i class="fa fa-info"></i> Chi tiết
                            </button>
                        </div>
                        
                    </li>
                    <li class="time-label">
                        <span class="bg-gray">
                        Approval 1
                        </span>
                    </li>
                    <template v-if="request.status == 'created'">
                        <template v-for="item in request.approver1">
                            <li>
                                <i class="fa" :class="classAprrover(item.status)"></i>
                                <div class="timeline-item">
                                    <span class="time" v-if="item.status != 'created'"><i class="fa fa-clock-o"></i> @{{ item.updated_at | dd-mm-yyyy }}</span>
                                    <h3 class="timeline-header"><a href="#">@{{ item.user_name }}</a> </h3>
                                    <div class="timeline-body">
                                        <span v-if="item.status == 'created'">
                                            <span v-if="item.user_id == user.id">
                                                <a class="btn btn-primary" :href="'/admin/order-request/'+request.id+'/approval?show=1'">Duyệt</a>
                                            </span>
                                            <span v-else>Đang chờ duyệt</span>
                                        </span>
                                        <span v-else-if="item.status == 'approved'">Đã duyệt</span>
                                        <span v-else>Từ chối</span>
                                        <div v-if="item.status != 'created'"><small><b><u>Ghi chú:</u></b> @{{ item.reason }}</small></div>
                                    </div>
                                </div>
                            </li>
                        </template>
                    </template>
                    <template v-else>
                        <template v-for="item in request.approver1">
                            <li v-if="request.user_approved === item.user_id">
                                <i class="fa" :class="classAprrover(item.status)"></i>
                                <div class="timeline-item">
                                    <span class="time" v-if="item.status != 'created'"><i class="fa fa-clock-o"></i> @{{ item.updated_at | dd-mm-yyyy }}</span>
                                    <h3 class="timeline-header"><a href="#">@{{ item.user_name }}</a> </h3>
                                    <div class="timeline-body">
                                        <span v-if="item.status == 'created'">Đang chờ duyệt</span>
                                        <span v-else-if="item.status == 'approved'">Đã duyệt</span>
                                        <span v-else>Từ chối</span>
                                        <div v-if="item.status != 'created'"><small><b><u>Ghi chú:</u></b> @{{ item.reason }}</small></div>
                                    </div>
                                </div>
                            </li>
                        </template>
                    </template>
                    <template v-if="checkStepSupplier">
                        <li class="time-label">
                            <span class="bg-gray">
                            Supplier
                            </span>
                        </li>
                        <template v-if="request.suppliers.length == 0">
                            <li>
                                <i class="fa fa-info bg-orange"></i>
                                <div class="timeline-item">
                                    <h4 class="timeline-header"><a>Chọn nhà cung ứng</a> </h4>
                                    <div class="timeline-body">
                                        <div style="margin-bottom: 10px">Đang chờ thu mua tìm nhà cung cấp</div>
                                        <template v-for="notify in request.notify">
                                            <div v-if="notify.user_id == user.id">
                                                <a :href="'/admin/order-request/'+request.id+'/supplier?show=1'" class="btn btn-primary">Chọn NCC</a>
                                            </div>
                                        </template>
                                        
                                        <!-- <button class="btn btn-success" :disabled="request.status == 'reject'" @click="goTo('{{ route('order-request.supplier',['id' => $request->id ]) }}')" ><i class="fa fa-plus"></i> Add</button> -->
                                    </div>
                                </div>
                            </li>
                        </template>
                        <template v-esle>
                            <li v-for="item,index in request.suppliers">
                                <i class="fa fa-plus bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> @{{ item.created_at | dd-mm-yyyy }}</span>
                                    <h3 class="timeline-header"><a href="#">@{{ request.user.name }}</a> </h3>
                                    <div class="timeline-body">
                                        Chọn nhà cung cấp: <strong>@{{ item.supplier_code }}</strong>
                                    </div>
                                </div>
                            </li>
                            <li v-if="request.suppliers.length" >
                                <i class="fa fa-edit bg-blue"></i>
                                <div class="timeline-item" :class="{'timeline-edit-button' : request.status != 'returned2'}">
                                    <!-- <button class="btn btn-primary" v-if="request.status == 'returned2'" type="button" @click="goToEdit('supplier')">
                                        <i class="fa fa-edit"></i> Chỉnh sửa
                                    </button> -->
                                    <div class="timeline-body" v-if="request.status == 'returned2'">
                                        <div  >
                                            Phiếu chọn nhà cung cấp bị trả lại, đang chờ thu mua chỉnh sửa lại
                                            <div class="mt-10">
                                                <button class="btn btn-default"  type="button" @click="goToDetail('supplier')">
                                                    <i class="fa fa-info"></i> Chi tiết
                                                </button>
                                                <template v-for="notify in request.notify">
                                                    <span v-if="notify.user_id == user.id">
                                                        <button  class="btn btn-primary" v-if="request.status == 'returned2'" type="button" @click="goToEdit('supplier')">
                                                            <i class="fa fa-edit"></i> Chỉnh sửa
                                                        </button>
                                                    </span>
                                                </template>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <button v-else class="btn btn-default"  type="button" @click="goToDetail('supplier')">
                                        <i class="fa fa-info"></i> Chi tiết
                                    </button>
                                </div>
                            </li>
                        </template>
                        <template v-if="request.approver2 != null">
                            <li class="time-label">
                                <span class="bg-gray">
                                Approval 2
                                </span>
                            </li>
                            <li>
                                <i class="fa" :class="classAprrover(request.approver2.status)"></i>
                                <div class="timeline-item">
                                    <span class="time" v-if="request.approver2.status != 'created'"><i class="fa fa-clock-o"></i> @{{ request.approver2.updated_at | dd-mm-yyyy }}</span>
                                    <h3 class="timeline-header"><a href="#">@{{ request.approver2.user_name }}</a> </h3>
                                    <div class="timeline-body">
                                        <span v-if="request.approver2.status == 'created'">
                                            <template v-if="request.approver2.user_id == user.id">
                                                <a :href="'/admin/order-request/'+request.id+'/approval-supplier?show=1'" class="btn btn-primary">Duyệt</a>
                                            </template>
                                            <template v-else>
                                                Đang chờ duyệt
                                            </template>
                                        </span>
                                        <span v-else-if="request.approver2.status == 'approved'">Đã chọn NCC <b>@{{ supplier_selected }}</b></span>
                                        <span v-else-if="request.approver2.status == 'returned'">Returned <b>@{{ supplier_selected }}</b></span>
                                        <span v-else>Từ chối</span>
                                        <div v-if="request.approver2.status != 'created'"><small><b><u>Ghi chú:</u></b> @{{ request.approver2.reason }}</small></div>
                                    </div>
                                </div>
                            </li>  
                            <template v-if="request.approver2.status == 'approved'">
                                <template v-if="request.approver4.length">
                                    <li class="time-label">
                                        <span class="bg-gray">
                                        Accountant 
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-edit" ></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header"><a href="#">Kế toán</a> </h3>
                                            <div class="timeline-body">
                                                <span v-if="request.approver4[0].status == 'created'">
                                                    <template v-if="user.role_id == 4">
                                                        <a :href="'/admin/order-request/'+request.id+'/accountant?show=1'" class="btn btn-primary">Duyệt</a>
                                                    </template>
                                                    <template v-else>Đang chờ duyệt</template>
                                                </span>
                                                <template v-for="account in request.approver4">
                                                    <span v-if="account.status =='approved'"><b>@{{ account.user.name }} </b>đã duyệt  @{{ account.updated_at | dd-mm-yyyy }}</span>
                                                    <span v-if="account.status =='rejected'"><b>@{{ account.user.name }} </b>Từ chối  @{{ account.updated_at | dd-mm-yyyy }}</span>
                                                </template>
                                            </div>
                                        </div>
                                    </li>  
                                </template>
                                <template v-if="request.status == 'advance'">
                                    <li class="time-label">
                                        <span class="bg-gray">
                                        Phê duyệt 
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-file-text-o bg-blue" ></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">
                                                <a href="#">Chọn người phê duyệt</a> 
                                                <div style="font-size: 14px" class="mt-10">
                                                    (Dành cho những đơn hàng có tổng tiền trên <b>35,000,000</b>)
                                                </div>
                                            </h3>

                                            <div class="timeline-body">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4">
                                                        <label>Chọn người phê duyệt</label>
                                                        <select2 :options="advance_users" style="width: 100%" class="form-control"  :search="true" v-model="approvers" placeholder="Chọn nhân viên"></select2>
                                                        <a class="btn btn-primary mt-10" @click="selectAdvanceUser">Xác nhận</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>  
                                </template>
                                <template v-if="request.approver_advance">
                                    <li class="time-label">
                                        <span class="bg-gray">
                                        Phê duyệt 
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa" :class="classAprrover(request.approver_advance.status)"></i>
                                        <div class="timeline-item">
                                            <span class="time" v-if="request.approver_advance.status != 'created'"><i class="fa fa-clock-o"></i> @{{ request.approver_advance.updated_at | dd-mm-yyyy }}</span>
                                            <h3 class="timeline-header"><a href="#">@{{ request.approver_advance.user_name }}</a> </h3>
                                            <div class="timeline-body">
                                                <template v-if="request.approver_advance.status == 'created'">
                                                    <a :href="'/admin/order-request/'+request.id+'/approval-advance?show=1'" class="btn btn-primary">Duyệt</a>
                                                </template>
                                                <span v-else-if="request.approver_advance.status == 'approved'">Đã duyệt</span>
                                                <span v-else>Từ chối</span>
                                                <div v-if="request.approver_advance.status != 'created'"><small><b><u>Ghi chú:</u></b> @{{ request.approver_advance.reason }}</small></div>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- <template  >
                                        <li v-if="display(request.approver_advance)">
                                            <i class="fa" :class="classAprrover(request.approver_advance.status)"></i>
                                            <div class="timeline-item">
                                                <span class="time" v-if="request.approver_advance.status != 'created'"><i class="fa fa-clock-o"></i> @{{ request.approver_advance.updated_at | dd-mm-yyyy }}</span>
                                                <h3 class="timeline-header"><a href="#">@{{ request.approver_advance.user_name }}</a> </h3>
                                                <div class="timeline-body">
                                                    <span v-if="request.approver_advance.status == 'created'">
                                                        <template v-for="item in request.approver_advance">
                                                            <div v-if="item.user_id == user.id">
                                                                <a :href="'/admin/order-request/'+request.id+'/approval-advance?show=1'" class="btn btn-primary">Duyệt</a>
                                                            </div>
                                                        </template>
                                                    </span>
                                                    <span v-else-if="item.status == 'approved'">Đã duyệt</span>
                                                    <span v-else>Từ chối</span>
                                                    <div v-if="item.status != 'created'"><small><b><u>Ghi chú:</u></b> @{{ item.reason }}</small></div>
                                                </div>
                                            </div>
                                        </li>
                                    </template> -->
                                </template>
                            </template>
                        </template>
                        
                    </template>
                    <li class="time-label" v-if="request.status =='success'">
                        <span class="bg-green">
                        Success
                        </span>
                        
                    </li>  
                    <li class="time-label" v-if="request.status =='success'">
                        <i class="fa fa-print bg-blue"></i>
                        <div class="timeline-item timeline-edit-button">
                            <button class="btn btn-primary" type="button" @click="print">
                                In
                            </button>
                        </div>
                    </li>
                    <li class="time-label" v-if="request.status =='rejected2'">
                        <span class="bg-red">
                        Fail
                        </span>
                    </li> 
                    <li class="time-label" v-if="request.status =='rejected1'">
                        <span class="bg-red">
                        Fail
                        </span>
                    </li> 
                    <li class="time-label" v-if="request.status =='rejected3'">
                        <span class="bg-red">
                        Fail
                        </span>
                    </li> 
                    <li class="time-label" v-if="request.status =='fail'">
                        <span class="bg-red">
                        Fail
                        </span>
                    </li> 
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            </div>
            <!-- /.col -->
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                request : <?php echo json_encode($request); ?>,
                users : <?php echo json_encode($users); ?>,
                approvers:'',
                user: <?php echo Auth::user(); ?>
            },
            methods:{
                selectAdvanceUser: function(){
                    var vm = this;
                    if(this.approvers == ''){
                        helper.showNotification('Chưa chọn người kiểm duyệt', 'danger')
                        return
                    }

                    $.post('/admin/order-request/'+vm.request.id+'/selectAdvanceUser',{approvers : this.approvers}, function(res){
                        if(res.success){
                            location.reload();
                        }else{
                            var message = res.message ? res.message : 'Thực hiện thao tác không thành công !'
                            helper.showNotification(message,'dănger')
                        }
                    })
                },
                classAprrover: function(status){
                    if(status == 'created'){
                        return 'bg-dark fa-clock-o'
                    }
                    if(status == 'approved'){
                        return ' bg-green fa-check'
                    }
                    if(status == 'rejected'){
                        return 'bg-red fa-times'
                    }
                    if(status == 'returned'){
                        return 'bg-orange fa-backward'
                    }
                },
                goTo(link){
                    location.href = link;
                },
                goToEdit(path){
                    var link =  '/admin/order-request/'+this.request.id;
                    if(path == 'supplier'){
                        link += '/supplier/edit?show=1';
                        location.href = link
                    }
                    return
                    
                    if(path == 'request'){
                        link += '/edit';
                    }
                    if(path == 'supplier'){
                        link += '/supplier/edit';
                    }
                    if(path == 'order'){
                        link += '/order/'+this.request.order.id+'/edit';
                    }
                    location.href = link
                },
                goToDetail(path){
                    var link =  '/admin/order-request/'+this.request.id;
                    if(path == 'request'){
                        link += '/detail';
                    }
                    if(path == 'supplier'){
                        link += '/supplier/detail';
                    }
                    if(path == 'order'){
                        link += '/order/'+this.request.order.id+'/detail';
                    }
                    location.href = link
                },
                print: function(){
                    var link =  '/admin/order-request/'+this.request.id+'/print';
                    var mywindow = window.open(link, 'PRINT', 'height=600,width=1200');
                    // mywindow.document.write();
                    // mywindow.document.close();
                    mywindow.focus();
                    mywindow.print();
                }, 
                display: function(item){
                    if(item.status == 'approved'){
                        return true;
                    }else{
                        if(this.request.approver_advance.length > 0){
                            var check = JSON.parse(JSON.stringify(this.request.approver_advance)).filter(function(k){
                                return k.status == 'approved';
                            })
                            if(check.length){
                                return false;
                            }else{
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },
            computed: {
                supplier_selected: function(){
                    var text = '';
                    var vm = this;
                    if(this.request.suppliers.length){
                        this.request.suppliers.forEach(function(item){
                            if(item.id == vm.request.approver2.supplier_id){
                                text = item.supplier_name;
                            }
                        })
                    }
                    return text;
                },
                checkStepSupplier: function(){
                    check = true;
                    if(this.request.status == 'created'){
                        check = false;
                    }
                    if(this.request.status == 'rejected' && this.request.suppliers.length == 0){
                        check = false;
                    }
                    return check;
                },
                advance_users: function(){
                    var users = JSON.parse(JSON.stringify(this.users));
                    return users.filter(function(item){
                        return item.role_id == 6;
                    })
                }
            }
        })
    </script>
@endsection

