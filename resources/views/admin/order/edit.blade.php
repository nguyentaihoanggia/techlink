@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Order
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Order</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
        <div class="row">
            <div class="col-xs-12">
                <a class="btn btn-success pull-right" @click="submit()" ><i class="fa fa-check"></i> Lưu lại</a>
            </div>
        </div>
        <div class="row flex" style="margin-top: 10px;align-items: stretch;">
            <div class="col-xs-8">
                <section class="box box-order" style="height: 300px">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6 form-horizontal">
                                <label >Yêu cầu mua hàng</label>
                                <div class="form-group">
                                    <div class="col-sm-3 control-label" style="text-align: left;">
                                        Bộ phận
                                    </div>
                                    <div class="col-sm-9">
                                        <select2 :options="departments" style="width: 100%"  class="form-control" :search="true" v-model="form.request_code" placeholder="Chọn bộ phận">
                                        </select2>
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <div class="col-sm-3 control-label" style="text-align: left;">
                                        Mã số
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" disabled :value="form.request_code">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 form-horizontal">
                                <label >Sử dụng</label>
                                <div class="form-group">
                                    <div class="col-sm-3 control-label" style="text-align: left;">
                                        Bộ phận
                                    </div>
                                    <div class="col-sm-9">
                                        <select2 :options="departments" style="width: 100%"  class="form-control" :search="true" v-model="form.use_code" placeholder="Chọn bộ phận">
                                        </select2>
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <div class="col-sm-3 control-label" style="text-align: left;">
                                        Mã số
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" :value="form.use_code" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-xs-6">
                                <label for="">Ngày yêu cầu</label>
                                <datepicker class="form-control" v-model="form.request_date"></datepicker> 
                            </div>
                            <div class="col-xs-6">
                                <label for="">Ghi chú</label>
                                <input type="text" class="form-control" v-model="form.note">
                            </div>
                        </div> 
                        <div class="row mt-10">
                            <div class="col-xs-6">
                                <label for="">Mục đích</label>
                                <input type="text" class="form-control" v-model="form.reason">
                            </div>
                            <div class="col-xs-6">
                                <label for="">Địa điểm giao hàng</label>
                                <input type="text" class="form-control" v-model="form.address_delivery">
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xs-4">
                <section class="box box-order" style="height: 300px">
                    <div class="box-body">
                        <label for="">Người kiểm duyệt</label>
                        <select2 :options="users" style="width: 100%"  class="form-control" :multiple="true" :search="true" v-model="form.approver" placeholder="Chọn nhân viên">
                        </select2>
                    </div>
                </section>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-xs-12">
                <section class="box box-order">
                    <div class="box-header with-border">
                        <h3 class="box-title">Yêu cầu mua hàng </h3>
                        <a class="btn-sm btn-success pull-right" @click="openModalProduct"><i class="fa fa-plus"></i> Add</a>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th class="w-60"></th>
                                    <th>Mã hàng</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Quy cách</th>
                                    <th>Đơn vị</th>
                                    <th width="150">Số lượng</th>
                                    <th>Số lượng tồn kho</th>
                                    <th>Đơn giá</th>
                                    <th>Ngày hết hạn mua</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item,index in form.products">
                                    <td>@{{ index+1 }}</td>
                                    <td>
                                        <div class="box-item-image" @click="openModalGallery(item.product_id)">
                                            <i class="fa fa-plus" v-if="!item.path"></i>
                                            <img v-else :src="item.path" width="100%" alt="">
                                        </div>
                                    </td>
                                    <td>@{{ item.product_id }}</td>
                                    <td>@{{ item.product_name }}</td>
                                    <td>
                                        @{{ item.specification }}
                                    </td>
                                    <td>@{{ item.unit }}</td>
                                    <td>
                                        <input-spinner v-model="item.quantity"></input-spinner>
                                    </td>
                                    <td>
                                        <money class="form-control" v-model="item.inventory"></money>
                                    </td>
                                    <td>
                                        <money class="form-control" v-model="item.price"></money>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" v-model="item.date" placeholder="dd/mm/yyyy">
                                    </td>
                                    <td>
                                        <a class="text-danger" @click="removeProduct(index)"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
        <div class="modal fade" id="modal-product" style="display: none;">
            <table-product v-model="form.products"></table-product>
        </div>
        <div class="modal fade" id="modal-gallery" style="display: none;">
            <gallery v-model="image.path"></gallery>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('resources/js/table-product.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/js/gallery.js')}}"></script>
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                form: <?php echo json_encode($request) ?>,
                products:[],
                users: <?php echo json_encode($users); ?>,
                departments: <?php echo json_encode($departments); ?>,
                image:{
                    path: '',
                    product_id: ''
                }
            },
            methods:{
                openModalProduct:function(){
                    $("#modal-product").modal('show');
                },
                openModalGallery:function(id){
                    this.image.product_id = id;
                    this.image.path = '';
                    $("#modal-gallery").modal('show');
                },
                removeItem: function(index){
                    r = confirm('Xoá sản phẩm ?');
                    if(r){
                        this.form.products.splice(index,1);
                    }
                },
                submit: function(){
                    if(this.form.products.length == 0){
                        helper.showNotification("Chưa có sản phẩm nào được chọn","danger")
                        return
                    }
                    if(this.form.approver.length == 0){
                        helper.showNotification("Chưa chọn người kiểm duyệt","danger")
                        return
                    }
                    var vm = this;
                    $.ajax({
                        url : '/admin/order-request/update',
                        type : 'PATCH',
                        data : this.form,
                        success : function(res, textStatus, jqXhr) {
                            if(res.success){
                                location.href = "/admin/order-request/"+vm.form.id
                            }else{
                                var message = res.message ? res.message : 'Thực hiện thao tác không thành công !'
                                helper.showNotification(message)
                            }
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            helper.showNotification('Thực hiện thao tác không thành công', 'danger')
                        }
                    });
                },
                removeProduct: function(index){
                    this.form.products.splice(index,1);
                }
            },
            watch:{
                'image.path': function(newval){
                    if(newval) {
                        var vm = this;
                        var index = this.form.products.findIndex(function(item) {
                            return item.product_id == vm.image.product_id
                        })
                        console.log(index)
                        if (index >= 0){
                            vm.form.products[index].path = newval
                        }
                    }
                },
                'form.request_code': function(newval){
                    var index = this.departments.findIndex(function(item){
                        return item.id == newval
                    })
                    var request_department = '';
                    if(index >= 0){
                        request_department = this.departments[index].name
                    }
                    this.form.request_department = request_department;
                },
                'form.use_code': function(newval){
                    var index = this.departments.findIndex(function(item){
                        return item.id == newval
                    })
                    var use_department = '';
                    if(index >= 0){
                        use_department = this.departments[index].name
                    }
                    this.form.use_department = use_department;
                }
            },
            computed:{
                total_quantity: function(){
                    var total = 0;
                    this.form.products.forEach(function(item){
                        total += item.quantity;
                    })
                    return total;
                },
                total_money: function(){
                    var total = 0;
                    this.form.products.forEach(function(item){
                        total += (item.price*item.quantity);
                    })
                    return total;
                },
                approver: function(){
                    var vm = this;
                    return this.users.filter(function(item){
                        return vm.form.approver.indexOf(String(item.id)) >=0;
                    })
                }
            },
            mounted(){
                // helper.showNotification("asdas","","success",100000);
            }
        })
    </script>
@endsection
