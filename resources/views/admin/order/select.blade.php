@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Order
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Order</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tìm kiếm phiếu yêu cầu</h3>
                    </div>
                    <form class="box-body" method="get" action="{{ route('order-request.create') }}">
                        @csrf
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <label for="">Mã phiếu</label>
                                <input type="text" class="form-control" name="code" placeholder="Ex: A000-00000000">
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-success pull-right">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                code: ''
            },
            methods:{
                
            },
            watch:{
                
            },
            computed:{
                
            },
            mounted(){
            }
        })
    </script>
@endsection
