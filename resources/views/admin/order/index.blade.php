@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Order</a></li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
       <div class="row">
            <div class="col-md-12">
                <a class="btn btn-primary pull-right" href="{{route('order-request.search')}}"><i class="fa fa-plus"></i> Tạo mới</a>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Danh sách yêu cầu mua hàng</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-response">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mã yêu cầu</th>
                                                <th>Người tạo</th>
                                                <th>Ngày tạo</th>
                                                <th>Trạng thái</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in order_requests">
                                                <td>@{{ (pagination.current -1)*10 + index+1 }}</td>
                                                <td>@{{ item.code }}</td>
                                                <td>@{{ item.user.name }}</td>
                                                <td>@{{ item.created_at | dd-mm-yyyy }}</td>
                                                <td>@{{ item.status }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-primary" :href="'/admin/order-request/'+item.id">Chi tiết</a>
                                                    <!-- <a class="btn btn-sm btn-warning" :href="'/admin/order-request/'+item.id+'/edit'">Chỉnh sửa</a> -->
                                                    <!-- <button class="btn btn-sm btn-danger">Xóa</button> -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 " style="text-align: right">
                                <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                order_requests : <?php echo json_encode($order_requests); ?>,
                pagination: {
                    page: 1,
                    current:1,
                    total_records:<?php echo json_encode($total_records); ?>,
                    total:<?php echo json_encode($total); ?>
                }
            },
            methods:{
                load: function(){
                    var vm = this;
                    $.post('/admin/order-request/getOrderRequest', {page: vm.pagination.page}, function(res){
                        if(res.success){
                            vm.order_requests = res.order_requests
                            vm.pagination.current = res.page
                            vm.pagination.total = res.total
                            vm.pagination.total_records = res.total_records
                        }
                    })
                }
            },
            watch: {
                'pagination.page': function(){
                    this.load();
                }
            }
        })
    </script>
@endsection
