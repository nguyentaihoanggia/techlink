@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Order
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/admin/order-request">List Order Request</a></li>
            <li><a href="/admin/order-request/{{$order->order_request_id}}">Order Request</a></li>
            <li class="active">Detail</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
        <div class="row">
            <div class="col-xs-12">
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-xs-12 col-md-8">
                <section class="box box-order">
                    <!-- title row -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Nhà cung cấp</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>
                                    <b>Mã:</b> @{{ form.order.supplier_code }}
                                </p>
                                <p>
                                    <b>Tên:</b> @{{ form.order.supplier_name }}
                                </p>
                                <p>
                                    <b>Đất nước</b> @{{ form.order.country }}
                                </p>
                                <p>
                                    <b>Địa chỉ:</b> @{{ form.order.address1 }}
                                </p>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <p>
                                    <b>Người liên hệ:</b> @{{ form.order.contact_person }}
                                </p>
                                <p>
                                    <b>Số điện thoại:</b> @{{ form.order.phone1 }}
                                </p>
                                <p>
                                    <b>Fax:</b> @{{ form.order.fax }}
                                </p>
                                <p>
                                    <b>Email:</b> @{{ form.order.email }}
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="box box-order">
                    <!-- title row -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Đơn hàng </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th></th>
                                                <th>Mã sản phẩm</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Đơn vị</th>
                                                <th>Số lượng</th>
                                                <th>Đơn giá</th>
                                                <th>Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in form.order.details">
                                                <td>@{{index+1}}</td>
                                                <td>
                                                    <div class="box-item-image">
                                                        <img v-if="!item.request_detail.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                        <img v-else :src="item.request_detail.path" width="100%" alt="">
                                                    </div>
                                                </td>
                                                <td>@{{ item.request_detail.product_id }}</td>
                                                <td>@{{ item.request_detail.product_name }}</td>
                                                <td>@{{item.request_detail.unit}}</td>
                                                <td width="150">
                                                    @{{ item.request_detail.quantity | money }}
                                                </td>
                                                <td width="150">
                                                    @{{item.price | money}}
                                                </td>
                                                
                                                <td class="text-right">@{{ item.request_detail.quantity * item.price | money }}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot style="font-weight: bold;">
                                            <tr>
                                                <th colspan="5"  class="text-right">TOTAL</th>
                                                <td colspan="1" >@{{ total_quantity | money}}</td>
                                                <td class="text-right" colspan="1"></td>
                                                <td colspan="1" class="text-right">@{{ total_money | money }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xs-12 col-md-4">
                <section class="box">
                    <!-- title row -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Người kiểm duyệt</h3>
                    </div>
                    <div class="box-body" style="padding-bottom: 20px">
                        <div class="row">
                            <div class="col-xs-12">
                                <label for="">Users</label>
                                <select2 disabled :options="users" style="width: 100%" class="form-control" :multiple="true" :search="true" v-model="form.approvers" placeholder="Chọn nhân viên"></select2>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-xs-12">
                                <table class="table">
                                    <tr v-for="item,index in approvers">
                                        <td>@{{index+1}}</td>
                                        <td>@{{item.name}}</td>
                                        <td>@{{item.email}}</td>
                                        <td>@{{item.phone}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('resources/js/table-product.js')}}"></script>
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                users: <?php echo json_encode($users); ?>,
                form:{
                    approvers: <?php echo json_encode($approvers); ?>,
                    order: <?php echo json_encode($order); ?>
                }
            },
            methods:{
                submit: function(){
                    var vm = this;
                    if(this.form.approvers.length == 0){
                        helper.showNotification("Chưa chọn người kiểm duyệt","danger")
                        return
                    }
                    $.post('/admin/order-request/'+vm.form.order.order_request_id+'/order/' + vm.form.order.id,this.form,function(res){
                        if(res.success){
                            location.href = '/admin/order-request/'+vm.form.order.order_request_id
                        }else{
                            var message = res.message ? res.message : 'Thực hiện thao tác không thành công !'
                            helper.showNotification(message)
                        }
                    });
                }
            },
            computed:{
                total_quantity: function(){
                    var total = 0;
                    this.form.order.details.forEach(function(item){
                        total += item.request_detail.quantity;
                    })
                    return total;
                },
                total_money: function(){
                    var total = 0;
                    this.form.order.details.forEach(function(item){
                        total += (item.price*item.request_detail.quantity);
                    })
                    return total;
                },
                approvers: function(){
                    var vm = this;
                    return this.users.filter(function(item){
                        return vm.form.approvers.indexOf(String(item.id)) >=0;
                    })
                },
            }
        })
    </script>
@endsection
