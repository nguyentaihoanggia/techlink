@extends('admin.layouts.app')
@section('content')
<style>
    .btn-action{
        width: 200px;
        position: relative;
    }
    .btn-action button{
        position: absolute;
        top: 4px;
        z-index: 100;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Phiếu cần duyệt
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Phiếu cần duyệt</a></li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
       <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Danh sách phiếu
                            
                        cần duyệt </h3>
                    </div>
                    <div class="box-body">
                        <div class="row mt-10">
                            <div class="col-xs-6">
                                <span>Tìm thấy <b>@{{ pagination.total_records | money }}</b> phiếu</span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <div class="form-inline">
                                    <label for="">Trạng thái</label>
                                    <select v-model="status" class="form-control">
                                        <option value="">Tất cả</option>
                                        <option value="created">created</option>
                                        <option value="approved">approved</option>
                                        <option value="rejected">rejected</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-xs-12">
                                <div class="table-response">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr style="background-color: #ddd">
                                                <th width="50" class="text-center">
                                                    <input type="checkbox" v-model="selectAll">
                                                </th>
                                                <th>#</th>
                                                <th>Mã phiếu yêu cầu</th>
                                                <th>Người tạo</th>
                                                <th>Người duyệt</th>
                                                <th>Ngày tạo</th>
                                                <th>Trạng thái</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody style="border: solid 1px #ddd;">
                                            <tr v-for="item,index in list_approval"  :class="{'success': checkBox.indexOf(item.id) >=0}">
                                                <td class="text-center" @click="toogleSelect(item.id)">
                                                    <input type="checkbox" v-model="checkBox" :value="item.id">
                                                </td>
                                                <td @click="toogleSelect(item.id)">@{{ (pagination.current -1)*10 + index+1 }}</td>
                                                <td @click="toogleSelect(item.id)">@{{ item.order_request.code }}</td>
                                                <td @click="toogleSelect(item.id)">@{{ item.order_request.user.name }}</td>
                                                <td @click="toogleSelect(item.id)">@{{ item.user.name }}</td>
                                                <td @click="toogleSelect(item.id)">@{{ item.created_at | dd-mm-yyyy }}</td>
                                                <td @click="toogleSelect(item.id)">@{{ item.status }}</td>
                                                <td class="btn-action">
                                                    <button class="btn btn-sm btn-primary" @click="detail(item)">Chi tiết</button>
                                                    <!-- <button class="btn btn-sm btn-danger">Xóa</button> -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 " >
                                <div class="btn-group">
                                    <button type="button" :disabled="checkBox.length == 0" class="btn btn-default  btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span>Hành động</span>
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a @click="actionApprover('approved')">Approve</a></li>
                                        <li><a @click="actionApprover('rejected')">Reject</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 " style="text-align: right">
                                <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                list_approval : <?php echo json_encode($list_approval); ?>,
                pagination: {
                    page: 1,
                    current:1,
                    total_records:<?php echo json_encode($total_records); ?>,
                    total:<?php echo json_encode($total); ?>
                },
                checkBox: [],
                status: ''
            },
            methods:{
                actionApprover: function(status){
                    var vm = this;
                    var title = '';
                    var type = '';
                    console.log(status);
                    if(status == 'rejected'){
                        title = 'Từ chối đơn yêu cầu mua hàng';
                        type = 'red';
                    }else if(status == 'approved'){
                        title = 'Phê duyệt đơn yêu cầu mua hàng';
                        type = 'green';
                    }else{
                        title = 'Return đơn yêu cầu mua hàng';
                        type = 'orange';
                    }
                    if(vm.checkBox.length == 0){
                        helper.ShowNotification('Chưa chọn phiếu', 'danger')
                    }
                    $.confirm({
                        title: title,
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: type,
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(status == 'rejected' || status == 'returned'){
                                        if(!input.val().trim()){
                                            $.alert({
                                                content: "Please don't keep the name field empty.",
                                                type: 'red'
                                            });
                                            return false;
                                        }else{
                                            vm.submit(status,input.val())
                                        }
                                    }else{
                                        vm.submit(status,input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                submit: function(status, reason){
                    var vm = this;

                    $.post('/admin/accountant/actionAccountant', {reason: reason,status: status, ids: vm.checkBox}, function(res){
                        if(res.success){
                            var success = '<ul>';
                            var fail = '<ul>';
                            res.data.success.forEach(function(row){
                                success += "<li>"+row+", </li>";
                            })
                            success += '</ul>';
                            res.data.fail.forEach(function(row){
                                fail += "<li>"+row+",</li>";
                            })
                            fail += '</ul>';
                            $.alert({
                                title: 'Thông báo',
                                content: '<div>\
                                        <b>Thành công:</b>\
                                    </div>\
                                    <div>'+success+
                                    '</div>\
                                    <div>\
                                        <b>Thất bại:</b>\
                                    </div>\
                                    <div>'+fail+
                                    '</div>',
                                type: "blue",
                                columnClass: 'col-md-6 col-md-offset-3',
                            });
                            vm.load();
                        }else{
                            helper.ShowNotification('Thực hiện thao tác không thành công','danger')
                        }
                    })
                },
                toogleSelect: function(id){
                    var index = this.checkBox.indexOf(id);
                    if(index == -1){
                        this.checkBox.push(id);
                    }else{
                        this.checkBox.splice(index,1);
                    }
                },
                detail: function(item){
                        location.href = '/admin/order-request/'+item.order_request.id+'/accountant'
                },
                load: function(){
                    var vm = this;
                    $.post('/admin/accountant/getOrderRequestAccountant', {page: vm.pagination.page,status: this.status}, function(res){
                        if(res.success){
                            vm.list_approval = res.list_approval
                            vm.pagination.current = res.page
                            vm.pagination.total = res.total
                            vm.pagination.total_records = res.total_records
                        }
                    })
                }
            },
            computed:{
                selectAll: {
                    get: function () {
                        var vm = this;
                        if( this.list_approval.length ){
                            var check = true;
                            this.list_approval.forEach(function(item){
                                if( vm.checkBox.indexOf(item.id) == -1){
                                    check = false;
                                }
                            })
                            return check;
                        }
                        return false;
                    },
                    set: function (value) {
                        var vm = this;
                        if (value) {
                            this.list_approval.forEach(function (item) {
                                if( vm.checkBox.indexOf(item.id) == -1){
                                    vm.checkBox.push(item.id);
                                }
                            });
                        }else{
                            this.list_approval.forEach(function (item) {
                                var index =  vm.checkBox.indexOf(item.id);
                                if( index >= 0){
                                    vm.checkBox.splice(index, 1);
                                }
                            });
                        }
                    }
                }
            },
            watch: {
                'pagination.page': function(){
                    this.load();
                },
                status: function(){
                    this.load();
                }
            }
        })
    </script>
@endsection
