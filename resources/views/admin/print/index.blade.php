<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{asset('resources/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<style>
		body{
			font-size: 11px;
		}
		tr td {
			vertical-align: middle !important;
			padding: 0 5px !important;
		}
	</style>
</head>
<body>
	<div  style="display: table;width: 100%;  margin-bottom: 10px">
		<div class=" text-center" style="display: table-cell; width: 25%">
			<img src="/images/logo.png" alt="" style="width:150px;">
		</div>
		<div class=" text-center" style="display: table-cell; width: 50%">
			<h1 style="font-size: 18px;"><b>TECH-LINK Silicones(Vietnam)Co.Ltd</b></h1>
			<div style="font-size: 14px;"><b>PURCHASE ORDER</b></div>
			<div style="font-size: 14px;"><b>ĐƠN ĐẶT HÀNG</b></div>
		</div>
		<div class="" style="display: table-cell; vertical-align: bottom; width: 25%;">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-9">
			<p>Supplier: {{$supplier->supplier_name}}</p> 
			<p>Nhà cung ứng</p>
			<p>Address: {{$supplier->address1}}</p>
			<p>Địa chỉ </p>
		</div>
		<div class="col-xs-3">
			<p>PO_NO: {{ $order_request->code }}</p>
			<p style="opacity: 0">a </p>
			<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ Date('d/m/Y') }}</p>
			<p>Ngày tháng</p>
		</div>
	</div>	
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-bordered" >
				<tr>
					<td colspan="2" >
						<div>
							SHIP VIA
							<div>Phương thức giao</div>
						</div>
					</td>
					<td colspan="4" class="text-center">
						<div>
							PAYMENT TERMS
							<div>Điều kiện thanh toán</div>
						</div>
					</td>
					<td rowspan="3" >
						<div>
							Tax(%)
							<div>Thuế(%)</div>
						</div>
					</td>
					<td rowspan="3" ></td>
					<td rowspan="2" >
						Price includes tax or no
						<div>Giá có bao gồm thuế không</div>
					</td>
					<td rowspan="2" >
						CURRENCY
						<div>Loại tiền tệ</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div>
							<div style="width: 80%; display: inline-block;">
								ROAD Đường bộ
							</div>
							<div style="width: 10%; display: inline-block;">
								AIR
							</div>
						</div>
						<div>
							<div style="width: 80%; display: inline-block;">
								<input type="checkbox">
							</div>
							<div style="width: 10%; display: inline-block;">
								<input type="checkbox">
							</div>
						</div>
					</td>
					<td colspan="4" rowspan="2">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div>
							VESSEL Đường biển
						</div>
						<div >
							<input type="checkbox">
						</div>
					</td>
					<td>0%</td>
					<td>VND</td>
				</tr>
				<tr>
					<td>
						Remarks
						<div>Ghi chú</div>
					</td>
					<td colspan="9">
						
					</td>
				</tr>
				<tr>
					<td style="text-align: center; width: 5.2%">
						NO
						<div>STT</div>
					</td>
					<td style="text-align: center; width: 11.2%">
						PART NO.
						<div>Tên</div>
					</td>
					<td style="text-align: center; width: 20.4%">
						DESCRIPTION
						<div>Diễn giải</div>
					</td>
					<td style="text-align: center; width: 15.4%">
						Dimenation
						<div>Quy cách</div>
					</td>
					<td style="text-align: center; width: 8.3%">
						PR NO
						<div>Mã số đơn xin mua hàng</div>
					</td>
					<td style="text-align: center; width: 6.6%">
						DEL DATE
						<div>Ngày giao hàng</div>
					</td>
					<td style="text-align: center; width: 7.3%">
						QUANTITY
						<div>Số lượng</div>
					</td>
					<td style="text-align: center; width: 4.9%">
						UNIT
						<div>Đơn vị</div>
					</td>
					<td style="text-align: center; width: 9.4%">
						UNIT PRICE
						<div>Đơn giá</div>
					</td>
					<td style="text-align: center; ">
						AMOUNT
						<div>Thành tiền</div>
					</td>
				</tr>
				@if(!empty($supplier->details))
				@foreach($supplier->details as $detail)
				<tr>
					<td style="text-align: center; ">
						{{$loop->index + 1}}
					</td>
					<td>{{ $detail->erp['product_id'] }}</td>
					<td>{{ $detail->erp['product_name'] }}</td>
					<td>{{ $detail->erp['specification'] }}</td>
					<td></td>
					<td>{{ Date("d/m/Y",strtotime($detail->appointment_date)) }}</td>
					<td>{{ $detail->erp['quantity'] }}</td>
					<td>{{ $detail->erp['unit'] }}</td>
					<td>{{ number_format($detail->price) }}</td>
					<td>{{ number_format($detail->price * $detail->erp['quantity']) }}</td>
				</tr>
				@endforeach
				@endif
			</table>
			<table class="table table-bordered" style="margin-top: -20px">
				<!-- <tr >
					<td class="text-center" colspan="4" style="padding: 20px 0px !important;">Đơn vị thu mua</td>
					<td class="text-center" colspan="5">Đơn vị yêu cầu mua</td>
					<td ></td>
				</tr>
				<tr>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%">
						<div>Kiểm</div> soát
					</td>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%"></td>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%"><div>Người</div> mua</td>
					<td class="text-center" style="padding: 20px 5px !important;width: 9%"></td>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%">Duyệt</td>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%"></td>
					<td class="text-center" style="padding: 20px 5px !important;width: 7%"><div>Kiểm</div> soát</td>
					<td style="padding: 20px 5px !important;width: 7%"></td>
					<td style="padding: 20px 5px !important;width: 12%"><div>Người</div> mua</td>
					<td style="padding: 20px 5px !important;width: 30%"></td>
				</tr> -->
				<tr>
					<td style="text-align: center; padding: 5px !important;">Approval</td>
					<td style="text-align: center; padding: 5px !important;">Manager</td>
					<td style="text-align: center; padding: 5px !important;">Accountant</td>
					<td style="text-align: center; padding: 5px !important;">Made by</td>
					<td style="text-align: center; padding: 5px !important;">HK approval</td>
				</tr>
				<tr>
					<td style="text-align: center;padding: 5px 5px 50px 5px !important; width: 20%;">
						{{ $approval }}
					</td>
					<td style="text-align: center;padding: 5px 5px 50px 5px !important; width: 20%;">
						{{ $manager }}
					</td>
					<td style="text-align: center;padding: 5px 5px 50px 5px !important; width: 20%;">
						{{ $accountant }}
					</td>
					<td style="text-align: center;padding: 5px 5px 50px 5px !important; width: 20%;">
						{{ $madeby }}
					</td>
					<td style="text-align: center;padding: 5px 5px 50px 5px !important; width: 20%;">
						{{ $hk }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>