@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Approval
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Order</a></li>
                <li class="active">Detail</li>
            </ol>
        </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
        <section class="content" id="app">
            <div class="row">
                <div class="col-xs-12">
                    <template v-if="approver.status != 'created'">
                        <span v-if="approver.status == 'approved'" class="label label-success">Đã phê duyệt</span>
                        <span v-if="approver.status == 'rejected'" class="label label-danger">Từ chối</span>
                        <span v-if="approver.status == 'returned'" class="label label-danger">Return</span>
                    </template>
                    <template v-if="approver.status == 'created'" >
                        <a class="btn btn-danger pull-right" @click="rejected" style="margin-right: 10px"><i class="fa fa-times"></i> Reject</a>
                        <a class="btn btn-success pull-right" style="margin-right: 10px" @click="approved()" ><i class="fa fa-check"></i> Approve</a>
                    </template>
                </div>
                <div class="col-xs-12 mt-10">
                    <section class="box box-order">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nhà cung cấp @{{supplier.supplier_code}}</h3> <span class="label label-success" v-if="approver.supplier_id == supplier.id">Đã chọn</span>
                            
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        <strong>Mã NCC:</strong> @{{supplier.supplier_code}}
                                    </div>
                                    <div>
                                        <strong>Tên NCC:</strong> @{{supplier.supplier_name}}
                                    </div>
                                    <div>
                                        <strong>Địa chỉ:</strong> @{{supplier.address1}}
                                    </div>
                                    <div>
                                        <strong>Người liên hệ:</strong> @{{supplier.contact_person}}
                                    </div>
                                    <div>
                                        <strong>Email:</strong> @{{supplier.email}}
                                    </div>
                                    <div>
                                        <strong>Số điện thoại:</strong> @{{supplier.phone1}}
                                    </div>
                                </div>
                                <div class="col-xs-12 mt-10">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Ngày hẹn giao</th>
                                                    <th>Số lượng</th>
                                                    <th>Ngày cần sử dụng</th>
                                                    <th>Đơn vị</th>
                                                    <th>Đơn giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="item,index in supplier.details">
                                                    <td>@{{index+1}}</td>
                                                    <td>
                                                        <div class="box-item-image">
                                                            <img v-if="!item.erp.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                            <img v-else :src="item.erp.path" width="100%" alt="">
                                                        </div>
                                                    </td>
                                                    <td>@{{item.erp.product_id}}</td>
                                                    <td>@{{item.erp.product_name}}</td>
                                                    <td>@{{ item.appointment_date | dd-mm-yyyy}}</td>

                                                    <td width="150">
                                                        @{{ item.erp.quantity | money }}
                                                    </td>
                                                    <td>@{{ item.required_date | dd-mm-yyyy }}</td>
                                                    <td>@{{item.erp.unit}}</td>
                                                    <td width="150">
                                                        @{{ item.price | money }}
                                                    </td>
                                                    <td>Unit</td>
                                                    <td class="text-right">@{{ item.erp.quantity * item.price | money }}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot style="font-weight: bold;">
                                                <tr>
                                                    <th colspan="6"  class="text-right">TOTAL</th>
                                                    <td colspan="1" >@{{ total_quantity(supplier.details) | money}}</td>
                                                    <td class="text-right" colspan="3"></td>
                                                    <td colspan="1" class="text-right">@{{ total_money(supplier.details) | money }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>

    </div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                supplier:<?php echo json_encode($supplier); ?>,
                order_request:<?php echo json_encode($order_request); ?>,
                approver: <?php echo json_encode($approver); ?>,
                show: <?php echo isset($_GET['show']) ? 1 : 0; ?>,
                tab:0
            },
            methods:{
                getDate: function(value){
                    if(value){
                        var date = value.substr(-2);
                        var month = value.substr(4,2);
                        var year = value.substr(0,4);
                        return date+'/'+month+'/'+year;
                    }
                    return '';
                },
                approved: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Phê duyệt đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "green",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    vm.submit('approved',input.val())
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                rejected: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Từ chối đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('rejected',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                submit(status, content,id){
                    var vm = this;
                    $.ajax({
                        type: "Patch",
                        url: "/admin/order-request/"+vm.order_request.id+"/approval-accountant",
                        data: {
                            status : status,
                            reason: content
                        },
                        success: function (res) {
                            if(res.success){
                                if(vm.show){
                                    location.href = '/admin/order-request/'+vm.order_request.id;
                                }else{
                                    location.href = '/admin/accountant';
                                }
                                
                            }else{
                                var message = res.message ? res.message : 'Something went wrong'
                                helper.showNotification(message,'danger')
                            }
                        },
                    });
                },
                total_quantity: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += item.erp.quantity;
                    })
                    return total;
                },
                total_money: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += (item.price*item.erp.quantity);
                    })
                    return total;
                },
            },
            computed:{

            },
            mounted() {

            }
        })
    </script>
@endsection

