@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Approval
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Order</a></li>
                <li class="active">Detail</li>
            </ol>
        </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
        <section class="content" id="app">
            <div class="row">
                <div class="col-xs-12">
                    <section class="box box-order">
                    <!-- title row -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Phiếu yêu cầu mua hàng <b>@{{ order_request.code }}</b></h3>
                        <template v-if="approver.status != 'created'">
                            <span v-if="approver.status == 'approved'" class="label label-success">Đã phê duyệt</span>
                            <span v-if="approver.status == 'rejected'" class="label label-danger">Từ chối</span>
                            <span v-if="approver.status == 'returned'" class="label label-warning">Return</span>
                        </template>
                        <template v-if="approver.status == 'created'" >
                            <a class="btn btn-danger pull-right" @click="rejected"><i class="fa fa-times"></i> Reject</a>
                            <a class="btn btn-warning pull-right" style="margin-right: 10px" @click="on_return" ><i class="fa fa-check"></i> Return</a>
                            <a class="btn btn-success pull-right" style="margin-right: 10px" @click="approved" ><i class="fa fa-check"></i> Approve</a>
                        </template>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td rowspan="2" style="width: 7%;" class="text-center">
                                                <b>Yêu cầu mua hàng</b>
                                            </td>
                                            <td style="width: 7%;" class="text-center">
                                                <b>Bộ phận</b>
                                            </td>
                                            <td style="width: 15%">@{{ order_request.request_department }}</td>
                                            <td rowspan="2" style="width: 7%;" class="text-center">
                                                <b>Sử dụng</b>
                                            </td>
                                            <td style="width: 7%;" class="text-center">
                                                <b>Bộ phận</b>
                                            </td>
                                            <td style="width: 15%">
                                                @{{ order_request.use_department }}
                                            </td>
                                            <td rowspan="2" style="width: 7%;" class="text-center">
                                                <b>Ngày yêu cầu</b>
                                            </td>
                                            <td style="width: 15%" rowspan="2">@{{ getDate(order_request.request_date) }}</td>
                                            <td class="text-center">
                                                <b>Ghi chú</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <b>Mã số</b>
                                            </td>
                                            <td>@{{ order_request.request_code }}</td>
                                            <td class="text-center">
                                                <b>Mã</b>
                                            </td>
                                            <td>@{{ order_request.use_code }}</td>
                                            <td rowspan="3">
                                                @{{ order_request.note }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <b>Mục đích</b>
                                            </td>
                                            <td colspan="5">
                                                @{{ order_request.reason }}
                                            </td>
                                            <td class="text-center">
                                                <b>Địa điểm giao hàng</b>
                                            </td>
                                            <td>
                                                @{{ order_request.address_delivery }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <div class="row mt-10" v-if="order_request.comment">
                            <div class="col-xs-12">
                                <span class="text-danger" >(*) </span><b style="text-decoration: underline">Ghi chú:</b> @{{ order_request.comment }}
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th></th>
                                                <th>Mã hàng</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Quy cách</th>
                                                <th>Đơn vị</th>
                                                <th>Số lượng</th>
                                                <!-- <th>Đơn giá</th> -->
                                                <th>Thời hạn sử dụng (Ngày)</th>
                                                <th>Ngày hết hạn mua</th>
                                                <th></th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in details">
                                                <td>@{{index+1}}</td>
                                                <td>
                                                    <div class="box-item-image">
                                                        <img v-if="!item.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                        <img v-else :src="item.path" width="100%" alt="">
                                                    </div>
                                                </td>
                                                <td>@{{ item.product_id }}</td>
                                                <td>@{{ item.product_name }}</td>
                                                <td>@{{ item.specification }}</td>
                                                <td>@{{ item.unit }}</td>
                                                <td>
                                                    <p>@{{ item.quantity |money }}</p>
                                                    <p>
                                                        <small class="text-danger">
                                                            Tồn kho: @{{ item.inventory |money }}
                                                        </small>
                                                    </p>
                                                </td>
                                                <!-- <td>@{{ item.price | money }}</td> -->
                                                <td>@{{ item.expiry_date }}</td>
                                                <td>@{{ getDate(item.date) }}</td>
                                                <td>
                                                    <a style="cursor: pointer;" @click="showHistory(item.product_id)">Lịch sử mua</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot style="font-weight: bold; font-size: 16px;">
                                            <tr>
                                                <th colspan="6"  class="text-right">TOTAL</th>
                                                <td colspan="1" >@{{ total_quantity | money}}</td>
                                                <td class="text-right" colspan="2"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalHistory" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Lịch sử mua hàng</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Mã đơn hàng</th>
                                                    <th>Số lượng</th>
                                                    <th>Đơn vị</th>
                                                    <th>Ngày giao</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template v-if="!is_loading">
                                                    <tr v-for="item,index in list_history.data">
                                                        <td>@{{ (list_history.page - 1)*10 + index+1 }}</td>
                                                        <td>@{{ item.TH004 }}</td>
                                                        <td>@{{ item.TH005 }}</td>
                                                        <td>@{{ item.TH011 }}-@{{ item.TH012 }}</td>
                                                        <td>@{{ item.TH007 | money }}</td>
                                                        <td>@{{ item.TH008 }}</td>
                                                        <td>@{{ getDate(item.TG003) }}</td>
                                                    </tr>
                                                </template>
                                                <template v-else>
                                                    <tr>
                                                        <td colspan="7" class="text-center">
                                                            <div class="loader"></div>
                                                        </td>
                                                    </tr>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xs-12 " style="text-align: right">
                                    <pagination v-model="list_history.page" :total="list_history.total" :current="list_history.current_page"></pagination>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </section>

        <!-- /.content -->
        <div class="clearfix"></div>

    </div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                details:<?php echo json_encode($details); ?>,
                order_request: <?php echo json_encode($order_request); ?>,
                approver: <?php echo json_encode($approver); ?>,
                show: <?php echo isset($_GET['show']) ? 1 : 0; ?>,
                list_history:{
                    data : [],
                    total_records: 0,
                    page: 1,
                    current_page: 1,
                    total: 0,
                    id: ''
                },
                is_loading: false
            },
            methods:{
                showHistory: function(id){
                    this.list_history.id = id;
                    if(this.list_history.page != 1){
                        this.list_history.page = 1;
                    }else{
                        this.loadHistory();
                    }
                    
                },
                loadHistory: function(){
                    var vm = this;
                    vm.is_loading = true;
                    $("#modalHistory").modal('show')
                    $.post('/admin/product/history', {id: vm.list_history.id, page : vm.list_history.page}, function(res){
                        if(res.success){
                            vm.list_history.data = res.products;
                            vm.list_history.total_records = res.total_records;
                            vm.list_history.current_page = res.page;
                            vm.list_history.total = res.total;
                            vm.is_loading = false;
                        }
                    })
                },
                getDate: function(value){
                    if(value){
                        var date = value.substr(-2);
                        var month = value.substr(4,2);
                        var year = value.substr(0,4);
                        return date+'/'+month+'/'+year;
                    }
                    return '';
                },
                approved: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Phê duyệt đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "green",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    vm.submit('approved',input.val())
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                rejected: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Từ chối đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('rejected',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                on_return: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Return đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('returned',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                submit: function(status, content){
                    var vm = this;
                    $.ajax({
                        type: "Patch",
                        url: "/admin/order-request/"+vm.order_request.id+"/approval",
                        data: {
                            status : status,
                            reason: content
                        },
                        success: function (res) {
                            if(res.success){
                                if(vm.show){
                                    location.href = '/admin/order-request/'+vm.order_request.id
                                }else{
                                    location.href = '/admin/approval';
                                }
                            }else{
                                var message = res.message ? res.message : 'Something went wrong'
                                helper.showNotification(message,'danger')
                            }
                        },
                    });
                },
                history: function(id){

                }
            },
            computed:{
                total_quantity: function(){
                    var total = 0;
                    this.details.forEach(function(item){
                        total += item.quantity;
                    })
                    return total;
                },
                total_money: function(){
                    var total = 0;
                    this.details.forEach(function(item){
                        total += item.quantity * item.price;
                    })
                    return total;
                },

            },
            watch:{
                'list_history.page': function(){
                    this.loadHistory()
                }
            },
            mounted() {

            }
        })
    </script>
@endsection

