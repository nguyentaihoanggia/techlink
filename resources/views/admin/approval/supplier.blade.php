@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Approval
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Order</a></li>
                <li class="active">Detail</li>
            </ol>
        </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
        <section class="content" id="app">
            <div class="row">
                <div class="col-xs-12">
                    <template v-if="approver.status != 'created'">
                        <span v-if="approver.status == 'approved'" class="label label-success">Đã phê duyệt</span>
                        <span v-if="approver.status == 'rejected'" class="label label-danger">Từ chối</span>
                        <span v-if="approver.status == 'returned'" class="label label-danger">Return</span>
                    </template>
                    <template v-if="approver.status == 'created'" >
                        <a class="btn btn-warning pull-right" @click="on_return"><i class="fa fa-times"></i> Return</a>
                        <a class="btn btn-danger pull-right" @click="rejected" style="margin-right: 10px"><i class="fa fa-times"></i> Reject</a>
                    </template>
                </div>
                <div class="col-xs-12 mt-10">
                    <section class="box box-order" v-for="supplier,index in suppliers">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nhà cung cấp @{{supplier.supplier_code}}</h3> <span class="label label-success" v-if="approver.supplier_id == supplier.id">Đã chọn</span>
                            <a v-if="approver.status == 'created'" class="btn btn-success pull-right" style="margin-right: 10px" @click="approved(supplier.id)" ><i class="fa fa-check"></i> Approve</a>
                            <a class="btn btn-default pull-right" style="margin-right: 10px" @click="history(supplier._id)" ><i class="fa fa-search"></i> Lịch sử</a>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        <strong>Mã NCC:</strong> @{{supplier.supplier_code}}
                                    </div>
                                    <div>
                                        <strong>Tên NCC:</strong> @{{supplier.supplier_name}}
                                    </div>
                                    <div>
                                        <strong>Địa chỉ:</strong> @{{supplier.address1}}
                                    </div>
                                    <div>
                                        <strong>Người liên hệ:</strong> @{{supplier.contact_person}}
                                    </div>
                                    <div>
                                        <strong>Email:</strong> @{{supplier.email}}
                                    </div>
                                    <div>
                                        <strong>Số điện thoại:</strong> @{{supplier.phone1}}
                                    </div>
                                </div>
                                <div class="col-xs-12 mt-10">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Ngày hẹn giao</th>
                                                    <th>Số lượng</th>
                                                    <th>Ngày cần sử dụng</th>
                                                    <th>Đơn vị</th>
                                                    <th>Đơn giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="item,index in supplier.details">
                                                    <td>@{{index+1}}</td>
                                                    <td>
                                                        <div class="box-item-image">
                                                            <img v-if="!item.erp.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                            <img v-else :src="item.erp.path" width="100%" alt="">
                                                        </div>
                                                    </td>
                                                    <td>@{{item.erp.product_id}}</td>
                                                    <td>@{{item.erp.product_name}}</td>
                                                    <td>@{{ item.appointment_date | dd-mm-yyyy}}</td>
                                                    
                                                    <td width="150">
                                                        @{{ item.erp.quantity | money }}
                                                    </td>
                                                    <td>@{{ item.required_date | dd-mm-yyyy }}</td>
                                                    <td>@{{item.erp.unit}}</td>
                                                    <td width="150">
                                                        @{{ item.price | money }}
                                                    </td>
                                                    <td>Unit</td>
                                                    <td class="text-right">@{{ item.erp.quantity * item.price | money }}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot style="font-weight: bold;">
                                                <tr>
                                                    <th colspan="6"  class="text-right">TOTAL</th>
                                                    <td colspan="1" >@{{ total_quantity(supplier.details) | money}}</td>
                                                    <td class="text-right" colspan="3"></td>
                                                    <td colspan="1" class="text-right">@{{ total_money(supplier.details) | money }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="modal fade" id="modalHistory" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Lịch sử giao hàng</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Mã đơn hàng</th>
                                                    <th>Ngày dự kiến giao</th>
                                                    <th>Ngày giao</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template v-if="!is_loading">
                                                    <tr v-for="item,index in list_history.data">
                                                        <td>@{{ (list_history.page - 1)*10 + index+1 }}</td>
                                                        <td>@{{ item.code }}</td>
                                                        <td>--</td>
                                                        <td>@{{ getDate(item.TG003) }}</td>
                                                    </tr>
                                                </template>
                                                <template v-else>
                                                    <tr>
                                                        <td colspan="4" class="text-center">
                                                            <div class="loader"></div>
                                                        </td>
                                                    </tr>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xs-12 " style="text-align: right">
                                    <pagination v-model="list_history.page" :total="list_history.total" :current="list_history.current_page"></pagination>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
        
    </div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                suppliers:<?php echo json_encode($suppliers); ?>,
                order_request:<?php echo json_encode($order_request); ?>,
                approver: <?php echo json_encode($approver); ?>,
                show: <?php echo isset($_GET['show']) ? 1 : 0; ?>,
                tab:0,
                list_history:{
                    data : [],
                    total_records: 0,
                    page: 1,
                    current_page: 1,
                    total: 0,
                    id: ''
                },
                is_loading: false
            },
            methods:{
                history: function(id){
                    this.list_history.id = id;
                    this.list_history.page = 1;
                    this.loadHistory();
                },
                loadHistory: function(){
                    var vm = this;
                    vm.is_loading = true;
                    $("#modalHistory").modal('show')
                    $.post('/admin/supplier/history', {id: vm.list_history.id, page : vm.list_history.page}, function(res){
                        if(res.success){
                            vm.list_history.data = res.suppliers;
                            vm.list_history.total_records = res.total_records;
                            vm.list_history.current_page = res.page;
                            vm.list_history.total = res.total;
                            vm.is_loading = false;
                        }
                    })
                },
                getDate: function(value){
                    if(value){
                        var date = value.substr(-2);
                        var month = value.substr(4,2);
                        var year = value.substr(0,4);
                        return date+'/'+month+'/'+year;
                    }
                    return '';
                },
                approved: function(id){
                    var vm = this;
                    $.confirm({
                        title: 'Phê duyệt đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "green",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    vm.submit('approved',input.val(),id)
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                rejected: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Từ chối đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('rejected',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                on_return: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Return đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('returned',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                submit(status, content,id){
                    var vm = this;
                    $.ajax({
                        type: "Patch",
                        url: "/admin/order-request/"+vm.order_request.id+"/approval-supplier",
                        data: {
                            status : status,
                            reason: content,
                            supplier_id:id
                        },
                        success: function (res) {
                            if(res.success){
                                if(vm.show){
                                    location.href = '/admin/order-request/'+vm.order_request.id;
                                }else{
                                    location.href = '/admin/approval?step=2';
                                }
                                
                            }else{
                                var message = res.message ? res.message : 'Something went wrong'
                                helper.showNotification(message,'danger')
                            } 
                        },
                    });
                },
                total_quantity: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += item.erp.quantity;
                    })
                    return total;
                },
                total_money: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += (item.price*item.erp.quantity);
                    })
                    return total;
                },
            },
            watch:{
                'list_history.page' : function(){
                    this.loadHistory();
                }
            },
            computed:{
                
            },
            mounted() {
                
            }
        })
    </script>
@endsection

