@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Approval
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Order</a></li>
                <li class="active">Detail</li>
            </ol>
        </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
        <section class="content" id="app">
            <div class="row">
                <div class="col-xs-12">
                    <section class="box box-order">
                    <!-- title row -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Phiếu mua hàng </h3>
                        <template v-if="approver.status != 'created'">
                            <span v-if="approver.status == 'approved'" class="label label-success">Đã phê duyệt</span>
                            <span v-if="approver.status == 'rejected'" class="label label-danger">Từ chối</span>
                        </template>
                        <template v-if="approver.status == 'created'" >
                            <a class="btn btn-danger pull-right" @click="rejected"><i class="fa fa-times"></i> Reject</a>
                            <a class="btn btn-success pull-right" style="margin-right: 10px" @click="approved" ><i class="fa fa-check"></i> Approve</a>
                        </template>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6">
                                <p><b>Mã đơn hàng: </b>@{{ order.code }}</p>
                                <p><b>Ngày tạo: </b>@{{ order.created_at | dd-mm-yyyy }}</p>
                                <p><b>Người tạo: </b>@{{ order.created_by.name }}</p>
                                <p><b>Trạng thái: </b>@{{ order.status }}</p>
                            </div>
                            <div class="col-xs-6">
                                <p><b>Mã nhà cung cấp: </b>@{{ order.supplier_code }}</p>
                                <p><b>Tên nhà cung cấp: </b>@{{ order.supplier_name }}</p>
                                <p><b>Tên người liên hệ: </b>@{{ order.contact_person }}</p>
                                <p><b>Số điện thoại: </b>@{{ order.phone1 }}</p>
                                <p><b>Địa chỉ: </b>@{{ order.address1 }}</p>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-xs-12">
                                <label for="">Đơn hàng</label>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <td></td>
                                                <td>Mã sản phẩm</td>
                                                <th>Tên sản phẩm</th>
                                                <th>Đơn vị</th>
                                                <th>Số lượng</th>
                                                <th>Đơn giá</th>
                                                <th class="text-right">Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in order.details">
                                                <td>@{{index+1}}</td>
                                                <td>
                                                    <div class="box-item-image">
                                                        <img v-if="!item.request_detail.path" src="{{ asset('images/empty.jpg') }}" width="100%">
                                                        <img v-else :src="item.request_detail.path" width="100%" alt="">
                                                    </div>
                                                </td>
                                                <td>@{{item.request_detail.product_id}}</td>
                                                <td>@{{item.request_detail.product_name}}</td>
                                                <td>@{{item.request_detail.unit}}</td>
                                                <td width="150">
                                                    @{{ item.request_detail.quantity | money }}
                                                </td>
                                                <td width="150">
                                                    @{{item.price | money}}
                                                </td>
                                                <td class="text-right">@{{ item.request_detail.quantity * item.price | money }}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot style="font-weight: bold;">
                                            <tr>
                                                <th colspan="5"  class="text-right">TOTAL</th>
                                                <td colspan="1" >@{{ total_quantity(order.details) | money}}</td>
                                                <td class="text-right" colspan="1"></td>
                                                <td colspan="1" class="text-right">@{{ total_money(order.details) | money }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>

    </div>
@endsection
@section('js')
    <script >
        var app = new Vue({
            el: '#app',
            data: {
                order:<?php echo json_encode($order); ?>,
                order_request:<?php echo json_encode($order_request); ?>,
                approver: <?php echo json_encode($approver); ?>,
                tab:0
            },
            methods:{
                approved: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Phê duyệt đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "green",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    vm.submit('approved',input.val())
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                rejected: function(){
                    var vm = this;
                    $.confirm({
                        title: 'Từ chối đơn yêu cầu mua hàng',
                        content: 'Ghi Chú: <textarea class="form-control" rows="4" id="content-approve" style="margin-top:10px"></textarea>',
                        type: "red",
                        draggable: false,
                        theme: 'material',
                        columnClass: 'col-md-6 col-md-offset-3',
                        buttons: {
                            ok: {
                                text: 'Xác nhận',
                                btnClass: 'btn btn-success',
                                keys: ['enter'],
                                action: function() {
                                    var input = this.$content.find('textarea#content-approve');
                                    if(!input.val().trim()){
                                        $.alert({
                                            content: "Please don't keep the name field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    }else{
                                        vm.submit('rejected',input.val())
                                    }
                                }
                            },
                            cancel: {
                                text: 'Đóng',
                                keys: ['esc'],
                                btnClass: 'btn btn-default',
                            }
                        }
                    });
                },
                submit(status, content){
                    var vm = this;
                    $.ajax({
                        type: "Patch",
                        url: "/admin/order-request/"+vm.order_request.id+"/order-approver/"+vm.order.id,
                        data: {
                            status : status,
                            reason: content
                        },
                        success: function (res) {
                            if(res.success){
                                location.href = '/admin/approval';
                            }else{
                                var message = res.message ? res.message : 'Something went wrong'
                                helper.showNotification(message,'danger')
                            } 
                        },
                    });
                },
                total_quantity: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += item.request_detail.quantity;
                    })
                    return total;
                },
                total_money: function(details){
                    var total = 0;
                    details.forEach(function(item){
                        total += (item.price*item.request_detail.quantity);
                    })
                    return total;
                },
            },
            computed:{
                
            },
            mounted() {
                
            }
        })
    </script>
@endsection

