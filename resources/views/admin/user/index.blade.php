@extends('admin.layouts.app')
@section('content')
<style>
    .btn-dark{
        background-color: #3c3c3c;
        color: #fff;
    }
    .btn-dark:hover{
        background-color: #3c3c3c !important;
        color: #fff !important;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create User
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> List User
                </h2>

            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                Result: <b>{{$users->total()}} </b>
            </div>
            <div class="col-xs-12 col-md-4">
                <a class="btn btn-success pull-right" href="{{route('user.create')}}"><i class="fa fa-plus"></i> Create</a>
            </div>
        </div>
        <div class="row mt-10">
            <div class="col-xs-12 text-right">
                <div class="btn-group">
                    @if(isset($_GET['is_delete']) && $_GET['is_delete'] == 1)
                        <a class="btn btn-default " href="{{ route('user.index') }}"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        <button class="btn btn-default btn-dark" disabled><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    @else
                        <button class="btn btn-default btn-dark" disabled><i class="fa fa-bars" aria-hidden="true"></i></button>
                        <a class="btn btn-default" href="{{ route('user.index', ['is_delete' => 1]) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    @endif
                    
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$loop->index  +1}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{ \App\Models\User::$roles[$user->role_id] }}</td>
                                <td width="200">
                                    @if(isset($_GET['is_delete']) && $_GET['is_delete'] == 1)
                                    <form method="POST" class="form-inline" action="{{ route('user.restore', ['id' => $user->id]) }}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <button class="btn btn-primary" type="submit">Khôi phục</button>
                                    </form>
                                    @else
                                    <a class="btn btn-warning" href="{{route('user.edit', ['id' => $user->id])}}">Edit</a>
                                    <form method="POST" class="form-inline" action="{{ route('user.destroy', ['id' => $user->id]) }}">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <button class="btn btn-danger" type="submit">Xóa</button>
                                    </form>
                                    @endif
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-right">
                {{$users->links()}}
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection
