@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create User
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Create User
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                {!! Form::open(['method' => 'POST', 'url' => route('user.store')]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name' , null , ['class' => 'form-control','placeholder' => 'Name','required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::email('email' , null , ['class' => 'form-control','placeholder' => 'Email','required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('role_id', 'Role') !!}
                    {!! Form::select('role_id', $roles,null,['class' => 'form-control']) !!} 
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Password Confirmation') !!}
                    {!! Form::password('password', ['class' => 'form-control','placeholder' => '','name' => 'password_confirmation']) !!}
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Create</button>
                        <a href="{{route('user.index')}}" class="btn btn-default pull-right" style="margin-right: 5px;" ><i class="fa fa-chevron-left"></i> Back</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection