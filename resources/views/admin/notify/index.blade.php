@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách mua hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách mua hàng</a></li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Danh sách mua hàng
                </h2>

            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                Result: <b>{{$notify->total()}} </b>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover" style="    border: solid 2px #f4f4f4;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Mã yêu cầu</th>
                                <th>Người tạo</th>
                                <th>Ngày tạo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($notify) > 0)
                            @foreach($notify as $item)
                            <tr>
                                <td>{{$loop->index  +1}}</td>
                                <td>{{$item->order_request->code}}</td>
                                <td>{{$item->order_request->user->name }}</td>
                                <td>
                                    {{ Date('d/m/Y', strtotime($item->order_request->created_at)) }}
                                </td>
                                <td width="200">
                                    @if($item->status == 'success')
                                    <a class="btn btn-success" href="{{ route('order-request.supplier', ['id' => $item->order_request_id]) }}">Tìm NCC</a>
                                    @endif
                                    @if($item->status != 'success')
                                    <a class="btn btn-default" href="{{ route('order-request.supplier-detail', ['id' => $item->order_request_id]) }}">Chi tiết</a>
                                    @endif
                                    @if($item->status == 'lock')
                                        @if($item->order_request->status != 'returned2')
                                            <a type="button"   class="btn btn-primary" href="" disabled="">Chỉnh sửa</a>
                                        @else 
                                            <a type="button"   class="btn btn-primary" href="{{ route('order-request.supplier-edit', ['id' => $item->order_request_id]) }}">Chỉnh sửa</a>
                                        @endif
                                        
                                    @endif
                                    
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5" class="text-center">KHÔNG CÓ DỮ LIỆU</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-right">
                {{$notify->links()}}
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection
