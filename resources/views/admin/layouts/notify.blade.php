@if($errors->any())
	<div class="pad margin no-print">
	    <div class="callout callout-danger" style="margin-bottom: 0!important;">
	        <h4><i class="fa fa-info"></i> Note:</h4>
	        <ul>
	        @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		    @endforeach
			</ul>
	    </div>
	</div>
@endif
@if (\Session::has('message'))
   <div class="pad margin no-print">
	    <div class="callout callout-success" style="margin-bottom: 0!important;">
	    	<div>{{ Session::get('message') }}</div>
	    </div>
	</div>
@endif