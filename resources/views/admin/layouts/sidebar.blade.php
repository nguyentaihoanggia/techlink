<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="">
                <a  href="{{route('admin')}}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
            </li>
            @if(Auth::user()->role_id ==  \App\Models\User::ROLE_ADMIN)
            <li class="treeview">
                <a href="#">
                <i class="fa fa-user"></i> <span>Nhân sự</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('user.index')}}"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                    <li><a href="{{route('user.create')}}"><i class="fa fa-circle-o"></i> Tạo mới</a></li>
                </ul>
            </li>
            @endif
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Phiếu YCMH</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('order-request.index')}}"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                    <li><a href="{{route('order-request.search')}}"><i class="fa fa-circle-o"></i> Tạo mới</a></li>
                </ul>
            </li>
            @if(Auth::user()->role_id ==  \App\Models\User::ROLE_MANAGER)
            <li class="treeview" >
                <a href="#">
                    <i class="fa fa-user"></i> <span>Phiếu cần duyệt</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('approval.index',['step' => 1])}}"><i class="fa fa-circle-o"></i> Danh sách phiếu YCMH</a></li>
                    <li><a href="{{route('approval.index',['step' => 2])}}"><i class="fa fa-circle-o"></i> Danh sách phiếu chọn NCC</a></li>
                    <li><a href="{{route('approval.index',['step' => 4])}}"><i class="fa fa-circle-o"></i> Danh sách phiếu nâng cao</a></li>
                </ul>
            </li>
            @endif
            @if(Auth::user()->role_id == \App\Models\User::ROLE_ACCOUNTANT)
            <li class="" >
                <a href="{{route('order-request.accountant_index')}}">
                    <i class="fa fa-user"></i> <span>Kế toán</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role_id == \App\Models\User::ROLE_NOTIFY)
            <li class="">
                <a href="{{route('notify.index')}}">
                    <i class="fa fa-user"></i> <span>Tìm nhà cung cấp</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
