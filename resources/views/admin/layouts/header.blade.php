<style>
    .user-image{
        border-radius: 50%;
        line-height: 25px;
        text-align: center;
        background: #fff;
        color: #3c8dbc;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>
<header class="main-header">
    <!-- Logo -->
    <a href="/admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>TL</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>TechLink</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="user-image">{{ substr(Auth::user()->name,0,1) }}</span>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu" style="width: auto">
                        
                        <li class="">
                            <form action="{{ route('logout') }}" method="POST" >
                                @csrf
                                <button style="width: 100%" type="submit" class="btn btn-default btn-flat">Sign out</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
