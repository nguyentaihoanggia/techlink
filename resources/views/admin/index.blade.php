@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Dashboard</a></li>
        </ol>
    </section>
    @include('admin.layouts.notify')
    <!-- Main content -->
    <section class="content" id="app">
       <div class="row">
            <div class="col-xs-12">
                <div class="daterange">
                    <daterange class="form-control" v-model="date" ></daterange>
                </div>
            </div>
        </div>
        <div class="row mt-10">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-clipboard"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Tổng số phiếu yêu cầu</span>
                                <span class="info-box-number">@{{ total_request | money }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Tổng số phiếu thành công</span>
                                <span class="info-box-number">@{{ total_order | money }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-cube"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Tổng số mặt hàng</span>
                                <span class="info-box-number">@{{ total_quantity | money }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-usd"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Tổng số tiền</span>
                                <span class="info-box-number">@{{ total_cost | money }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
        <div class="row  mt-10">
            <div class="col-xs-12 col-sm-6">
                <div class="box" style="height: 550px; overflow: auto;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Danh sách phiếu YCMH</h3>
                        <span class="text-info">(Chỉ hiện thị phiếu YCMH chưa được duyệt hơn 1 ngày)</span>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Mã phiếu</th>
                                                <th>Ngày mua</th>
                                                <th>Ngày tạo</th>
                                                <th>Trạng thái</th>
                                                <th>Số ngày chờ duyệt</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="item,index in orders">
                                                <td>@{{ index+1 }}</td>
                                                <td>@{{ item.code }}</td>
                                                <td>@{{ item.user.name }}</td>
                                                <td>@{{ item.created_at | dd/mm/yyyy }}</td>
                                                <td>@{{ item.status }}</td>
                                                <td>
                                                    <a :href="'/admin/order-request/'+item.id" target="_blank">Chi tiết</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="box"  style="height: 550px; overflow: auto;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Danh sách phiếu YCMH</h3>
                        <span class="text-info">(Chỉ hiện thị phiếu YCMH chưa được duyệt hơn 1 ngày)</span>
                    </div>
                    <div class="box-body">
                        <canvas id="canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div>
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/Chart.min.js')}}"></script>
    <script >
        var ctx;
        var chart;
        var app = new Vue({
            el: '#app',
            data: {
                date:{
                    startDate: <?php echo json_encode($startDate); ?>,
                    endDate: <?php echo json_encode($endDate); ?>,
                },
                total_cost:  null,
                total_quantity:  null,
                total_request:  null,
                total_order:  null,
                orders: [],
                report: [],
                pagination: {
                    page: 1,
                    total: null,
                    current:1
                }
            },
            methods:{
                updateData: function(){
                    var vm = this;
                    $.post('/admin/updateData',{startDate:vm.date.startDate , endDate: vm.date.endDate, page: vm.pagination.page}, function(res){
                        if(res.success){
                            var data = res.data;
                            vm.date.startDate = data.startDate;
                            vm.date.endDate = data.endDate;
                            vm.total_cost = data.total_cost;
                            vm.total_quantity = data.total_quantity;
                            vm.total_request = data.total_request;
                            vm.total_order = data.total_order;

                            vm.report = data.report;
                            vm.orders = data.orders;
                            vm.pagination.current = data.page;
                            vm.pagination.total = data.total;
                            vm.updateReport();
                        }
                    });
                },
                updateReport: function(){
                    var vm =this;
                    var color = Chart.helpers.color;
                    var barChartData = {
                        labels: vm.report.range,
                        datasets: [{
                            label: 'Tổng số phiếu',
                            backgroundColor: color( '#444').alpha(0.5).rgbString(),
                            borderColor: '#444',
                            borderWidth: 1,
                            data: vm.report.data,
                        }, {
                            label: 'Tổng số phiếu thành công',
                            backgroundColor: color('#09f15b').alpha(0.5).rgbString(),
                            borderColor: '#09f15b',
                            borderWidth: 1,
                            data: vm.report.data_success,
                        }]

                    };
                    if(chart){
                        chart.destroy();
                    }

                    
                    ctx = document.getElementById('canvas').getContext('2d');
                    chart = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData,
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'Yêu cầu mua hàng'
                            }
                        }
                    });
                }
            },
            watch: {
                'date.startDate': function(){
                    this.updateData();
                },
                'date.endDate': function(){
                    this.updateData();
                }
            },
            created: function(){
                this.updateData();
                
            }
        })
    </script>
@endsection
