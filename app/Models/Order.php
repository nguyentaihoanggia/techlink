<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        "code",
        "order_request_id",
        "supplier_id",
        "supplier_code",
        "supplier_name",
        "country",
        "area",
        "phone1",
        "address1",
        "contact_person",
        "fax",
        "email",
        "total_cost",
        "status",
        "create_user_id"
    ];
    const STATUS_CREATED = "created";
    const STATUS_APPROVER = 'approved';
    const STATUS_REJECT = 'rejected';

    public static function setCode(){
        $max = Order::query()->max('id') + 1;
        return "OD".Date("Ym").'_'.sprintf( "%06d", $max);
    }

    public function details(){
        return $this->hasMany('App\Models\OrderDetail','order_id','id');
    }

    public function created_by(){
        return $this->belongsTo('App\Models\User','create_user_id','id');
    }

    public function supplier(){
        return $this->belongsTo('App\Models\OrderRequestSupplier','create_user_id','id');
    }

}
