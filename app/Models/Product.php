<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
//    protected $connection= 'sqlsrv';
    protected $table="INVMB";
    protected $fillable = [
        "name",
        "unit",
        "unit_price"
    ];

    protected $appends = ['id', 'name', 'specification'];

    public function getIdAttribute(){
        return $this->MB001;
    }

    public function getNameAttribute(){
        return $this->MB002;
    }

    public function getSpecificationAttribute(){
        return $this->MB003;
    }
}
