<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRequestSupplier extends Model
{
    protected $table='order_request_suppliers';
    protected $fillable = [
        "order_request_id",
        "supplier_code",
        "supplier_name",
        "country",
        "area",
        "phone1",
        "phone2",
        "phone2",
        "address1",
        "address2",
        "fax",
        "email",
        "contact_person",
        "_id"
    ];

    public function details(){
        return $this->hasMany('App\Models\ORSupplierDeatil','order_request_supplier_id','id');
    }

    public static function removeSupplier($order_request_id){
        $suppliers = OrderRequestSupplier::where('order_request_id', $order_request_id)->get();
        foreach ($suppliers as $supplier){
            ORSupplierDeatil::where('order_request_supplier_id', $supplier->id)->delete();
            $supplier->delete();
        }
    }
}
