<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRequestApprover extends Model
{
    protected $table="order_request_approver";
    protected $fillable = [
        "order_request_id",
        "user_id",
        "step",
        "status",
        "supplier_id",
        "reason"
    ];
    const REQUEST_STEP_1 = 1;
    const REQUEST_STEP_2 = 2;
    const REQUEST_STEP_3 = 3;
    const REQUEST_STEP_4 = 4;
    const STATUS_CREATED = 'created';
    const STATUS_APPROVE = 'approved';
    const STATUS_REJECT = 'rejected';
    const STATUS_RETURN = 'returned';

    public $appends=['user_name'];
    public function getUserNameAttribute(){
        $user = User::find($this->user_id);
        if($user){
            return $user->name;
        }
        return "";
    }

    public function order_request(){
        return $this->belongsTo('App\Models\OrderRequest','order_request_id','id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public static function createOrderRequestApprover($order_request_id = '', $approver = []){
        foreach ($approver as $user_id){
            OrderRequestApprover::create([
                "order_request_id" => $order_request_id,
                "user_id" => $user_id,
                "step" => self::REQUEST_STEP_1,
                "status" => self::STATUS_CREATED,
            ]);
        }
    }

    public static function createOrderRequestSupplier($order_request_id = '', $approver = ''){
        OrderRequestApprover::create([
            "order_request_id" => $order_request_id,
            "user_id" => $approver,
            "step" => self::REQUEST_STEP_2,
            "status" => self::STATUS_CREATED,
        ]);
    }

    public static function removeSupplier($order_request_id = ''){
        OrderRequestApprover::where('order_request_id', $order_request_id)->where('step', self::REQUEST_STEP_2)->delete();
    }

    public static function createOrderRequestOrder($order_request_id = '', $approver = []){
        foreach ($approver as $user_id) {
            OrderRequestApprover::create([
                "order_request_id" => $order_request_id,
                "user_id" => $user_id,
                "step" => self::REQUEST_STEP_3,
                "status" => self::STATUS_CREATED,
            ]);
        }
    }

    public static function removeOrder($order_request_id = ''){
        OrderRequestApprover::where('order_request_id', $order_request_id)
            ->where('step', self::REQUEST_STEP_3)->delete();
    }

    public static function removeOrderRequestApprover($order_request_id = ''){
        OrderRequestApprover::where('order_request_id',$order_request_id)->where('step', self::REQUEST_STEP_1)->delete();
    }

    public static function createAdvanceApprover($order_request_id = '', $users = ''){
            OrderRequestApprover::create([
                "order_request_id" => $order_request_id,
                "user_id" => $users,
                "step" => self::REQUEST_STEP_4,
                "status" => self::STATUS_CREATED,
            ]);
    }
}
