<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRequestNotify extends Model
{
    protected $table="order_request_notify";
    protected $fillable = [
        "order_request_id",
        "user_id",
        "status"
    ];
    const STATUS_CREATED = 'created';
    const STATUS_SUCCESS = 'success';
    const STATUS_LOCK = 'lock';
    const STATUS_DONE = 'done';
    public static function createOrderRequestNotify($order_request_id = '', $notify = ''){
        foreach ($notify as $user_id){
            OrderRequestNotify::create([
                "order_request_id" => $order_request_id,
                "user_id" => $user_id,
                'status' => self::STATUS_CREATED
            ]);
        }
    }

    public function order_request(){
        return $this->belongsTo('App\Models\OrderRequest','order_request_id','id');
    }
}
