<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use phpDocumentor\Reflection\Types\Self_;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const ROLE_NOTIFY = 3;
    const ROLE_ACCOUNTANT = 4;
    const ROLE_MANAGER = 5;
    const ROLE_SALES = 6;


    public static $roles = [
        self::ROLE_ADMIN => 'Admin',
        self::ROLE_USER => 'User',
        self::ROLE_NOTIFY => 'Purchasing ',
        self::ROLE_ACCOUNTANT => 'Accountant',
        self::ROLE_MANAGER => 'Manager',
        self::ROLE_SALES => 'Sales',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
