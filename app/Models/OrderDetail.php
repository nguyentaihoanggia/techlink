<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderDetail extends Model
{
    protected $table = "order_details";
    protected $fillable = [
        "order_id",
        "request_detail_id",
        "price"
    ];

    public function request_detail(){
        return $this->hasOne('App\Models\OrderRequestDetail','id','request_detail_id');
    }
}
