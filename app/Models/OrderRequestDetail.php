<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRequestDetail extends Model
{
    protected $table="order_request_details";
    protected $fillable = [
        "order_request_id",
        "product_id",
        "product_name",
        "expiry_date",
        "path"
    ];

    public static function createDetail($order_request_id = '', $products = []){
        $total_cost = 0;
        foreach ($products as $product){
            OrderRequestDetail::create([
                "expiry_date" => $product['expiry_date'],
                "order_request_id" => $order_request_id,
                "product_id" => $product['TB004'],
                "product_name" => $product['TB005'],
                "path" => isset($product['path']) ? $product['path'] : '' ,
            ]);
        }
    }

    public static function removeDetail($order_request_id = '')
    {
        OrderRequestDetail::where('order_request_id',$order_request_id)->delete();
    }
}
