<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ORSupplierDeatil extends Model
{
    protected $table='order_request_supplier_details';
    protected $fillable = [
        "order_request_supplier_id",
        "appointment_date",
        "date",
        'appointment_date',
        "order_request_detail_id",
        "price",
        'required_date'
    ];

    public function request_detail(){
        return $this->hasOne('App\Models\OrderRequestDetail','id','order_request_detail_id');
    }

    protected $appends = ['erp'];

    public function getErpAttribute(){
        $detail = $this->request_detail;
        if($detail){
            $order_request = OrderRequest::find($detail->order_request_id);
            if($order_request){
                $arr_code = explode('-', $order_request->code);
                if(count($arr_code) == 2) {
                    $details_erp = DB::connection('sqlsrv')->table('PURTB')
                        ->where('TB001', $arr_code[0])
                        ->where('TB002', $arr_code[1])->get([
                            'TB004',
                            'TB005',
                            'TB006',
                            'TB007',
                            'TB009',
                            'TB011',
                        ]);
                    foreach ($details_erp as $row){
                        if(trim($row->TB004) == trim($detail->product_id) &&
                            trim($row->TB005) == trim($detail->product_name)
                        ){
                            return [
                                'product_id' => $row->TB004,
                                'product_name' => $row->TB005,
                                'quantity' => $row->TB009,
                                'unit' => $row->TB007,
                                'path' => $detail->path,
                                'specification' => $row->TB006
                            ];
                        }
                    }
                }
            }
        }
        return [
            'product_id' => '',
            'product_name' => '',
            'quantity' => '',
            'unit' => '',
            'path' => '',
            'specification' => ''
        ];
    }
}
