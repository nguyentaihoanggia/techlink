<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderRequest extends Model
{
    protected $table = "order_requests";
    protected $fillable = [
        "code",
        "status",
        "create_user_id",
        "note",
        "reason",
        "address_delivery",
        'user_approved',
        'comment'
    ];

    const STATUS_CREATED = 'created';
    const STATUS_APPROVED_1 = 'approved1';
    const STATUS_REJECTED_1 = 'rejected1';
    const STATUS_RETURNED_1 = 'returned1';
    const STATUS_SUPPLIER = 'supplier';
    const STATUS_APPROVED_2 = 'approved2';
    const STATUS_REJECTED_2 = 'rejected2';
    const STATUS_RETURNED_2 = 'returned2';
    const STATUS_SUCCESS = 'success';
    const STATUS_ADVANCE = 'advance';
    const STATUS_REJECTED_3 = 'rejected3';
    const STATUS_APPROVED_3 = 'approved3';
    const STATUS_ADVANCE_SELECTED = 'advance_selected';
    const STATUS_FAIL = 'fail';

//    const STATUS_CREATED = 'created';
//    const STATUS_APPROVER_1 = 'approver1';
//    const STATUS_APPROVER_2 = 'approver2';
//    const STATUS_SUCCESS = 'success';
//    const STATUS_REJECT = 'rejected';
//    const STATUS_SUPPLIER = 'supplier';
//    const STATUS_ORDER = 'order';
//    const STATUS_RETURN = 'return';

    public function user(){
        return $this->belongsTo('App\Models\User','create_user_id','id');
    }

    public function details(){
        return $this->hasMany('App\Models\OrderRequestDetail','order_request_id','id');
    }

    public function approver1(){
        return $this->hasMany('App\Models\OrderRequestApprover','order_request_id','id')
                    ->where('order_request_approver.step',OrderRequestApprover::REQUEST_STEP_1);
    }
    public function approver2(){
        return $this->hasOne('App\Models\OrderRequestApprover','order_request_id','id')
            ->where('order_request_approver.step',OrderRequestApprover::REQUEST_STEP_2);
    }

    public function approver3(){
        return $this->hasMany('App\Models\OrderRequestApprover','order_request_id','id')
            ->where('order_request_approver.step',OrderRequestApprover::REQUEST_STEP_3);
    }

    public function approver4(){
        return $this->hasMany('App\Models\OrderRequestAccountant','order_request_id','id');
    }
    public function approver_advance(){
        return $this->hasOne('App\Models\OrderRequestApprover','order_request_id','id')
            ->where('order_request_approver.step',OrderRequestApprover::REQUEST_STEP_4);
    }

    public function suppliers(){
        return $this->hasMany('App\Models\OrderRequestSupplier','order_request_id','id');
    }

    public function notify(){
        return $this->hasMany('App\Models\OrderRequestNotify','order_request_id','id');
    }

    public function order(){
        return $this->hasOne('App\Models\Order','order_request_id','id');
    }

    public static function createOrderRequest($data = []){
        $data['create_user_id'] = Auth::id();
        $data['status'] = self::STATUS_CREATED;
        $data['total_cost'] = 0;
        return OrderRequest::create($data);
    }

    public static function is_exist($data){
        $cnt = OrderRequest::where('create_user_id', $data['create_user_id'])
            ->where('id', $data['id'])->count();
        return $cnt > 0;
    }

    public static function is_conflict($data){
        $cnt = OrderRequest::where('create_user_id', $data['create_user_id'])
            ->where('id', $data['id'])
            ->where('updated_at', $data['updated_at'])
            ->count();
        return $cnt == 0;
    }

    public static function updateOrderRequest($data){
        $result = [
            'success' => false,
            'request' => []
        ];
        $request = OrderRequest::where('create_user_id', $data['create_user_id'])
            ->where('id', $data['id'])
            ->where('updated_at', $data['updated_at'])
            ->first();
        if($request){
            $request->request_department = $data['request_department'];
            $request->request_code = $data['request_code'];
            $request->use_department = $data['use_department'];
            $request->use_code = $data['use_code'];
            $request->request_date = $data['request_date'];
            $request->note = $data['note'];
            $request->reason = $data['reason'];
            $request->address_delivery = $data['address_delivery'];
            $request->save();
            Log::info('request'.$request->id. 'is changed in '. Date('Ymd His').' by '. Auth::user()->name);
            $result['success'] = true;
            $result['request'] = $request;
        }
        return $result;
    }
}
