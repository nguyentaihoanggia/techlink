<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRequestAccountant extends Model
{
    protected $table="order_request_accountant_approver";
    protected $fillable = [
        "order_request_id",
        "user_id",
        "status",
        "reason"
    ];
    const STATUS_CREATED = 'created';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';
    const STATUS_APPROVED_0 = 'approved0';

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function order_request(){
        return $this->belongsTo('App\Models\OrderRequest','order_request_id','id');
    }

}
