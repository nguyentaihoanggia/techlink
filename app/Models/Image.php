<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    protected $fillable = [
        "path",
        "name",
        "create_user_id",
        "created_at",
        "updated_at"
    ];
}
