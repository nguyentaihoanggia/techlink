<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\OrderRequestHelper;
use App\Helpers\RoleHelper;
use App\Helpers\SendMailHelper;
use App\Http\Response\APIResponse;
use App\Mail\Approver;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderRequest;
use App\Models\OrderRequestAccountant;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestDetail;
use App\Models\OrderRequestNotify;
use App\Models\OrderRequestSupplier;
use App\Models\ORSupplierDeatil;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;

class OrderRequestController extends Controller
{
    public function index(){
        $query = OrderRequest::latest();
        if(Auth::user()->role_id){

        }

        switch (Auth::user()->role_id){
            case User::ROLE_USER:
                $query->where('create_user_id', Auth::id());
                break;
            case User::ROLE_NOTIFY:
                $query->whereHas('notify',function (Builder $query) {
                    $query->where('user_id', Auth::id());
                });
                break;
            case User::ROLE_MANAGER:
                $query->whereHas('approver1',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_1);
                })->orWhereHas('approver2',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_2);
                });
                break;
            case User::ROLE_SALES:
                $query->whereHas('approver_advance',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_4);
                });
                break;
        }


        $total_records = $query->count();
        $limit = config('const.ITEM_PER_PAGE');
        $total = ceil($total_records/ $limit);
        $order_requests = $query->with('user')->skip(0)->take($limit)->get();
        return view("admin.order.index",compact('order_requests','total','total_records'));
    }

    public function getOrderRequest(Request $request){
        $page = (int)$request->post('page', 1);
        $query = OrderRequest::latest();
        switch (Auth::user()->role_id){
            case User::ROLE_USER:
                $query->where('create_user_id', Auth::id());
                break;
            case User::ROLE_NOTIFY:
                $query->whereHas('notify',function (Builder $query) {
                    $query->where('user_id', Auth::id());
                });
                break;
            case User::ROLE_MANAGER:
                $query->whereHas('approver1',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_1);
                })->orWhereHas('approver2',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_2);
                });
                break;
            case User::ROLE_SALES:
                $query->whereHas('approver_advance',function (Builder $query) {
                    $query->where('user_id', Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_4);
                });
                break;
        }
        $total_records = $query->count();
        $limit = config('const.ITEM_PER_PAGE');
        $total = ceil($total_records/ $limit);
        $skip = ($page - 1)*$limit;
        $order_requests = $query->with('user')->skip($skip)->take($limit)->get();
        return response()->json([
            'success' => true,
            'order_requests' => $order_requests,
            'total' => $total,
            'total_records' => $total_records,
            'page' => $page
        ]);
    }

    public function search(){
        return view('admin.order.select');
    }

    public function create(Request $request){
        if(!RoleHelper::createOrderRequest(Auth::user())){
            return abort(403, 'Bạn không có quyền tạo mới phiếu yêu cầu');
        }
        $code = $request->input('code','');
        $message = 'Hãy nhập mã phiếu';
        if($code){
            $local_request = OrderRequest::where('code', trim($code))->first();
            if($local_request){
                return redirect()->route('order-request.show',['id' => $local_request->id]);
            }
            $arr_code = explode('-',$code);
            if(count($arr_code) == 2){
                $query = DB::connection('sqlsrv')->table('PURTA')
                    ->where('TA001', $arr_code[0])
                    ->where('TA002',$arr_code[1])
                    ->where('TA007','Y');
                $order_request = $query->first();
                if($order_request){
                    $details = DB::connection('sqlsrv')->table('PURTB')
                        ->where('TB001', $arr_code[0])
                        ->where('TB002',$arr_code[1])->get([
                            'TB004',
                            'TB005',
                            'TB006',
                            'TB007',
                            'TB009',
                            'TB011',
                        ]);
                    foreach ($details as &$detail){
                        $detail->inventory = DB::connection('sqlsrv')->table('INVMC')
                            ->where('MC001', $detail->TB004)->sum('MC007');
                        $detail->expiry_date = '';
                    }

                    $users = User::all();
                    $departments = DB::connection('sqlsrv')->table('CMSME')->get([
                        'ME001','ME002'
                    ]);
                    $departments->map(function ($item){
                        $item->id = trim($item->ME001);
                        $item->name =  trim($item->ME002);
                        return $item;
                    });
                    return view('admin.order.create', compact('order_request','details','users','departments'));
                }else{
                    $message = 'Không tìm thấy phiếu yêu cầu';
                }
            }else{
                $message = 'Mã phiếu không đúng định dạng';
            }
        }
        return redirect()->back()->withErrors([$message]);
    }

    public function store(Request $request){
        if(!RoleHelper::createOrderRequest(Auth::user())){
            return APIResponse::fail('Bạn không có quyền tạo mới phiếu yêu cầu');
        }
        $validator = Validator::make($request->all(),[
            'products' => 'array|required',
            'approver' =>  'array|required'
        ]);
        if($validator->fails()){
            APIResponse::fail($validator->errors()->first());
        }
        $data = $request->all();
        DB::beginTransaction();
        try {
            $order = OrderRequest::createOrderRequest($data);
            if(isset($order->id)){
                OrderRequestDetail::createDetail($order->id,$data['products']);
                OrderRequestApprover::createOrderRequestApprover($order->id,$data['approver']);
                OrderRequestNotify::createOrderRequestNotify($order->id,$data['notify']);
                DB::commit();
                SendMailHelper::sendMailApproval($data['approver'],$order->id);
                return APIResponse::success($order);
            }
            Log::error('OrderRequestController: Create order request fail');
            DB::rollBack();
            return APIResponse::fail();
        }catch (\Exception $e){
            Log::error('OrderRequestController: '.$e->getMessage());
            DB::rollBack();
            return APIResponse::fail(json_encode($e->getMessage()));
        }
    }

    public function show($id){
        $request = OrderRequest::find($id);
        if($request){
            if(!RoleHelper::showOrderRequest(Auth::user(), $request)){
                return abort('403','Phiếu không tồn tại');
            }
            $request = $request->load([
                'user',
                'details',
                'approver1',
                'approver2',
                'approver3',
                'approver4' => function($q){
                    $q->with('user');
                },
                'approver_advance' => function($q){
                    $q->with('user');
                },
                'suppliers',
                'order'
            ]);
            $users = User::all();
            return view('admin.order.show',compact('request','users'));
        }
        return redirect()->back();
    }

    public function edit($id){
        $request = OrderRequest::where('create_user_id',Auth::id())->where('status', OrderRequest::STATUS_CREATED)->find($id);
        if($request){
            $products = array();
            $details = OrderRequestDetail::where('order_request_id',$request->id)->get();

            foreach ($details as $detail){
                $products[] = [
                    "date" => $detail->date,
                    "original_price" => $detail->original_price,
                    "price" => $detail->price,
                    "product_id" => $detail->product_id,
                    "product_name" => $detail->product_name,
                    "quantity" => $detail->quantity,
                    "inventory" => $detail->inventory,
                    "specification" => $detail->specification,
                    "unit" => $detail->unit,
                    "path" => $detail->path,
                ];
            }

            $request_approver = OrderRequestApprover::where('order_request_id', $request->id)->pluck('user_id');
            $request->products = $products;
            $request->approver = $request_approver;
            $users = User::all();
            $departments = DB::connection('sqlsrv')->table('CMSME')->get([
                'ME001','ME002'
            ]);
            $departments->map(function ($item){
                $item->id = trim($item->ME001);
                $item->name =  trim($item->ME002);
                return $item;
            });
            return view('admin.order.edit',compact('request','users','departments'));
        }
        return redirect()->back();
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'products' => 'array|required',
            'approver' =>  'array|required'
        ]);
        if($validator->fails()){
            APIResponse::fail($validator->errors()->first());
        }
        $data = $request->all();
        $is_exist = OrderRequest::is_exist($data);
        if($is_exist){
            $is_conflict = OrderRequest::is_conflict($data);
            if($is_conflict){
                return APIResponse::fail('Dữ liệu đã bị thay đổi, vui lòng F5 để tải lại trang');
            }
        }else{
            return APIResponse::fail('Không tìm thấy yêu cầu');
        }
        DB::beginTransaction();
        try {
            $result = OrderRequest::updateOrderRequest($data);
            if($result['success']){
                $order = $result['request'];
                OrderRequestDetail::removeDetail($order->id);
                OrderRequestDetail::createDetail($order->id,$data['products']);
                OrderRequestApprover::removeOrderRequestApprover($order->id);
                OrderRequestApprover::createOrderRequestApprover($order->id,$data['approver']);
                DB::commit();
                SendMailHelper::sendMailApproval($data['approver'],$order->id);
                return APIResponse::success();
            }
            Log::error('OrderRequestController: Create order request fail');
            DB::rollBack();
            return APIResponse::fail();
        }catch (\Exception $e){
            Log::error('OrderRequestController: '.$e->getMessage());
            DB::rollBack();
            return APIResponse::fail(json_encode($e->getMessage()));
        }
    }

    public function detail($id){
        $request = OrderRequest::where('status','!=', OrderRequest::STATUS_CREATED)->find($id);
        if($request){
            if(!RoleHelper::showOrderRequest(Auth::user(), $request)){
                return abort('403','Phiếu không tồn tại');
            }
            $arr_code = explode('-', $request->code);
            if(count($arr_code) == 2) {
                $query = DB::connection('sqlsrv')->table('PURTA')
                    ->where('TA001', $arr_code[0])
                    ->where('TA002',$arr_code[1])
                    ->where('TA007','Y');
                $order_request = $query->first();

                $products = array();
                $details = OrderRequestDetail::where('order_request_id', $request->id)->get();
                $erp_details = DB::connection('sqlsrv')->table('PURTB')
                    ->where('TB001', $arr_code[0])
                    ->where('TB002',$arr_code[1])->get([
                        'TB004',
                        'TB005',
                        'TB006',
                        'TB007',
                        'TB009',
                        'TB011',
                    ]);
                foreach ($details as $detail) {
                    $product = [];
                    $inventory = DB::connection('sqlsrv')->table('INVMC')
                        ->where('MC001', $detail->product_id)->sum('MC007');
                    foreach ($erp_details as $row){
                        if(trim($detail->product_id) == trim($row->TB004) &&
                            trim($detail->product_name) == trim($row->TB005)
                        ){
                            $product['product_id'] = $detail->product_id;
                            $product['product_name'] = $detail->product_name;
                            $product['inventory'] = $inventory;
                            $product['quantity'] = $row->TB009;
                            $product['specification'] = $row->TB006;
                            $product['unit'] = $row->TB007;
                            $product['path'] = $detail->path;
                            $product['date'] = $detail->date;
                            $product['date'] = $row->TB011;
                        }
                    }
                    $products[] = $product;
                }

                $request_approver = OrderRequestApprover::where('order_request_id', $request->id)->pluck('user_id');
                $request_notify = OrderRequestNotify::where('order_request_id', $request->id)->pluck('user_id');
                $request->products = $products;
                $request->approver = $request_approver;
                $request->notify = $request_notify;
                $users = User::all();
                $departments = DB::connection('sqlsrv')->table('CMSME')->get([
                    'ME001', 'ME002'
                ]);
                $departments->map(function ($item) {
                    $item->id = trim($item->ME001);
                    $item->name = trim($item->ME002);
                    return $item;
                });
            }
            return view('admin.order.detail',compact('request','users','departments','order_request'));
        }
        return redirect()->back();
    }

    public function approval($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $arr_code = explode('-', $order_request->code);
            if(count($arr_code) == 2) {
                $user = Auth::user();
                $approver = OrderRequestApprover::where('user_id',$user->id)->where('order_request_id',$order_request->id)->first();
                if($approver){
                    $query = DB::connection('sqlsrv')->table('PURTA as a')
                        ->leftJoin('CMSME as e', 'a.TA004','=','e.ME001')
                        ->where('TA001', $arr_code[0])
                        ->where('TA002', $arr_code[1])
                        ->where('TA007', 'Y');
                    $order_request_erp = $query->select('a.*', 'e.ME002')->first();
                    $details_erp = DB::connection('sqlsrv')->table('PURTB')
                        ->where('TB001', $arr_code[0])
                        ->where('TB002',$arr_code[1])->select(
                                'TB004',
                                'TB005',
                                'TB006',
                                'TB007',
                                'TB009',
                                'TB011'
                        )->get();
                    if($order_request_erp){
                        $order_request->request_department = $order_request_erp->ME002;
                        $order_request->note = $order_request_erp->TA006;
                        $order_request->request_code = $order_request_erp->TA004;
                        $order_request->request_date = $order_request_erp->TA003;
                        $details = OrderRequestDetail::where('order_request_id', $order_request->id)->get();
                        foreach ($details as &$detail){
                            foreach ($details_erp as $row) {
                                if(trim($detail->product_id) == trim($row->TB004) &&
                                    trim($detail->product_name) == trim($row->TB005)
                                ){
                                    $detail->product_name = $row->TB005;
                                    $detail->specification = $row->TB006;
                                    $detail->unit = $row->TB007;
                                    $detail->quantity = $row->TB009;
                                    $detail->date = $row->TB011;
                                    $detail->inventory = DB::connection('sqlsrv')->table('INVMC')
                                        ->where('MC001', $detail->product_id)->sum('MC007');
                                }
                            }
                        }
                        return view('admin.approval.index', compact('order_request', 'approver', 'details'));
                    }
                }
            }
        }
        abort(403);
    }

    public function updateApproval($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $status = $request->status;
            $reason = trim($request->reason);
            $user = Auth::user();
            $approver = OrderRequestApprover::where('user_id',$user->id)->where('order_request_id',$order_request->id)->first();
            if($approver){
                $approver->update([
                    'status' => $status,
                    'reason' => $reason
                ]);
                OrderRequestHelper::checkOrderRequest($approver);
                return APIResponse::success();
            }
        }
        return APIResponse::fail('Order Request not found');
    }

    public function supplier($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!RoleHelper::createSupplier(Auth::user(), $order_request)){
                return abort('403', 'Bạn không có quyền vào trang này');
            }
            $arr_code = explode('-', $order_request->code);
            if(count($arr_code) == 2) {
                if ($order_request->create_user_id == Auth::id()) {
                    $is_admin = true;
                } else {
                    $is_admin = false;
                }
                if ($order_request->status == OrderRequest::STATUS_APPROVED_1) {
                    $query = DB::connection('sqlsrv')->table('PURTA as a')
                        ->leftJoin('CMSME as e', 'a.TA004','=','e.ME001')
                        ->where('TA001', $arr_code[0])
                        ->where('TA002', $arr_code[1])
                        ->where('TA007', 'Y');
                    $order_request_erp = $query->select('a.*', 'e.ME002')->first();
                    $details_erp = DB::connection('sqlsrv')->table('PURTB')
                        ->where('TB001', $arr_code[0])
                        ->where('TB002',$arr_code[1])->get([
                            'TB004',
                            'TB005',
                            'TB006',
                            'TB007',
                            'TB009',
                            'TB011',
                        ]);
                    if($order_request_erp){
                        $order_request->request_department = $order_request_erp->ME002;
                        $order_request->note = $order_request_erp->TA006;
                        $order_request->request_code = $order_request_erp->TA004;
                        $order_request->request_date = $order_request_erp->TA003;
                        $details = OrderRequestDetail::where('order_request_id', $order_request->id)->get();
                        foreach ($details as &$detail){
                            foreach ($details_erp as $row) {
                                if(trim($detail->product_id) == trim($row->TB004) &&
                                    trim($detail->product_name) == trim($row->TB005)
                                ){
                                    $detail->product_name = $row->TB005;
                                    $detail->specification = $row->TB006;
                                    $detail->unit = $row->TB007;
                                    $detail->quantity = $row->TB009;
                                    $detail->date = $row->TB011;
                                    $detail->inventory = DB::connection('sqlsrv')->table('INVMC')
                                        ->where('MC001', $detail->product_id)->sum('MC007');
                                }
                            }
                        }
                        $users = User::all();
                        return view('admin.order.supplier', compact('order_request','details', 'users', 'is_admin'));
                    }
                }
            }
        }
        abort(404);
    }

    public function addSupplier($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request && $order_request->status == OrderRequest::STATUS_APPROVED_1){
            if(!RoleHelper::createSupplier(Auth::user(), $order_request)){
                return APIResponse::fail('Bạn không có quyền vào trang này');
            }
            $validator = Validator::make($request->all(), [
                'approver' => 'required',
                'suppliers' => 'required|array',
                'suppliers.*.supplier_name' => 'required|min:1',
                'suppliers.*.supplier_code' => 'required',
            ]);
            if($validator->fails()){
                return APIResponse::fail($validator->errors()->first());
            }
            DB::beginTransaction();
            $data = $request->all();
            try {
                foreach ($data['suppliers'] as $data_supplier){
                    $data_supplier['order_request_id'] = $id;
                    $supplier = OrderRequestSupplier::create($data_supplier);
                    foreach ($data_supplier['details'] as $detail){
                        $detail['order_request_supplier_id'] = $supplier->id;
                        $detail['order_request_detail_id'] = $detail['detail_id'];
                        ORSupplierDeatil::create($detail);
                    }
                }
                $order_request->status = OrderRequest::STATUS_SUPPLIER;
                $order_request->save();
                $notify = OrderRequestNotify::where('order_request_id',$id)->update([
                    'status' => OrderRequestNotify::STATUS_LOCK
                ]);
                OrderRequestApprover::createOrderRequestSupplier($id,$data['approver']);
                SendMailHelper::sendMailApprovalSupplier($data['approver'],$id);
                DB::commit();
                return APIResponse::success();
            }catch (\Exception $e){
                Log::error('OrderRequestController: '.$e->getMessage());
                DB::rollBack();
                return APIResponse::fail();
            }
        }
        return APIResponse::fail('Something went wrong');
    }

    public function editSupplier($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!RoleHelper::createSupplier(Auth::user(), $order_request)){
                return abort('403', 'Bạn không có quyền vào trang này');
            }
            if($order_request->status == OrderRequest::STATUS_RETURNED_2){
                $arr_code = explode('-', $order_request->code);
                if(count($arr_code) == 2) {
                    $query = DB::connection('sqlsrv')->table('PURTA as a')
                        ->leftJoin('CMSME as e', 'a.TA004','=','e.ME001')
                        ->where('TA001', $arr_code[0])
                        ->where('TA002', $arr_code[1])
                        ->where('TA007', 'Y');
                    $order_request_erp = $query->select('a.*', 'e.ME002')->first();
                    $details_erp = DB::connection('sqlsrv')->table('PURTB')
                        ->where('TB001', $arr_code[0])
                        ->where('TB002',$arr_code[1])->get([
                            'TB004',
                            'TB005',
                            'TB006',
                            'TB007',
                            'TB009',
                            'TB011',
                        ]);
                    if($order_request_erp){
                        $order_request->request_department = $order_request_erp->ME002;
                        $order_request->note = $order_request_erp->TA006;
                        $order_request->request_code = $order_request_erp->TA004;
                        $order_request->request_date = $order_request_erp->TA003;
                        $details = OrderRequestDetail::where('order_request_id', $order_request->id)->get();
                        foreach ($details as &$detail){
                            foreach ($details_erp as $row) {
                                if(trim($detail->product_id) == trim($row->TB004) &&
                                    trim($detail->product_name) == trim($row->TB005)
                                ){
                                    $detail->product_name = $row->TB005;
                                    $detail->specification = $row->TB006;
                                    $detail->unit = $row->TB007;
                                    $detail->quantity = $row->TB009;
                                    $detail->date = $row->TB011;
                                    $detail->inventory = DB::connection('sqlsrv')->table('INVMC')
                                        ->where('MC001', $detail->product_id)->sum('MC007');
                                }
                            }
                        }
                        $users = User::all();
                        $data_approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                            ->where('step',OrderRequestApprover::REQUEST_STEP_2)->first();
                        $approver = '';
                        if($data_approver){
                            $approver = $data_approver->user_id;
                        }
                        $suppliers = OrderRequestSupplier::where('order_request_id',$order_request->id)->with(['details' => function ($q){
                            $q->with('request_detail');
                        }])->get();
                        return view('admin.order.supplier_edit', compact('suppliers','approver','order_request','details', 'users', 'is_admin'));
                    }
                }
            }
        }
        abort(404);
    }

    public function updateSupplier($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!RoleHelper::createSupplier(Auth::user(), $order_request)){
                return APIResponse::fail('Bạn không có quyền vào trang này');
            }
            $validator = Validator::make($request->all(), [
                'approver' => 'required',
                'suppliers' => 'required|array',
                'suppliers.*.supplier_name' => 'required|min:1',
                'suppliers.*.supplier_code' => 'required'
            ]);
            if($validator->fails()){
                return APIResponse::fail($validator->errors()->first());
            }
            $data = $request->all();
            DB::beginTransaction();
            try {
                OrderRequestSupplier::removeSupplier($id);
                OrderRequestApprover::removeSupplier($id);
                foreach ($data['suppliers'] as $data_supplier){
                    $data_supplier['order_request_id'] = $id;
                    $supplier = OrderRequestSupplier::create($data_supplier);
                    foreach ($data_supplier['details'] as $detail){
                        $detail['order_request_supplier_id'] = $supplier->id;
                        $detail['order_request_detail_id'] = $detail['detail_id'];
                        ORSupplierDeatil::create($detail);
                    }
                }
                $order_request->status = OrderRequest::STATUS_SUPPLIER;
                $order_request->save();
                OrderRequestApprover::createOrderRequestSupplier($id,$data['approver']);
                SendMailHelper::sendMailApprovalSupplier($data['approver'],$id);
                DB::commit();
                return APIResponse::success();
            }catch (\Exception $e){
                Log::error('OrderRequestController: '.$e->getMessage());
                Log::error($e->getTraceAsString());
                DB::rollBack();
                return APIResponse::fail();
            }
        }
        return APIResponse::fail('Something went wrong');
    }

    public function detailSupplier($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!RoleHelper::showOrderRequest(Auth::user(), $order_request)){
                return abort(403,'Bạn không có quyền vào trang này');
            }
            $order_request->load(['details']);
            $users = User::all();
            $suppliers = OrderRequestSupplier::where('order_request_id',$order_request->id)->with(['details' => function ($q){
                $q->with('request_detail');
            }])->get();
            if(!$suppliers->isEmpty()){
                $data_approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                    ->where('step',OrderRequestApprover::REQUEST_STEP_2)->first();
                $approver = '';
                if($data_approver){
                    $approver = $data_approver->user_id;
                }
                return view('admin.order.supplier_detail',compact('order_request','users','suppliers','approver'));
            }
        }
        abort(404);
    }

    public function approvalSupplier($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $suppliers = OrderRequestSupplier::where('order_request_id',$order_request->id)->with(['details'])->get();
            $approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                ->where('user_id',Auth::id())->where('step',OrderRequestApprover::REQUEST_STEP_2)->first();
            if($approver){
                return view('admin.approval.supplier',compact('suppliers','approver','order_request'));
            }
        }
        abort(403);
    }

    public function updateApprovalSupplier($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $validator = Validator::make($request->all(),[
                'status' => 'required|in:'.OrderRequestApprover::STATUS_REJECT.','.OrderRequestApprover::STATUS_APPROVE,','.OrderRequestApprover::STATUS_RETURN,
                'reason' => 'required_if:status,'.OrderRequestApprover::STATUS_REJECT,
                'id'     => 'required_if:status,'.OrderRequestApprover::STATUS_APPROVE
            ]);
            $status = $request->status;
            $supplier_id = $request->supplier_id;
            $reason = trim($request->reason);
            $user = Auth::user();
            $approver = OrderRequestApprover::where('user_id',$user->id)
                        ->where('order_request_id',$order_request->id)
                        ->where('step',OrderRequestApprover::REQUEST_STEP_2)
                        ->where('is_deleted',0)->first();
            if($approver){
                $total_cost = 0;
                $suppliers = OrderRequestSupplier::where('id',$supplier_id)->with(['details' => function ($q){
                    $q->with('request_detail');
                }])->first();
                if($suppliers){
                    foreach ($suppliers->details as $detail){
                        if(!empty($detail['erp'])){
                            $total_cost += (int)$detail['erp']['quantity'] * $detail->price;
                        }
                    }
                }
                $approver->update([
                    'status' => $status,
                    'reason' => $reason,
                    'supplier_id' => $supplier_id
                ]);

                OrderRequestHelper::checkSupplier($order_request->id, $total_cost);
                return APIResponse::success();
            }
        }
        return APIResponse::fail('Order Request not found');
    }

    public function order($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if($order_request->status == OrderRequest::STATUS_APPROVER_2){
                $approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                            ->where('step',OrderRequestApprover::REQUEST_STEP_2)
                            ->where('is_deleted',0)
                            ->where('status',OrderRequestApprover::STATUS_APPROVE)
                            ->first();
                if($approver){
                    $supplier = OrderRequestSupplier::where('order_request_id',$order_request->id)
                                ->where('id',$approver->supplier_id)->with(['details' => function($q){
                                    $q->with('request_detail');
                                }])
                                ->first();
                    if($supplier){
                        $users = User::all();
                        return view('admin.order.order',compact('supplier','users'));
                    }
                }
            }
        }
        abort(404);
    }

    public function orderCreate($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request && $order_request->status == OrderRequest::STATUS_APPROVER_2) {
            $validator = Validator::make($request->all(), [
                'approvers' => 'required',
                'order.supplier_id' => 'required',
                'order.supplier_code' => 'required',
                'order.supplier_name' => 'required',
                'order.details' => 'required|array'
            ]);
            if ($validator->fails()) {
                return APIResponse::fail($validator->errors()->first());
            }
            DB::beginTransaction();
            $data = $request->all();
            try {
                $data['order']['order_request_id'] = $id;
                $data['order']['code'] = Order::setCode();
                $data['order']['create_user_id'] = Auth::id();
                $order = Order::create($data['order']);
                foreach ($data['order']['details'] as $detail){
                    $detail['order_id'] = $order->id;
                    $detail['request_detail_id'] = $detail['order_request_detail_id'];
                    OrderDetail::create($detail);
                }
                $order_request->status = OrderRequest::STATUS_ORDER;
                $order_request->save();
                OrderRequestApprover::createOrderRequestOrder($id,$data['approvers']);
                SendMailHelper::sendMailApprovalOrder($data['approvers'], $id, $order->id);
                DB::commit();
                return APIResponse::success();
            } catch (\Exception $e) {
                Log::error('OrderRequestController: ' . $e->getMessage());
                DB::rollBack();
                return APIResponse::fail();
            }
        }else{
            return APIResponse::fail('Dữ liệu đã được cập nhật');
        }
    }

    public function editOrder($id, $order_id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if($order_request->status == OrderRequest::STATUS_ORDER){
                $users = User::all();
                $order = Order::where('id',$order_id)
                    ->where('order_request_id',$id)
                    ->with(['details' => function($q){
                        $q->with('request_detail');
                    },'created_by','supplier'])
                    ->first();
                $approvers = [];
                $data_approvers = OrderRequestApprover::where('order_request_id',$order_request->id)
                    ->where('step',OrderRequestApprover::REQUEST_STEP_3)
                    ->where('is_deleted',0)
                    ->get(['user_id'])->toArray();
                if($data_approvers){
                    $approvers = array_column($data_approvers,'user_id');
                }
                return view('admin.order.order_edit',compact('order','users','approvers'));
            }
        }
        abort(404);
    }

    public function detailOrder($id, $order_id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $users = User::all();
            $order = Order::where('id',$order_id)
                ->where('order_request_id',$id)
                ->with(['details' => function($q){
                    $q->with('request_detail');
                },'created_by','supplier'])
                ->first();
            $approvers = [];
            if($order){
                $data_approvers = OrderRequestApprover::where('order_request_id',$order_request->id)
                    ->where('step',OrderRequestApprover::REQUEST_STEP_3)
                    ->where('is_deleted',0)
                    ->get(['user_id'])->toArray();
                if($data_approvers){
                    $approvers = array_column($data_approvers,'user_id');
                }
                return view('admin.order.order_detail',compact('order','users','approvers'));
            }
        }
        abort(404);
    }

    public function updateOrder($id, $order_id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request) {
            $validator = Validator::make($request->all(), [
                'approvers' => 'required',
                'order.supplier_id' => 'required',
                'order.supplier_code' => 'required',
                'order.supplier_name' => 'required',
                'order.details' => 'required|array'
            ]);
            if ($validator->fails()) {
                return APIResponse::fail($validator->errors()->first());
            }
            $data = $request->all();
            DB::beginTransaction();
            try {
                $order = Order::find($order_id);
                if($order){
                    foreach ($data['order']['details'] as $detail){
                        $order_detail = OrderDetail::find($detail['id']);
                        if($order_detail){
                            $order_detail->price = $detail['price'];
                            $order_detail->updated_at = Date('Y-m-d H:i:s');
                            $order_detail->save();
                        }
                    }
                    OrderRequestApprover::removeOrder($id);
                    OrderRequestApprover::createOrderRequestOrder($id,$data['approvers']);
                    SendMailHelper::sendMailApprovalOrder($data['approvers'], $id, $order->id);
                    DB::commit();
                    return APIResponse::success();
                }else{
                    DB::rollBack();
                    return APIResponse::fail("order not found");
                }

            } catch (\Exception $e) {
                Log::error('OrderRequestController: ' . $e->getMessage());
                DB::rollBack();
                return APIResponse::fail();
            }
        }
    }

    public function orderApproval($id, $order_id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                ->where('step',OrderRequestApprover::REQUEST_STEP_3)
                ->where('is_deleted',0)
                ->where('user_id',Auth::id())
                ->first();
            if($approver){
                $order = Order::where('id',$order_id)
                    ->where('order_request_id',$id)
                    ->with(['details' => function($q){
                        $q->with('request_detail');
                    },'created_by','supplier'])
                    ->first();
                if($order){
                    return view('admin.approval.order',compact('order','approver','order_request'));
                }
            }
        }
        abort(404);
    }

    public function updateOrderApproval($id, $order_id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $validator = Validator::make($request->all(),[
                'status' => 'required|in:'.OrderRequestApprover::STATUS_REJECT.','.OrderRequestApprover::STATUS_APPROVE,
                'reason' => 'required_if:status,'.OrderRequestApprover::STATUS_REJECT
            ]);
            $status = $request->status;
            $reason = trim($request->reason);
            $user = Auth::user();
            $approver = OrderRequestApprover::where('user_id',$user->id)
                ->where('order_request_id',$order_request->id)
                ->where('step',OrderRequestApprover::REQUEST_STEP_3)
                ->where('is_deleted',0)->first();
            if($approver){
                $approver->update([
                    'status' => $status,
                    'reason' => $reason
                ]);
                $order = Order::where('order_request_id',$id)->where('id',$order_id)->update(['status' => $status]);
                OrderRequestHelper::checkOrder($order_request->id);
                return APIResponse::success();
            }
        }
        return APIResponse::fail('Order Request not found');
    }

    public function printOrder($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!RoleHelper::showOrderRequest(Auth::user(), $order_request)){
                return abort('404','Bạn không có quyền vào trang này');
            }
            $users = User::all();
            $approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                ->where('step',OrderRequestApprover::REQUEST_STEP_2)
                ->where('status',OrderRequestApprover::STATUS_APPROVE)
                ->where('is_deleted',0)->first();
            if($approver){
                $supplier = OrderRequestSupplier::with('details')->find($approver->supplier_id);
//                dd($supplier->toArray());
                if($supplier){
                    $arr_code = explode('-', $order_request->code);
                    if(count($arr_code) == 2) {
                        $query = DB::connection('sqlsrv')->table('PURTA as a')
                            ->leftJoin('CMSME as e', 'a.TA004', '=', 'e.ME001')
                            ->where('TA001', $arr_code[0])
                            ->where('TA002', $arr_code[1])
                            ->where('TA007', 'Y');
                        $order_request_erp = $query->select('a.*', 'e.ME002')->first();
                        $details_erp = DB::connection('sqlsrv')->table('PURTB')
                            ->where('TB001', $arr_code[0])
                            ->where('TB002', $arr_code[1])->get([
                                'TB004',
                                'TB005',
                                'TB006',
                                'TB007',
                                'TB009',
                                'TB011',
                            ]);
                        if ($order_request_erp) {
                            $order_request->request_department = $order_request_erp->ME002;
                            $order_request->note = $order_request_erp->TA006;
                            $order_request->request_code = $order_request_erp->TA004;
                            $order_request->request_date = $order_request_erp->TA003;
                            $madeby = '';
                            $approval = '';
                            $manager = '';
                            $accountant = '';
                            $hk = '';
                            if($order_request->user){
                                $madeby = $order_request->user->name;
                            }
                            $user_approval = User::find($order_request->user_approved);
                            if($user_approval){
                                $approval = $user_approval->name;
                            }
                            if($order_request->approver2){
                                $user_2 = User::find($order_request->approver2->user_id);
                                if($user_2){
                                    $manager = $user_2->name;
                                }
                            }

                            $accountant = OrderRequestAccountant::where('order_request_id', $order_request->id)
                                ->where('status', OrderRequestAccountant::STATUS_APPROVED)->first();
                            if($accountant){
                                $user_3 = User::find($accountant->user_id);
                                if($user_3){
                                    $accountant = $user_3->name;
                                }
                            }

                            if($order_request->approver_advance){
                                $user_a = User::find($order_request->approver_advance->user_id);
                                if($user_a){
                                    $hk = $user_a->name;
                                }
                            }

                            return view('admin.print.index',
                                compact('order_request',
                                    'supplier',
                                    'approval',
                                    'madeby',
                                    'manager',
                                    'accountant',
                                    'hk'
                                )
                            );
                        }
                    }
                }
            }
//            $order = Order::where('id',$order_id)
//                ->where('order_request_id',$id)
//                ->with(['details' => function($q){
//                    $q->with('request_detail');
//                },'created_by','supplier'])
//                ->first();
//            $approvers = [];
//            if($order){
//                $data_approvers = OrderRequestApprover::where('order_request_id',$order_request->id)
//                    ->where('step',OrderRequestApprover::REQUEST_STEP_3)
//                    ->where('is_deleted',0)
//                    ->get(['user_id'])->toArray();
//                if($data_approvers){
//                    $approvers = array_column($data_approvers,'user_id');
//                }
//                return view('admin.print.index',compact('order','users','approvers'));
//            }
        }
        abort(404);
    }

    public function selectAdvanceUser(Request $request, $id){
        $users = $request->input('approvers', '');
        $order_request = OrderRequest::find($id);
        if($order_request->status == OrderRequest::STATUS_ADVANCE){
            if(!RoleHelper::createSupplier(Auth::user(), $order_request)){
                return APIResponse::fail('Bạn không có quyền vào trang này');
            }
            if($users){
                // create Approver
                DB::beginTransaction();
                try {
                    OrderRequestApprover::createAdvanceApprover($order_request->id, $users);

                    DB::commit();
                    //send mail
                    SendMailHelper::sendMailAdvance($users, $order_request->id);
                    $order_request->status = OrderRequest::STATUS_ADVANCE_SELECTED;
                    $order_request->save();
                    return APIResponse::success();
                }catch (\Exception $e){
                    Log::error('OrderRequestController: '.$e->getMessage());
                    DB::rollBack();
                    return APIResponse::fail('Thực hiện thao tác không thành công');
                }

            }else{
                return APIResponse::fail('Chưa chọn người kiểm duyệt');
            }
        }else{
            return APIResponse::fail('Thực hiện thao tác không thành công');
        }

    }
}
