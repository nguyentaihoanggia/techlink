<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($this->checkAdmin()){
            $query = User::latest();
            if($request->get('is_delete') == 1){
                $query->onlyTrashed();
            }
            $users = $query->paginate(10);
            return view('admin.user.index',compact('users'));
        }
        return abort(403);
    }

    public function restore($id){
        $user = User::onlyTrashed()->where('id', $id)->first();
        if($user){
            $user->restore();
            flash('Khôi phục '.$user->name.' thành công');
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors([
                'User not found'
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if($this->checkAdmin()) {
            $roles = User::$roles;
            return view('admin.user.create', compact('roles'));
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(UserCreateRequest $request)
    {
        if($this->checkAdmin()) {
            $data = $request->all();
            $data['password'] = bcrypt($data['password']);
            User::create($data);
            flash('Success');
            return redirect()->to(route('user.index'));
        }
        return abort(403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($this->checkAdmin()) {
            $user = User::find($id);
            if ($user) {
                $roles = User::$roles;
                return view('admin.user.edit', compact('user', 'roles', 'id'));
            }
            return abort('404');
        }
        return abort('403');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'nullable|min:6'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        if($this->checkAdmin()) {
            $data = $request->only(['name', 'role_id','password']);
            if(empty($data['password'])){
                unset($data['password']);
            }else{
                $data['password'] = bcrypt($data['password']);
            }
            $user = User::find($id);
            if ($user) {
                $user->update($data);
                flash('Cập nhật thông tin thành công');
                return redirect()->back();
            }
            return redirect()->back()->withErrors(['Thực hiện thao tác không thành công']);
        }
        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->checkAdmin()) {
            $user = User::find($id);
            if ($user) {
                $user->delete();
            }
            flash('Xóa thành công');
            return redirect()->back();
        }
        return abort(403);
    }

    public function checkAdmin(){
        return Auth::user()->role_id == User::ROLE_ADMIN;
    }
}
