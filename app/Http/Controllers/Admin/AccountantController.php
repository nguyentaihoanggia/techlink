<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\OrderRequestHelper;
use App\Http\Response\APIResponse;
use App\Models\Order;
use App\Models\OrderRequest;
use App\Models\OrderRequestAccountant;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestSupplier;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class AccountantController extends Controller
{
    public function index(){
        if(Auth::user()->role_id == User::ROLE_ACCOUNTANT){
            $query = OrderRequestAccountant::latest();
            $query->where('user_id',Auth::id())->where('is_deleted',0);
            $query->with(['order_request' => function($q){
                $q->with(['order','user']);
            },'user']);

            $total_records = $query->count();
            $limit = config('const.ITEM_PER_PAGE');
            $total = ceil($total_records/ $limit);
            $list_approval = $query->with('user')->skip(0)->take($limit)->get();
            return view('admin.view_approval.accountant',compact('list_approval','total','total_records'));
        }else{
            return abort(403);
        }

    }

    public function getOrderRequestAccountant(Request $request){
        $status = $request->input('status', '');
        $page= (int)$request->input('page', 1);
        $query = OrderRequestAccountant::latest();
        $query->where('user_id',Auth::id())->where('is_deleted',0);
        $query->with(['order_request' => function($q){
            $q->with(['order','user']);
        },'user']);
        if($status != ''){
            $query->where('status',$status);
        }
        $total_records = $query->count();
        $limit = config('const.ITEM_PER_PAGE');
        $total = ceil($total_records/ $limit);
        $skip = ($page - 1)*$limit;
        $list_approval = $query->with('user')->skip($skip)->take($limit)->get();
        return response()->json([
            'success' => true,
            'list_approval' => $list_approval,
            'total' => $total,
            'total_records' => $total_records,
            'page' => $page
        ]);
    }

    public function accountant($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!empty($order_request->approver2)){
                $supplier_id = $order_request->approver2->supplier_id;
                $supplier = OrderRequestSupplier::with('details')->find($supplier_id);
                if ($supplier){
                    $approver = OrderRequestAccountant::where('order_request_id',$order_request->id)
                        ->where('user_id',Auth::id())->first();
                    if($approver){
                        return view('admin.approval.accountant',compact('supplier','approver','order_request'));
                    }
                }
            }

        }
        abort(403);
    }

    public function approval($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $status = $request->status;
            $reason = trim($request->reason);
            $user = Auth::user();
            $approver = OrderRequestAccountant::where('user_id',$user->id)
                ->where('order_request_id',$order_request->id)
                ->where('is_deleted',0)->first();
            if($approver){
                $approver->update([
                    'status' => $status,
                    'reason' => $reason,
                ]);
                $total_cost = 0;
                $suppliers = OrderRequestSupplier::where('id',$order_request->approver2->supplier_id)->with(['details' => function ($q){
                    $q->with('request_detail');
                }])->first();
                if($suppliers){
                    foreach ($suppliers->details as $detail){
                        if(!empty($detail['erp'])){
                            $total_cost += (int)$detail['erp']['quantity'] * $detail->price;
                        }
                    }
                }
                OrderRequestHelper::checkAccountant($approver,$total_cost);
                return APIResponse::success();
            }
        }
        return APIResponse::fail('Order Request not found');
    }

    public function action(Request $request){
        $arr_status = [OrderRequestAccountant::STATUS_REJECTED, OrderRequestAccountant::STATUS_APPROVED];
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:'.implode(',',$arr_status)
        ]);
        if($validator->fails()){
            return APIResponse::fail($validator->errors()->first());
        }
        $data = $request->all();
        $ids = $data['ids'];
        $response = [
            'success' => [

            ],
            'fail' => [

            ]
        ];
        foreach ($ids as $id){
            $approver = OrderRequestAccountant::find($id);
            if($approver){
                if($approver->status == OrderRequestAccountant::STATUS_CREATED){
                    $approver->update([
                        'status' => $data['status'],
                        'reason' => $data['reason']
                    ]);
                    OrderRequestHelper::checkAccountant($approver);
                    $response['success'][] = $approver->order_request ? $approver->order_request->code : '';
                }else{
                    $response['fail'][] = $approver->order_request ? $approver->order_request->code : '';
                }
            }
        }
        return APIResponse::success($response);
    }
}
