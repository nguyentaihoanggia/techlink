<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\OrderRequestHelper;
use App\Http\Response\APIResponse;
use App\Models\OrderRequest;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestSupplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdvanceController extends Controller
{
    public function advance($id){
        $order_request = OrderRequest::find($id);
        if($order_request){
            if(!empty($order_request->approver2)){
                $supplier_id = $order_request->approver2->supplier_id;
                $supplier = OrderRequestSupplier::with('details')->find($supplier_id);
                if ($supplier){
                    $approver = OrderRequestApprover::where('order_request_id',$order_request->id)
                        ->where('step', OrderRequestApprover::REQUEST_STEP_4)
                        ->where('user_id',Auth::id())->first();
                    if($approver){
                        return view('admin.approval.advance',compact('supplier','approver','order_request'));
                    }
                }
            }

        }
        abort(403);
    }

    public function approval($id, Request $request){
        $order_request = OrderRequest::find($id);
        if($order_request){
            $status = $request->status;
            $reason = trim($request->reason);
            $user = Auth::user();
            $approver = OrderRequestApprover::where('user_id',$user->id)
                ->where('step',OrderRequestApprover::REQUEST_STEP_4)
                ->where('order_request_id',$order_request->id)
                ->where('is_deleted',0)->first();
            if($approver){
                $approver->update([
                    'status' => $status,
                    'reason' => $reason,
                ]);
                OrderRequestHelper::checkAdvance($approver);
                return APIResponse::success();
            }
        }
        return APIResponse::fail('Order Request not found');
    }
}
