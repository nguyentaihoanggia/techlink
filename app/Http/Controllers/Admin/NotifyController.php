<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderRequestNotify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotifyController extends Controller
{
    public function index(){
        $notify = OrderRequestNotify::latest()->where('user_id', Auth::id());
        $notify->where('status','!=',OrderRequestNotify::STATUS_CREATED)->with('order_request');
        $notify = $notify->paginate(10);
        return view('admin.notify.index', compact('notify'));
    }
}
