<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderRequest;
use App\Models\OrderRequestAccountant;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestDetail;
use App\Models\OrderRequestSupplier;
use App\Models\ORSupplierDeatil;
use Carbon\Traits\Date;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(Request $request){
        $startDate = strtotime(Date('Y-m-01 00:00:00'));
        $endDate = strtotime( Date('Y-m-t 23:59:59'));
        return view('admin.index', compact('startDate','endDate')
        );
    }

    public function updateData(Request $request){
        $startDate = (int)$request->get('startDate');
        $endDate = (int)$request->get('endDate');
        $start = Date('Y-m-d 00:00:00', $startDate);
        $end = Date('Y-m-d 23:59:59', $endDate);


        $order_request = OrderRequest::wherebetween('updated_at', [$start, $end]);
        $total_request = $order_request->count();
        $order_request= $order_request->pluck('id');
        $order = OrderRequest::where('status', 'success')->wherebetween('updated_at', [$start, $end]);
        $total_order = $order->count();
        $order = $order->pluck('id');
        $approver = OrderRequestApprover::whereIn('order_request_id',$order)
            ->where('step',OrderRequestApprover::REQUEST_STEP_2)
            ->where('status',OrderRequestApprover::STATUS_APPROVE)
            ->where('is_deleted',0)->pluck('supplier_id');
        $total_quantity = 0;
        $total_cost = 0;
        $supplier_details = ORSupplierDeatil::whereIN('order_request_supplier_id', $approver)->get();
        foreach ($supplier_details as $item){
            $quantity = !empty($item->erp) ? (int)$item->erp['quantity'] : 0 ;
            $total_quantity += $quantity;
            $total_cost = $quantity*$item->price;
        }
        $today_sub_1 = time()-86400;
        $today_sub_1 = Date('Y-m-d H:i:s', $today_sub_1);
        $approver = OrderRequestApprover::whereIn('order_request_id',$order_request)
            ->where('status', OrderRequestApprover::STATUS_CREATED)
            ->where('updated_at','<',$today_sub_1)
            ->pluck('order_request_id')->toArray();

        $approver_accountant = OrderRequestAccountant::whereIn('order_request_id',$order_request)
            ->where('status', OrderRequestAccountant::STATUS_CREATED)
            ->where('updated_at','<',$today_sub_1)
            ->pluck('order_request_id')->toArray();

        $ids = array_merge($approver , $approver_accountant);
        $page = (int)$request->input('page',1);
        $skip = ($page - 1)*10;


        $orders = OrderRequest::whereIn('id',$ids)->with(['user']);
        $total = ceil($orders->count() / 10);
        $orders = $orders->skip($skip)->take(10)->get();
        $order_request = OrderRequest::wherebetween('updated_at', [$start, $end])->get();
        $report = $this->getDateRange($startDate, $endDate, $order_request);
        return response()->json([
            'success' => true,
            'data' => compact(
                'startDate',
                'endDate',
                'total_cost',
                'total_quantity',
                'total_request',
                'total_order',
                'orders',
                'page',
                'total',
                'report'
            )
        ]);
    }

    private function getDateRange($startDate, $endDate, $order_request){
        $range = [];
        $data = [];
        $data_success = [];

        $i = $startDate;
        do{
            $a = $order_request->where('created_at', '>', Date('Y-m-d 00:00:00',$i))->where('created_at', '<', Date('Y-m-d 23:59:59',$i));
            $data[] = $a->count();
            $data_success[] = $a->where('status', OrderRequest::STATUS_SUCCESS)->count();
            $range[] = Date('Y-m-d',$i);
            $i += 86400;
        }while($i < $endDate);

        return [
            'range'=> $range,
            'data'=> $data,
            'data_success'=> $data_success,
        ];
    }
}
