<?php

namespace App\Http\Controllers\Admin;

use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function index(Request $request){
        $page = (int)$request->get('page',1);
        $skip = ($page-1)*config('const.ITEM_PER_PAGE');
        $keyword = trim($request->get('keyword',''));
        $query = DB::connection('sqlsrv')->table('PURMA');
        if($keyword){
            $query->where('MA002','like',"%$keyword%")
                ->orWhere('MA003','like',"%$keyword%")
                ->orWhere('MA001','like',"%$keyword%");
        }
        $total_records = $query->count();
        $total = ceil($total_records/config('const.ITEM_PER_PAGE'));
        $suppliers = $query->skip($skip)->take(config('const.ITEM_PER_PAGE'))->get();
        return response()->json([
            "success" => true,
            "suppliers" => $suppliers,
            "total_records" => $total_records,
            'page' => $page,
            'total' => $total
        ]);
    }

    public function history(Request $request){
        $page = (int)$request->get('page',1);
        $id = $request->input('id');
        $skip = ($page-1)*config('const.ITEM_PER_PAGE');
        $query = DB::connection('sqlsrv')->table('PURTG');
        $query->where('TG005',$id);
        $total_records = $query->count();
        $total = ceil($total_records/config('const.ITEM_PER_PAGE'));
        $suppliers = $query->skip($skip)->take(config('const.ITEM_PER_PAGE'))->get();
        foreach ($suppliers as &$supplier){
            $detail = DB::connection('sqlsrv')->table('PURTH')
                ->where('TH001', $supplier->TG001)
                ->where('TH002', $supplier->TG002)->first();
            if($detail){
                $supplier->code = $detail->TH011.'-'.$detail->TH012;
            }else{
                $supplier->code = '';
            }
        }
        return response()->json([
            "success" => true,
            "suppliers" => $suppliers,
            "total_records" => $total_records,
            'page' => $page,
            'total' => $total
        ]);
    }
}
