<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\OrderRequestHelper;
use App\Http\Response\APIResponse;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class ApprovalController extends Controller
{
    public function index(Request $request){
        $step = $request->get('step', 1);
        $query = OrderRequestApprover::latest();
        $query->where('user_id',Auth::id())->where('is_deleted',0);
        $query->where('step', $step);
        $query->with(['order_request' => function($q){
            $q->with(['order','user']);
        },'user']);

        $total_records = $query->count();
        $limit = config('const.ITEM_PER_PAGE');
        $total = ceil($total_records/ $limit);
        $list_approval = $query->with(['user','order_request'=> function($q){
            $q->with('user');
        }])->skip(0)->take($limit)->get();
        $list_approval = $list_approval->map(function ($item){
            if(empty($item->order_request)){
                return $item;
            }
            $arr_code = explode('-', $item->order_request->code);
            if(count($arr_code) < 2) {
                return $item;
            }
            $details = OrderRequestDetail::where('order_request_id', $item->order_request_id)->get();
            $erp_details = DB::connection('sqlsrv')->table('PURTB')
                ->where('TB001', $arr_code[0])
                ->where('TB002', $arr_code[1])->get([
                    'TB004',
                    'TB005',
                    'TB006',
                    'TB007',
                    'TB009',
                    'TB011',
                ]);
            $products = [];
            foreach ($details as $detail) {
                $product = [];
                $inventory = DB::connection('sqlsrv')->table('INVMC')
                    ->where('MC001', $detail->product_id)->sum('MC007');
                foreach ($erp_details as $row) {
                    if (trim($detail->product_id) == trim($row->TB004) &&
                        trim($detail->product_name) == trim($row->TB005)
                    ) {
                        $product['product_id'] = $detail->product_id;
                        $product['product_name'] = $detail->product_name;
                        $product['inventory'] = $inventory;
                        $product['quantity'] = $row->TB009;
                        $product['specification'] = $row->TB006;
                        $product['unit'] = $row->TB007;
                        $product['path'] = $detail->path;
                        $product['date'] = $detail->date;
                        $product['date'] = $row->TB011;
                    }
                }
                $products[] = $product;
            }
            $item->details = $products;
            return $item;
        });
        return view('admin.view_approval.index',compact('list_approval','total','total_records','step'));
    }
    public function getOrderRequestApproval(Request $request){
        $step = $request->input('step', 1);
        $status = $request->input('status', '');
        $page= (int)$request->input('page', 1);
        $query = OrderRequestApprover::latest();
        $query->where('step', $step);
        $query->where('user_id',Auth::id())->where('is_deleted',0);
        $query->with(['order_request' => function($q){
            $q->with(['order','user']);
        },'user']);
        if($status != ''){
            $query->where('status',$status);
        }
        $total_records = $query->count();
        $limit = config('const.ITEM_PER_PAGE');
        $total = ceil($total_records/ $limit);
        $skip = ($page - 1)*$limit;
        $list_approval = $query->with('user')->skip($skip)->take($limit)->get();
        $list_approval = $list_approval->map(function ($item){
            if(empty($item->order_request)){
                return $item;
            }
            $arr_code = explode('-', $item->order_request->code);
            if(count($arr_code) < 2) {
                return $item;
            }
            $details = OrderRequestDetail::where('order_request_id', $item->order_request_id)->get();
            $erp_details = DB::connection('sqlsrv')->table('PURTB')
                ->where('TB001', $arr_code[0])
                ->where('TB002', $arr_code[1])->get([
                    'TB004',
                    'TB005',
                    'TB006',
                    'TB007',
                    'TB009',
                    'TB011',
                ]);
            $products = [];
            foreach ($details as $detail) {
                $product = [];
                $inventory = DB::connection('sqlsrv')->table('INVMC')
                    ->where('MC001', $detail->product_id)->sum('MC007');
                foreach ($erp_details as $row) {
                    if (trim($detail->product_id) == trim($row->TB004) &&
                        trim($detail->product_name) == trim($row->TB005)
                    ) {
                        $product['product_id'] = $detail->product_id;
                        $product['product_name'] = $detail->product_name;
                        $product['inventory'] = $inventory;
                        $product['quantity'] = $row->TB009;
                        $product['specification'] = $row->TB006;
                        $product['unit'] = $row->TB007;
                        $product['path'] = $detail->path;
                        $product['date'] = $detail->date;
                        $product['date'] = $row->TB011;
                    }
                }
                $products[] = $product;
            }
            $item->details = $products;
            return $item;
        });
        return response()->json([
            'success' => true,
            'list_approval' => $list_approval,
            'total' => $total,
            'total_records' => $total_records,
            'page' => $page
        ]);
    }

    public function actionApprover(Request $request){
        $step = $request->input('step');

        $arr_status = [OrderRequestApprover::STATUS_REJECT, OrderRequestApprover::STATUS_RETURN];
        if($step == 1 || $step == 4){
            $arr_status[] = OrderRequestApprover::STATUS_APPROVE;
        }
        $validator = Validator::make($request->all(), [
            'step' => 'required|in:1,2,4',
            'status' => 'required|in:'.implode(',',$arr_status)
        ]);
        if($validator->fails()){
            return APIResponse::fail($validator->errors()->first());
        }
        $data = $request->all();
        $ids = $data['ids'];
        $response = [
            'success' => [

            ],
            'fail' => [

            ]
        ];
        if($step == 1){
            foreach ($ids as $id){
                $approver = OrderRequestApprover::find($id);
                if($approver){
                    if($approver->status == OrderRequestApprover::STATUS_CREATED){
                        $approver->update([
                            'status' => $data['status'],
                            'reason' => $data['reason']
                        ]);
                        OrderRequestHelper::checkOrderRequest($approver);
                        $response['success'][] = $approver->order_request ? $approver->order_request->code : '';
                    }else{
                        $response['fail'][] = $approver->order_request ? $approver->order_request->code : '';
                    }
                }
            }
        }else if($step == 2){
            foreach ($ids as $id){
                $approver = OrderRequestApprover::find($id);
                if($approver){
                    if($approver->status == OrderRequestApprover::STATUS_CREATED) {
                        $approver->update([
                            'status' => $data['status'],
                            'reason' => $data['reason']
                        ]);
                        OrderRequestHelper::checkSupplier($approver->order_request_id);
                        $response['success'][] = $approver->order_request ? $approver->order_request->code : '';
                    }else{
                        $response['fail'][] = $approver->order_request ? $approver->order_request->code : '';
                    }
                }
            }
        }else if($step == 4){
            foreach ($ids as $id){
                $approver = OrderRequestApprover::find($id);
                if($approver){
                    if($approver->status == OrderRequestApprover::STATUS_CREATED) {
                        $approver->update([
                            'status' => $data['status'],
                            'reason' => $data['reason']
                        ]);
                        OrderRequestHelper::checkAdvance($approver);
                        $response['success'][] = $approver->order_request ? $approver->order_request->code : '';
                    }else{
                        $response['fail'][] = $approver->order_request ? $approver->order_request->code : '';
                    }
                }
            }
        }
        return APIResponse::success($response);
    }
}
