<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadHelper;
use App\Http\Response\APIResponse;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class ImageController extends Controller
{
    public function index(Request $request){
        $keyword = trim($request->input('keyword',''));
        $limit = (int)$request->input('limit',config("constants.ITEM_PER_PAGE"));
        $query = Image::latest();
        if($keyword){
            $query->where("name","like","%$keyword%");
        }
        $images = $query->paginate($limit);
        return APIResponse::success($images);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'images' => 'required|array|max:10',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->fails()) {
            return APIResponse::fail([],$validator->errors()->first());
        }
        $data = $request->all();
        $images = $data['images'];
        foreach ($images as $image){
            $path = UploadHelper::uploadImage($image);
            $name = $image->getClientOriginalName();
            $name = explode('.', $name);
            $name = $name[0];
            Image::create([
                "name" => $name,
                "path" => $path,
                "create_user_id" => Auth::id()
            ]);
        }
        return APIResponse::success();
    }
}
