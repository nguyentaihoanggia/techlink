<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(Request $request){
        $page = (int)$request->get('page',1);
        $skip = ($page-1)*config('const.ITEM_PER_PAGE');
        $keyword = trim($request->get('keyword',''));
        $query = DB::connection('sqlsrv')->table('INVMB');
        if($keyword){
            $query->where('MB001','like',"%$keyword%")
                ->orWhere('MB002','like',"%$keyword%");
        }
        $total_records = $query->count();
        $total = ceil($total_records/config('const.ITEM_PER_PAGE'));
        $products = $query->skip($skip)->take(config('const.ITEM_PER_PAGE'))->get();
        $products->map(function ($item){
            $item->name = $item->MB002;
            $item->id =  $item->MB001;
            $item->specification =  $item->MB003;
            return $item;
        });
        return response()->json([
            "success" => true,
            "products" => $products,
            "total_records" => $total_records,
            'page' => $page,
            'total' => $total
        ]);
    }

    public function history(Request $request){
        $page = (int)$request->get('page',1);
        $id = $request->input('id');
        $skip = ($page-1)*config('const.ITEM_PER_PAGE');
        $query = DB::connection('sqlsrv')->table('PURTH as h');
        $query->where('h.TH004',$id);
        $query->leftJoin('PURTG as g', function ($join){
            $join->on('h.TH001','g.TG001')
                ->on('h.TH002','g.TG002');
        });
        $query->select('h.TH004','h.TH007','h.TH005','h.TH008','g.TG003', 'h.TH011','h.TH012');
        $total_records = $query->count();
        $total = ceil($total_records/config('const.ITEM_PER_PAGE'));
        $products = $query->skip($skip)->take(config('const.ITEM_PER_PAGE'))->get();

        return response()->json([
            "success" => true,
            "products" => $products,
            "total_records" => $total_records,
            'page' => $page,
            'total' => $total
        ]);
    }
}
