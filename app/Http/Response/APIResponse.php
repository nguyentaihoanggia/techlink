<?php


namespace App\Http\Response;


class APIResponse
{
    public static function success($data = [], $message = ""){
        return response()->json([
            "success" => true,
            "data" => $data,
            "message" => $message
        ]);
    }

    public static function fail($message = "", $data = []){
        return response()->json([
            "success" => false,
            "data" => $data,
            "message" => $message
        ]);
    }
}
