<?php

namespace App\Mail;

use App\Models\OrderRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Accountant extends Mailable
{
    use Queueable, SerializesModels;
    public $id = '';
    public $user;
    public $order_request;
    public $tries = 5;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $user)
    {
        $this->id = $id;
        $this->user = $user;
        $this->order_request = OrderRequest::find($id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.accountant');
    }
}
