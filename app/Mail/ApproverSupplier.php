<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproverSupplier extends Mailable
{
    use Queueable, SerializesModels;
    public $id = '';
    public $user;
    public $tries = 5;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id = '',$user)
    {
        $this->id = $id;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.approver_supplier');
    }
}
