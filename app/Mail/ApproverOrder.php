<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproverOrder extends Mailable
{
    use Queueable, SerializesModels;
    public $id = '';
    public $order_id = '';
    public $user;
    public $tries = 5;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$order_id,$user)
    {
        $this->id = $id;
        $this->order_id = $order_id;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order');
    }
}
