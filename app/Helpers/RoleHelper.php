<?php


namespace App\Helpers;


use App\Models\OrderRequestNotify;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RoleHelper
{
    public static function createOrderRequest($user){
        if(isset($user->role_id)){
            if($user->role_id == User::ROLE_ADMIN || $user->role_id == User::ROLE_USER){
                return true;
            }
        }
        return false;
    }
    public static function createSupplier($user, $od_request){
        if(isset($user->role_id)){
            if($user->role_id == User::ROLE_ADMIN){
                return true;
            }else{
                $notify = OrderRequestNotify::where('order_request_id', $od_request->id)->get('user_id')->toArray();
                if (in_array($user->id, array_column($notify,'user_id'))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function showOrderRequest($user, $od_request){
        $users = [];
        $users[] = $od_request->create_user_id;
        if(!empty($od_request->approver1)){
            foreach ($od_request->approver1 as $item){
                $users[] = $item->user_id;
            }
        }

        if(!empty($od_request->approver2)){
            $users[] = $od_request->approver2->user_id;
        }

        if(!empty($od_request->approver3)){
            foreach ($od_request->approver3 as $item){
                $users[] = $item->user_id;
            }
        }

        if(!empty($od_request->approver_advance)){
            $users[] = $od_request->approver_advance->user_id;
        }

        if(!empty($od_request->notify)){
            foreach ($od_request->notify as $item){
                $users[] = $item->user_id;
            }
        }
        $accountant = User::where('role_id', User::ROLE_ACCOUNTANT)->get();
        foreach ($accountant as $item){
            $users[] = $item->id;
        }
        if($user->role_id == User::ROLE_ACCOUNTANT || $user->role_id == User::ROLE_ADMIN){
            return true;
        }
        return in_array($user->id,$users);
    }
}
