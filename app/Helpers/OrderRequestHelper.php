<?php


namespace App\Helpers;


use App\Models\OrderRequest;
use App\Models\OrderRequestAccountant;
use App\Models\OrderRequestApprover;
use App\Models\OrderRequestNotify;
use App\Models\OrderRequestSupplier;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class OrderRequestHelper
{
    public static function checkOrderRequest($approver){
        $order_requets = OrderRequest::find($approver->order_request_id);
        if($order_requets) {
            if ($approver->status == OrderRequestApprover::STATUS_APPROVE) {
                $order_requets->status = OrderRequest::STATUS_APPROVED_1;
                $order_requets->user_approved = $approver->user_id;
                $order_requets->save();

                $approvers = OrderRequestApprover::where('step',OrderRequestApprover::REQUEST_STEP_1)
                    ->where('order_request_id',$order_requets->id)
                    ->where('is_deleted',0)->update([
                        'status' => OrderRequestApprover::STATUS_APPROVE
                    ]);
                $notify = OrderRequestNotify::where('order_request_id', $order_requets->id)->get();
                foreach ($notify as $item){
                    $item->status = OrderRequestNotify::STATUS_SUCCESS;
                    $item->save();
                    SendMailHelper::sendMailNotify($item->user_id, $order_requets->id);
                }

            }
            if ($approver->status == OrderRequestApprover::STATUS_REJECT) {
                $order_requets->status = OrderRequest::STATUS_REJECTED_1;
                $order_requets->save();
                $check = 'reject';
            }
            if ($approver->status == OrderRequestApprover::STATUS_RETURN) {
                $order_requets->status = OrderRequest::STATUS_RETURNED_1;
                $order_requets->save();
                $check = 'return';
            }
        }
    }

    public static function checkSupplier($order_request_id, $total_cost= 0){
        $order_requets = OrderRequest::find($order_request_id);
        if($order_requets){
            if($order_requets->status == OrderRequest::STATUS_SUPPLIER){
                $approver = OrderRequestApprover::where('step',OrderRequestApprover::REQUEST_STEP_2)
                    ->where('order_request_id',$order_requets->id)->where('is_deleted',0)->first();
                if($approver->status == OrderRequestApprover::STATUS_APPROVE){
                    $order_requets->status = OrderRequest::STATUS_APPROVED_2;
                    $order_requets->save();
                    $accountants = User::where('role_id', User::ROLE_ACCOUNTANT)->get();
                    foreach ($accountants as $accountant){
                        SendMailHelper::sendMailAccountant($accountant->id, $order_requets->id);
                        OrderRequestAccountant::create([
                            'order_request_id' => $order_requets->id,
                            'status' => OrderRequestAccountant::STATUS_CREATED,
                            'user_id' => $accountant->id
                        ]);
                    }
                }
                if($approver->status == OrderRequestApprover::STATUS_REJECT){
                    $order_requets->status = OrderRequest::STATUS_REJECTED_2;
                    $order_requets->save();
                }
                if($approver->status == OrderRequestApprover::STATUS_RETURN){
                    $order_requets->status = OrderRequest::STATUS_RETURNED_2;
                    $order_requets->save();
                }else{
                    $notify = OrderRequestNotify::where('order_request_id',$order_requets->id)->update([
                        'status' => OrderRequestNotify::STATUS_DONE
                    ]);
                }

            }
        }
    }

    public static function checkOrder($order_request_id){
        $order_requets = OrderRequest::find($order_request_id);
        if($order_requets){
            if($order_requets->status == OrderRequest::STATUS_ORDER){
                $approvers = OrderRequestApprover::where('step',OrderRequestApprover::REQUEST_STEP_3)
                    ->where('order_request_id',$order_requets->id)->where('is_deleted',0)->get();
                $check = 'approve';
                foreach ($approvers as $approver){
                    if($approver->status == OrderRequestApprover::STATUS_REJECT){
                        $order_requets->status = OrderRequest::STATUS_REJECT;
                        $order_requets->save();
                        $check = 'reject';
                        break;
                    }
                    if($approver->status == OrderRequestApprover::STATUS_CREATED){
                        $check = 'pending';
                    }
                }
                if($check == 'approve'){
                    $order_requets->status = OrderRequest::STATUS_SUCCESS;
                    $order_requets->save();
                }
            }
        }
    }

    public static function checkAccountant($approver, $total_cost= 0){
        $order_requets = OrderRequest::find($approver->order_request_id);
        if($order_requets){
            if ($approver->status == OrderRequestAccountant::STATUS_APPROVED) {
                if($total_cost >= 35000000){
                    $order_requets->status = OrderRequest::STATUS_ADVANCE;
                    $order_requets->save();
                }else {
                    $order_requets->status = OrderRequest::STATUS_SUCCESS;
                    $order_requets->save();
                    $approvers = OrderRequestAccountant::where('order_request_id', $order_requets->id)
                        ->where('id', '!=', $approver->id)
                        ->where('is_deleted', 0)->update([
                            'status' => OrderRequestAccountant::STATUS_APPROVED_0
                        ]);
                }
            }
            if ($approver->status == OrderRequestAccountant::STATUS_REJECTED) {
                $order_requets->status = OrderRequest::STATUS_FAIL;
                $order_requets->save();
            }
        }
    }
    public static function checkAdvance($approver){
        $order_requets = OrderRequest::find($approver->order_request_id);
        if($order_requets) {
            if ($approver->status == OrderRequestApprover::STATUS_APPROVE) {
                $order_requets->status = OrderRequest::STATUS_SUCCESS;
                $order_requets->save();
                $approvers = OrderRequestAccountant::where('order_request_id', $order_requets->id)
                    ->where('status', '!=', OrderRequestAccountant::STATUS_APPROVED)
                    ->where('is_deleted', 0)->update([
                        'status' => OrderRequestAccountant::STATUS_APPROVED_0
                    ]);
            }
            if ($approver->status == OrderRequestApprover::STATUS_REJECT) {
                $order_requets->status = OrderRequest::STATUS_REJECTED_3;
                $order_requets->save();
                $check = 'reject';
            }
        }
    }
}
