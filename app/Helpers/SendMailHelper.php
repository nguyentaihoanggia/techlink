<?php


namespace App\Helpers;


use App\Mail\Accountant;
use App\Mail\Advance;
use App\Mail\Approver;
use App\Mail\ApproverOrder;
use App\Mail\ApproverSupplier;
use App\Mail\Notify;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendMailHelper
{
    public static function sendMailApproval(array $user_ids, $id){
        $users = User::whereIn('id',$user_ids)->get();
        foreach ($users as $user){
            Log::info('send mail:' . $user->email);
            Mail::to($user->email)->queue(new Approver($id,$user));
        }

    }

    public static function sendMailApprovalSupplier($user_id, $id){
        $users = User::where('id',$user_id)->get();
        foreach ($users as $user){
            Mail::to($user->email)->queue(new ApproverSupplier($id,$user));
        }
    }
    public static function sendMailApprovalorder($user_ids, $id, $order_id){
        $users = User::whereIn('id',$user_ids)->get();
        foreach ($users as $user){
            Mail::to($user->email)->queue(new ApproverOrder($id, $order_id, $user));
        }
    }

    public static function sendMailNotify($user_id, $id){
        $users = User::where('id',$user_id)->get();
        foreach ($users as $user){
            Mail::to($user->email)->queue(new Notify($id, $user));
        }
    }
    public static function sendMailAccountant($user_id, $id){
        $users = User::where('id',$user_id)->get();
        foreach ($users as $user){
            Mail::to($user->email)->queue(new Accountant($id, $user));
        }
    }

    public static function sendMailAdvance(array $user_ids, $id){
        $users = User::whereIn('id',$user_ids)->get();
        foreach ($users as $user){
            Log::info('send mail:' . $user->email);
            Mail::to($user->email)->queue(new Advance($id,$user));
        }

    }

}
