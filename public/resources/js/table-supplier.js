Vue.component('table-supplier', {
    props: {
        value: {
            required: true,
        },
    },
    template: '<div class="modal-dialog modal-lg-80" >\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">×</span>\
                        </button>\
                        <h4 class="modal-title">List supllier</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="row">\
                            <div class="col-xs-12 col-md-7">\
                                <p>Tìm thấy <strong>{{pagination.total_records | money}}</strong> nhà cung cấp</p>\
                            </div>\
                            <div class="col-xs-12 col-md-5">\
                                <div class="input-group input-group-sm hidden-xs" >\
                                    <input type="text" class="form-control" v-model="keyword">\
                                    <div class="input-group-btn">\
                                        <button type="button" class="btn btn-default" @click="getDataSuppliers()"><i class="fa fa-search"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="table-responsive" style="margin-top: 10px">\
                            <table class="table table-hover table-bordered">\
                                <thead>\
                                    <tr>\
                                        <th width="50" class="text-center" >\
                                            <input type="checkbox" v-model="selectAll">\
                                        </th>\
                                        <th>ID<th>\
                                        <th>Mã nhà cung cấp</th>\
                                        <th>Tên nhà cung cấp</th>\
                                        <th>Đất nước</th>\
                                        <th>Khu vực</th>\
                                        <th>SĐT 1</th>\
                                        <th>SĐT 2</th>\
                                        <th>ĐC 1</th>\
                                        <th>ĐC 2</th>\
                                        <th>Fax</th>\
                                        <th>Email</th>\
                                        <th>Tên người liên hệ</th>\
                                    </tr>\
                                </thead>\
                                <tbody>\
                                    <template v-if="!is_loading">\
                                        <tr v-for="item,index in suppliers" @click="selectItem(item)" :class="active(item)">\
                                            <td class="text-center">\
                                                <input type="checkbox" v-model="checkBox" :value="getValue(item)">\
                                            </td>\
                                            <td>{{ item._id }}</td>\
                                            <td>{{ item.supplier_code }}</td>\
                                            <td>{{ item.supplier_name }}</td>\
                                            <td>{{ item.country }}</td>\
                                            <td>{{ item.area }}</td>\
                                            <td>{{ item.phone1 }}</td>\
                                            <td>{{ item.phone2 }}</td>\
                                            <td>{{ item.address1 }}</td>\
                                            <td>{{ item.address2 }}</td>\
                                            <td>{{ item.fax }}</td>\
                                            <td>{{ item.email }}</td>\
                                            <td>{{ item.contact_person }}</td>\
                                        </tr>\
                                    </template>\
                                    <template v-else>\
                                        <tr>\
                                            <td colspan="14" class="text-center">\
                                                <div class="loader"></div>\
                                            </td>\
                                        </tr>\
                                    </template>\
                                </tbody>\
                            </table>\
                        </div>\
                        <div class="row">\
                            <div class="col-xs-12">\
                                <div class="pull-right">\
                                        <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="modal-footer">\
                        <button type="button" class="btn btn-primary" @click="submit">Add</button>\
                    </div>\
                </div>\
            </div>',
    created: function () {
        
    },
    mounted: function () {
        var vm = this;
        this.getDataSuppliers();
        
        
        vm.checkBox = [];
        this.value.forEach(function(item){
            var supplier = {
                _id: item._id,
                supplier_code: item.supplier_code,
                supplier_name: item.supplier_name,
                country: item.country ? item.country : "",
                area: item.area ? item.area : "",
                phone1: item.phone1 ? item.phone1 : "",
                phone2: item.phone2 ? item.phone2 : "",
                address1: item.address1 ? item.address1 : "",
                address2: item.address2 ? item.address2 : "",
                fax: item.fax ? item.fax : "",
                email: item.email ? item.email : "",
                contact_person: item.contact_person ? item.contact_person : ""
            }
            vm.checkBox.push(supplier);
        })
    },

    data: function () {
        return {
            suppliers: [],
            checkBox:[],
            pagination:{
                page:1,
                total:0,
                current:1
            },
            keyword:'',
            is_loading: false
        }
    },
    methods: {
        getDataSuppliers: function(){
            var vm = this;
            vm.is_loading = true;
           $.get('/admin/suppliers',{page:this.pagination.page, keyword: this.keyword},function(res){
            vm.is_loading = false;
                vm.suppliers = res.suppliers.map(function(item){
                    return {
                        _id: item.MA001.trim(),
                        supplier_code: item.MA002.trim(),
                        supplier_name: item.MA003.trim(),
                        country: item.MA006.trim(),
                        area: item.MA007.trim(),
                        phone1: item.MA008.trim(),
                        phone2: item.MA009.trim(),
                        address1: item.MA014.trim(),
                        address2: item.MA015.trim(),
                        fax: item.MA010.trim(),
                        email: item.MA011.trim(),
                        contact_person: item.MA013.trim(),
                    }
                })
                vm.pagination.current = res.page
                vm.pagination.total_records = res.total_records
                vm.pagination.total = res.total
           })
        },
        submit: function(){
            var suppliers = [];
            this.checkBox.forEach(function(item){
                suppliers.push(item)    
            })
            this.$emit('input',suppliers);
            $("#modal-supplier").modal('hide');
        },
        getValue: function(value){
            return JSON.parse(JSON.stringify(value))
        },
        selectItem: function(item){
            vm = this;
            var index = _.findIndex( vm.checkBox , {_id:item._id});
            if( index == -1){
                vm.checkBox.push(vm.getValue(item));
            }else{
                 vm.checkBox.splice(index, 1);
            }
        },
        active: function(item){
            if(_.findIndex( this.checkBox , {_id:item._id}) == -1){
                return '';
            }else{
                return 'success';
            }
        }
    },
    watch: {
        'pagination.page': function(){
            this.getDataSuppliers();
        },
        'value' : function(newval){
            var vm = this;
            vm.checkBox = [];
            console.log(newval)
            newval.forEach(function(item){
                var supplier = {
                    _id: item._id,
                    supplier_code: item.supplier_code,
                    supplier_name: item.supplier_name,
                    country: item.country ? item.country : "",
                    area: item.area ? item.area : "",
                    phone1: item.phone1 ? item.phone1 : "",
                    phone2: item.phone2 ? item.phone2 : "",
                    address1: item.address1 ? item.address1 : "",
                    address2: item.address2 ? item.address2 : "",
                    fax: item.fax ? item.fax : "",
                    email: item.email ? item.email : "",
                    contact_person: item.contact_person ? item.contact_person : "",
                }
                vm.checkBox.push(supplier);
            })
        }
    },
    computed: {
        selectAll: {
            get: function () {
                var vm = this;
                if( this.suppliers.length ){
                    var check = true;
                    this.suppliers.forEach(function(item){
                        if( _.findIndex( vm.checkBox , {_id:item._id}) == -1){
                            check = false;
                        }
                    })
                    return check;
                }
                return false;
            },
            set: function (value) {
                var vm = this;
                if (value) {
                    this.suppliers.forEach(function (item) {
                        if( _.findIndex( vm.checkBox , {_id:item._id}) == -1){
                            vm.checkBox.push(item);
                        }
                    });
                }else{
                    this.suppliers.forEach(function (item) {
                        var index = _.findIndex( vm.checkBox , {_id:item._id});
                        if( index >= 0){
                            vm.checkBox.splice(index, 1);
                        }
                    });
                }
            }
        }
    },
})