Vue.config.devtools = true
if (typeof window.VueMultiselect != 'undefined') {
    // Vue.use(window.VueMultiselect.default)
    Vue.component('multiselect', window.VueMultiselect.default)
}
if (typeof jconfirm != 'undefined') {
    jconfirm.defaults = {
        animateFromElement: false,
        smoothContent: true,
        title: '',
        content: '',
        theme: 'material',
        backgroundDismiss: true,
        defaultButtons: {
            close: {
                text: 'Đóng',
                btnClass: 'btn-inverse',
                action: function () {}
            },
        },
    };
}
Vue.prototype.$watchAll = function (props, callback) {
    var vm = this;
    props.forEach(function (prop) {
        vm.$watch(prop, callback.bind(null, prop));
    });
};


if (typeof window.VueTimepicker != 'undefined') {
    Vue.use(window.VueTimepicker);
}
Vue.component('select2', {
    props: {
        value: {
            required: true,
        },
        options: {
            type: Array,
        },
        placeholder: {
            type: String,
        },
        notfound: {
            type: String,
            default: 'Không tìm thấy !',
        },
        search: {
            type: Boolean,
            default: false,
        },
        multiple: {
            type: Boolean,
            default: false,
        },
        change: {
            type: Function,
        },
        allowclear: {
            type: Boolean,
            default: false,
        },
        max: {
            type: Number,
            default: 10,
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        readonly: {
            type: Boolean,
            default: false,
        },
        position: {
            type: String,
            default: 'left'
        },
        icon: {
            type: String,
        },
        width: {
            type: String,
            default: 'resolve'
        },
        labels: {
            type: Array,
        }
    },
    template: '<select class="form-control" ></select>',
    created: function () {
        this.convert();

    },
    mounted: function () {
        var vm = this;
        var config = {
            disabled: this.disabled,
            multiple: this.multiple,
            minimumResultsForSearch: this.search ? 0 : -1,
            allowClear: this.allowclear,
            data: this.data,
            language: {
                noResults: function () {
                    return vm.notfound;
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        };
        if (this.placeholder != undefined) {
            config['placeholder'] = this.placeholder;
        }
        this.config = config;
        this.init();
    },

    data: function () {
        return {
            data: [],
            config: {},
            selected: this.value,
            select2: null,
        }
    },
    methods: {
        convert: function () {
            var vm = this;
            var data = [];
            if (_.isArray(vm.options)) {

                if (vm.labels != undefined && vm.labels.length == 2) {
                    data = vm.options.map(function (item) {
                        var el = {};
                        el['id'] = item[vm.labels[0]];
                        var text = '';
                        if (item.hasOwnProperty(vm.labels[1])) {
                            text = item[vm.labels[1]];
                        }
                        el['text'] = String(text).capitalize();
                        return el;
                    })
                } else {
                    data = vm.options.map(function (item) {
                        var el = {};
                        el['id'] = item.hasOwnProperty('id') ? item['id'] : (item.hasOwnProperty('id') ? item['id'] : '');
                        var text = '';
                        if (item.hasOwnProperty('text')) {
                            text = item['text'];
                        } else if (item.hasOwnProperty('name')) {
                            text = item['name'];
                        } else if (item.hasOwnProperty('code')) {
                            text = item['code'];
                        }
                        el['text'] = String(text).capitalize()
                        return el;
                    })
                }
            }
            vm.data = data;
        },
        init: function () {
            var vm = this;
            vm.config['data'] = vm.data;
            if (vm.placeholder != undefined && !vm.multiple) {
                $(vm.$el).append("<option></option>")
            }

            if (vm.multiple) {
                vm.select2 = $(vm.$el).select2(vm.config).on('change', function (e) {
                    vm.$emit('input', $(this).val());
                    if (vm.change != undefined && typeof vm.change == 'function') {
                        vm.change();
                    }
                });
            } else {
                vm.select2 = $(vm.$el).select2(vm.config).on({
                    'select2:select': function (e) {
                        vm.$emit('input', e.params.data.id);
                        if (vm.change != undefined && typeof vm.change == 'function') {
                            vm.change();
                        }
                    },'select2:unselecting' : function(e){
                        var data = e.params.data;  
                        $(this).data('state', 'unselected');
                        if( data == undefined){
                            vm.$emit('input', '');
                        }
                    },'select2:open' : function(e){
                        if ($(this).data('state') === 'unselected') {
                            $(this).removeData('state'); 
                            var self = $(this);
                            setTimeout(function() {
                                self.select2('close');
                            }, 1);
                        }  
                    }
                });

            }
            if (vm.value != undefined && vm.value != '') {
                vm.select2.val(vm.value).trigger("change.select2");
            } else {
                if (_.find(vm.data, {
                        id: vm.value
                    }) == undefined && !vm.multiple) {
                    vm.$emit('input', '');
                }
            }
        },
        destroy: function () {
            if ($(this.$el).data('select2')) {
                this.select2.select2('destroy');
                $(this.$el).empty();
                this.init();
            }
        }
    },
    watch: {
        options: {
            handler : function(){
                this.convert();
                this.destroy();
            },
            deep : true
        },
        'value': function (newval) {
            this.select2.val(newval).trigger('change.select2');
        },
        'disabled': function (newval) {
            $(this.$el).attr('disabled', newval);
        }
    },
    computed: {

    },
})

Vue.filter('capitalize', function (value) {
    if (!value) return '';
    var to = _.capitalize(value);
    return to;
})
Vue.filter('text-lowercase', function (value) {
    return (value != '') ? String(value).toLowerCase() : '';
});
Vue.filter('iso-full-time', function (value) {
    return moment(value).format('HH:mm DD/MM');
});
Vue.filter('iso-time', function (value) {
    return moment(value).format('HH:mm');
});

Vue.filter('full-time', function (value) {

    return moment(new Date(value)).format('HH:mm:ss DD/MM/YYYY');
});
Vue.filter('full-time-facebook', function (value) {

    return moment(new Date(value)).format('DD/MM/YYYY HH:mm');
});

Vue.filter('only-day', function (value) {
    return moment(new Date(value)).format('DD/MM/YYYY');
})
Vue.filter('day', function (value) {
    return moment(new Date(value)).format('DD');
})
Vue.filter('month', function (value) {
    return moment(new Date(value)).format('MM');
})
Vue.filter('year', function (value) {
    return moment(new Date(value)).format('YYYY');
})
Vue.filter('dd-mm', function (value) {
    return moment(new Date(value)).format('DD/MM');
})
Vue.filter('prev-month', function (value) {
    return moment(new Date(value)).subtract(1, 'months').endOf('month').format('MM');
})
Vue.filter('money', function (value) {
    var value = parseInt(0+value);
    return (value == undefined) ? '' : value.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
})
Vue.filter('phone', function (phone) {
    return phone.replace(/[^0-9]/g, '')
        .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
});
Vue.filter('short-money', function (value) {
    if (value > 1000000000) {
        value = Math.floor((value / 1000000000)).toFixed(0);
        return '~ ' + value + 'B';
    }
    if (value > 1000000) {
        value = Math.floor((value / 1000000)).toFixed(0);
        return '~ ' + value + 'M';
    }
    if (value > 1000) {
        value = Math.floor((value / 1000)).toFixed(0);
        return value + 'K';
    }
    return value.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
})

Vue.filter('dd-mm-yyyy', function (value) {
    return helper.formatDate(new Date(value), 'dd/mm/yyyy');
})
Vue.filter('mm-dd-yyyy', function (value) {
    return helper.formatDate(new Date(value), 'mm/dd/yyyy');
})
Vue.directive('debounce', {
    bind: function (el, binding) {

        if (binding.value !== binding.oldValue) {
            el.oninput = _.debounce(function (evt) {
                el.dispatchEvent(new Event('change'))
            }, parseInt(binding.value) || 500)
        }
    }
})
Vue.directive('tooltip', {
    bind: function (el, binding) {
        if( helper.isTouchDevice() == false ){
            if( el != null && el != undefined){
                $(el).tooltip({
                    trigger: "hover"
                });
            }
        }
    },
    unbind(el) {
        $(el).tooltip('destroy');
    }
})
Vue.directive('dropdown', {
    twoWay: true,
    bind: function (el, binding, vnode) {
        var vm = this;
        $(el).find('.v-dropdown-toggle').on('click', function () {
            // $(el).toggleClass('open');
            binding.value = true;
            console.log(binding);
        });
        // this.handler = function () {
        //     el.value = true;
        //     this.set(el.value)
        // }.bind(this)

    },
    update: function (el, binding) {
        console.log(binding);
    },
    unbind: function () {

    }
})
Vue.directive('select', {
    twoWay: true,
    bind: function (el, binding, vnode) {
        var vm = this;
        $(el).on("select2:select", function (e) {
            var event = new Event('change', {
                bubbles: true
            })
            var value = $(el).select2("val");
            el.value = value;
            if ($(el).attr('create') != undefined && $(el).val() == '-1') {
                el.value = null;
                var addnew = $($(el).attr('create'));
                var modal = $(addnew).data('modal');
                modal = (modal == undefined) ? '#myModal' : modal;
                var a = document.createElement('a');
                var att = document.createAttribute("data-url");
                att.value = $(addnew).data('url');
                a.setAttributeNode(att);
                load_data_to_modal(a, modal);
            }
            el.dispatchEvent(event);
        });
    },
    update: function (el, vnode) {
        $(el).val(vnode.value).trigger('change');
    },
    inserted: function (el, vnode) {
        var holder = $(el).attr('placeholder');
        var isMutiple = $(el).attr('multiple');
        var notfound = $(el).attr('notfound');
        var create = $(el).attr('create');
        var noSearch = $(el).attr('no-search');
        var options = {}
        if (noSearch != undefined) {
            options['dropdownCssClass'] = 'no-search';
        }
        if (holder != undefined) {
            options['placeholder'] = holder;
        }
        if (isMutiple != undefined) {
            options['tags'] = true;
        }
        if (notfound != undefined) {
            var string = notfound;
            options['language'] = {
                noResults: function () {
                    return string;
                }
            }
        } else {
            options['language'] = {
                noResults: function () {
                    return "Không tìm thấy !";
                }
            }
        }
        options['escapeMarkup'] = function (markup) {
            return markup;
        }
        if (vnode.value === undefined) {
            $(el).select2(options).val(null).trigger('change');
        } else {
            $(el).select2(options).val(vnode.value).trigger('change');
        }
    }
});



function updateFunction(el, binding) {
    var options = {
        allowClear: true,
        multiple: true,
        tags: true,
    }
    var holder = $(el).attr('placeholder');
    if (holder != undefined) {
        options['placeholder'] = holder;
    }
    var vm = this;
    $(el).select2(options).on("select2:select", function (e) {
        vm.$emit('input', $(this).val())
    });
}

Vue.directive('datepicker', {
    bind: function (el, binding, vnode) {
        var options = {
            dateFormat: "dd/mm/yy HH:mm:ss",
            changeYear: true,
            changeMonth: true,
            yearRange: '1950:2050',
            onSelect: function (date, i) {
                if (date !== i.lastVal) {
                    var date = helper.toDateTime(date) / 1000;
                    var event = new Event('input', {
                        bubbles: true
                    })
                    el.value = date;
                    el.dispatchEvent(event);
                }
            }
        }

        var minDate = $(el).attr('min');
        if (minDate != undefined & maxDate != '') {
            if (moment(new Date(minDate)).isValid()) {
                var date = moment(minDate).add(1, 'days');
                options.minDate = new Date(date);
            }
        }
        var maxDate = $(el).attr('max');
        if (maxDate != undefined & maxDate != '') {
            if (moment(new Date(maxDate)).isValid()) {
                var date = moment(maxDate).add(-1, 'days');
                options.maxDate = new Date(date);
            }
        }
        $(el).datepicker(options);
    },
    update: function (el, binding) {
        if (el.value != '') {
            var date = helper.formatDate(new Date(el.value), 'dd/mm/yyyy');
            if (moment(new Date(el.value)).isValid()) {
                $(el).datepicker('setDate', new Date(el.value));
            }
        }
        var minDate = $(el).attr('min');
        if (minDate != undefined & minDate != null) {
            if (moment(new Date(minDate)).isValid()) {
                var date = moment(minDate).add(1, 'days');
                $(el).datepicker("option", "minDate", new Date(date));
            }
        }
        var maxDate = $(el).attr('max');
        if (maxDate != undefined & maxDate != '') {
            if (moment(new Date(maxDate)).isValid()) {
                var date = moment(maxDate).add(-1, 'days');
                $(el).datepicker("option", "maxDate", new Date(date));
            }
        }
    },
    destroyed: function () {
        $(this.$el).datepicker("destroy");
    }
});


Vue.component('input-spinner', {
    template : '<div class="input-group">\
                <span class="input-group-btn" @click.stop.prevent="_descrease">\
                    <button class="btn btn-default" type="button"><span class="ion-ios-minus-empty"></span></button>\
                </span>\
                <money v-if="max != undefined" v-model="num"  class="form-control text-center" :min="1" :max="max"></money>\
                <money v-else v-model="num"  class="form-control text-center" :min="1"></money>\
                <span class="input-group-btn"  @click.stop.prevent="_increase">\
                    <button class="btn btn-default" type="button"><span class="ion-ios-plus-empty"></span></button>\
                </span>\
            </div>',
    props: {
        value : {},
        max : {}
    },
    data: function(){
        return {
            num : this.value
        }
    },
    methods:{
        _increase : function(){
            this.num += 1;
        },
        _descrease : function(){
            if( this.value > 1){
                this.num -= 1;
            }
        }
    },
    watch : {
        'value' : function(value) {
            this.num = value;
        },
        'num' : function(value){
            if( value != this.value){
                this.$emit('input', value);
            }
        }
    }
    
})
Vue.component('datepicker', {
    template: '<input type="text" :id="id" class="datepicker" placeholder="mm/dd/yyyy" readonly/>',
    props: {
        value: {

        },
        id: {
            required: true
        }
    },
    mounted: function () {
        var options = {
            changeYear: true,
            changeMonth: true,
            yearRange: '1970:2050',
            autoclose: true
        }
        $(this.$el).datepicker(options);

        if (this.value == null) {
            $(this.$el).datepicker('setDate', this.value);
        } else {
            if (this.value != undefined && this.value != '' && _.isDate(new Date(this.value))) {
                $(this.$el).datepicker('setDate', new Date(this.value));
            } else {
                this.$emit('input', null);
            }
        }
        var vm = this;
        $('.datepicker').datepicker()
        .on('hide', function(e) {
            var d = Date.parse(e.date)
            vm.onClose(moment(d).format('YYYY-MM-DD'))
        });
    },
    methods: {
        onClose: function (date) {
            if( date != undefined && date != '' && date != null){
                this.$emit('input', date);
            }
        },
    },
    watch: {
        'value': function (newval) {
            if (newval == null) {
                $(this.$el).datepicker('setDate', newval);
            } else {
                if (newval != undefined && newval != '' ) {
                    $(this.$el).datepicker('setDate', new Date(newval));
                } else {
                    this.$emit('input', null);
                }
            }
        }
    }
});

Vue.component('slide', {
    props: {
        active: Boolean,
        duration: {
            type: Number,
            default: 500
        },
        tag: String,
    },
    render: function (h) {
        return h(
            this.tag == undefined ? 'div' : this.tag, {
                style: {
                    display: 'none',
                },
                ref: 'container',
            },
            this.$slots.default
        )
    },
    mounted: function () {
        this.render();
    },
    watch: {
        active: function () {
            this.render();
        }
    },
    methods: {
        render: function () {
            if (this.active) {
                $(this.$refs.container).slideDown(this.duration);
            } else {
                $(this.$refs.container).slideUp(this.duration);
            }
        }
    }
});


Vue.component('timerange', {
    props: ['classname', 'compare', 'start', 'end', 'open'],
    template: '<input type="text" :class="classname"  class="daterange_input" readonly />',
    data: function () {
        return {
            startDate: this.start,
            endDate: this.end,
        }
    },
    mounted: function () {
        var vm = this;
        if (this.startDate != undefined) {
            this.startDate = new Date(this.startDate * 1000);
        } else {
            this.startDate = typeof vm.compare == 'undefined' ? moment().startOf('month') : moment().subtract(1, 'month').startOf('month');
        }
        if (this.endDate != undefined) {
            this.endDate = new Date(this.endDate * 1000);
        } else {
            this.endDate = typeof vm.compare == 'undefined' ? moment().endOf('month') : moment().subtract(1, 'month').endOf('month');
        }
        $(this.$el).daterangepicker({
            startDate: this.startDate,
            endDate: this.endDate,
            alwaysShowCalendars: true,
            timePicker: true,
            timePicker24Hour: true,
            opens: (this.open == undefined) ? 'left' : this.open,
            locale: {
                format: 'DD/MM/YYYY HH:mm'
            },
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        });
        $(this.$el).on('apply.daterangepicker', function (ev, picker) {
            var endDate = moment(picker.endDate.format('YYYY-MM-DD HH:mm'), 'YYYY-MM-DD HH:mm').toDate().getTime() / 1000;
            var startDate = moment(picker.startDate.format('YYYY-MM-DD HH:mm'), 'YYYY-MM-DD HH:mm').toDate().getTime() / 1000;
            vm.$emit('input', {
                'endDate': endDate,
                'startDate': startDate
            });
        });
    },
    watch: {
        'value': function (val) {

        }
    }
});
Vue.component('daterange-single', {
    props: {
        value: {

        },
        compare: {

        },
        open: {
            type: String,
            default: 'right',
        },
        start: {

        },
        end: {

        },
        drop: {
            type: String,
            default: 'down',
        },
        classname: {

        },
        time: {
            type: Boolean,
            default: false,
        },
        allownull: {
            type: Boolean,
            default: false,
        },
    },
    template: '<input type="text" v-model="val" :class="classname"  class="daterange_input" readonly />',
    data: function () {
        return {
            //  startDate: null,
            //  endDate: null,
            val: '',
        }
    },
    mounted: function () {
        var vm = this;
        /* Then edit */
        if( ! this.value.hasOwnProperty('endDate') && this.value.hasOwnProperty('startDate') ){
            this.value.endDate = this.value.startDate;
        }
        /* end: Then edit */
        if (this.value.hasOwnProperty('startDate')) {
            this.startDate = parseInt(this.value.startDate);
        } else {
            if (this.allownull) {
                this.startDate = null;
            } else {
                this.startDate = typeof vm.compare == 'undefined' ? moment().startOf('month') : moment().subtract(1, 'month').startOf('month');
            }
        }

        if (this.value.hasOwnProperty('endDate')) {
            this.endDate = parseInt(this.value.endDate);
        } else {
            if (this.allownull) {
                this.endDate = null;
            } else {
                this.endDate = typeof vm.compare == 'undefined' ? moment().endOf('month') : moment().subtract(1, 'month').endOf('month');
            }
        }
        if (this.value.hasOwnProperty('endDate') && this.value.hasOwnProperty('startDate')) {
            var string_start = '';
            var string_end = '';
            if (moment(parseInt(this.value.startDate) * 1000).isValid() && moment(parseInt(this.value.endDate) * 1000)) {
                // if( vm.time){
                //    string_start = moment(parseInt(this.value.startDate)*1000).format('HH:mm DD/MM/YYYY');
                //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('HH:mm DD/MM/YYYY');
                // }else{
                //    string_start =  moment(parseInt(this.value.startDate)*1000).format('DD/MM/YYYY');
                //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('DD/MM/YYYY');
                // }
                string_start = moment(parseInt(this.value.startDate) * 1000).format('HH:mm DD/MM/YYYY');
                string_end = moment(parseInt(this.value.endDate) * 1000).format('HH:mm DD/MM/YYYY');
                this.val = string_start.toString() !== string_end.toString() ? (string_start + ' - ' + string_end) : string_start;
            }

        }
        var options = {
            singleDatePicker: true,
            alwaysShowCalendars: true,
            opens: this.open,
            drops: this.drop,
            timePicker: true,
            timePicker24Hour: true,
            autoApply: false,
            autoUpdateInput: false,
            applyButtonClasses: "btn-success",

            locale: {
                format: 'DD/MM/YYYY',
                direction: 'ltr',
                format: moment.localeData().longDateFormat('L'),
                separator: ' - ',
                applyLabel: 'Đồng ý',
                cancelLabel: 'Đóng',
                weekLabel: 'W',
                customRangeLabel: 'Tùy chọn',
                daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
            },
        };
        if (this.startDate != '' && this.startDate != null) {
            options['startDate'] = this.startDate;
        }
        if (this.endDate != '' && this.endDate != null) {
            options['endDate'] = this.endDate;
        }
        $(this.$el).daterangepicker(options);
        $(this.$el).on('apply.daterangepicker', function (ev, picker) {
            var startDate = parseInt(picker.startDate / 1000);
            // var endDate = parseInt(picker.endDate / 1000) + 59;
            var string_start = '';
            // var string_end = '';
            string_start = moment(picker.startDate).format('HH:mm DD/MM/YYYY');
            // string_end = ' - ' + moment(picker.endDate).format('HH:mm DD/MM/YYYY');
            vm.val = string_start ;
            vm.$emit('input', {
                // 'date': parseInt(picker.endDate / 1000),
                'startDate': parseInt(picker.startDate / 1000)
            });
        });
    },
});
Vue.component('daterange', {
    props: {
        value: {

        },
        compare: {

        },
        open: {
            type: String,
            default: 'right',
        },
        start: {

        },
        end: {

        },
        drop: {
            type: String,
            default: 'down',
        },
        classname: {

        },
        time: {
            type: Boolean,
            default: false,
        },
        allownull: {
            type: Boolean,
            default: false,
        },
    },
    template: '<input type="text" v-model="val" :class="classname"  class="daterange_input" readonly />',
    data: function () {
        return {
            //  startDate: null,
            //  endDate: null,
            val: '',
        }
    },
    watch:{
        'value.startDate':function(new_va){
            
            if (this.value.hasOwnProperty('startDate')) {
                this.startDate = parseInt(this.value.startDate);
            } else {
                if (this.allownull) {
                    this.startDate = null;
                } else {
                    this.startDate = typeof vm.compare == 'undefined' ? moment().startOf('month') : moment().subtract(1, 'month').startOf('month');
                }
            }

            if (this.value.hasOwnProperty('endDate')) {
                this.endDate = parseInt(this.value.endDate);
            } else {
                if (this.allownull) {
                    this.endDate = null;
                } else {
                    this.endDate = typeof vm.compare == 'undefined' ? moment().endOf('month') : moment().subtract(1, 'month').endOf('month');
                }
            }
            if (this.value.hasOwnProperty('endDate') && this.value.hasOwnProperty('startDate')) {
                var string_start = '';
                var string_end = '';
                if (moment(parseInt(this.value.startDate) * 1000).isValid() && moment(parseInt(this.value.endDate) * 1000)) {
                    // if( vm.time){
                    //    string_start = moment(parseInt(this.value.startDate)*1000).format('HH:mm DD/MM/YYYY');
                    //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('HH:mm DD/MM/YYYY');
                    // }else{
                    //    string_start =  moment(parseInt(this.value.startDate)*1000).format('DD/MM/YYYY');
                    //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('DD/MM/YYYY');
                    // }
                    string_start = moment(parseInt(this.value.startDate) * 1000).format('DD/MM/YYYY');
                    string_end = ' - ' + moment(parseInt(this.value.endDate) * 1000).format('DD/MM/YYYY');
                    this.val = string_start + string_end;
                }

            }
        }
    },
    mounted: function () {
        var vm = this;
        if (this.value.hasOwnProperty('startDate')) {
            this.startDate = parseInt(this.value.startDate);
        } else {
            if (this.allownull) {
                this.startDate = null;
            } else {
                this.startDate = typeof vm.compare == 'undefined' ? moment().startOf('month') : moment().subtract(1, 'month').startOf('month');
            }
        }

        if (this.value.hasOwnProperty('endDate')) {
            this.endDate = parseInt(this.value.endDate);
        } else {
            if (this.allownull) {
                this.endDate = null;
            } else {
                this.endDate = typeof vm.compare == 'undefined' ? moment().endOf('month') : moment().subtract(1, 'month').endOf('month');
            }
        }
        if (this.value.hasOwnProperty('endDate') && this.value.hasOwnProperty('startDate')) {
            var string_start = '';
            var string_end = '';
            if (moment(parseInt(this.value.startDate) * 1000).isValid() && moment(parseInt(this.value.endDate) * 1000)) {
                // if( vm.time){
                //    string_start = moment(parseInt(this.value.startDate)*1000).format('HH:mm DD/MM/YYYY');
                //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('HH:mm DD/MM/YYYY');
                // }else{
                //    string_start =  moment(parseInt(this.value.startDate)*1000).format('DD/MM/YYYY');
                //    string_end = ' - ' + moment(parseInt(this.value.endDate)*1000).format('DD/MM/YYYY');
                // }
                string_start = moment(parseInt(this.value.startDate) * 1000).format('DD/MM/YYYY');
                string_end = ' - ' + moment(parseInt(this.value.endDate) * 1000).format('DD/MM/YYYY');
                this.val = string_start + string_end;
            }

        }
        var options = {
            alwaysShowCalendars: true,
            opens: this.open,
            drops: this.drop,
            timePicker: false,
            timePicker24Hour: true,
            autoApply: false,
            autoUpdateInput: false,
            applyButtonClasses: "btn-success",

            locale: {
                format: 'DD/MM/YYYY',
                direction: 'ltr',
                format: moment.localeData().longDateFormat('L'),
                separator: ' - ',
                applyLabel: 'Đồng ý',
                cancelLabel: 'Đóng',
                weekLabel: 'W',
                customRangeLabel: 'Tùy chọn',
                daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
            }
        };
        if (this.startDate != '' && this.startDate != null) {
            options['startDate'] = this.startDate;
        }
        if (this.endDate != '' && this.endDate != null) {
            options['endDate'] = this.endDate;
        }
        $(this.$el).daterangepicker(options);
        $(this.$el).on('apply.daterangepicker', function (ev, picker) {
            var startDate = parseInt(picker.startDate / 1000);
            var endDate = parseInt(picker.endDate / 1000) + 59;
            var string_start = '';
            var string_end = '';
            string_start = moment(picker.startDate).format('DD/MM/YYYY');
            string_end = ' - ' + moment(picker.endDate).format('DD/MM/YYYY');
            vm.val = string_start + string_end;
            vm.$emit('input', {
                'endDate': parseInt(picker.endDate / 1000),
                'startDate': parseInt(picker.startDate / 1000)
            });
        });
    },
});
Vue.component('number', {
    props: ['value', 'max', 'min', 'classname', "disabled"],
    template: '<input  @focus="onFocus" type="text" :class="classname" v-model="val" @keyup="onChange" @change="onChange" :disabled="disabled"/>',
    data: function () {
        return {
            val: this.value,
            focused: false,
        }
    },
    mounted: function () {
        if (this.val == '' || this.val == undefined || this.val == 'NaN') {
            if (this.min != undefined) {
                this.val = _.toNumber(this.min);
            } else {
                this.val = 0;
            }
            this.$emit('input', this.val);
            return;
        }
        if (this.min != undefined && parseInt(this.min) != 'NaN') {
            var min = parseInt(this.min.toString().replace(/,/g, ""));
            if (parseInt(String(this.val).replace(/,/g, "")) < min) {
                this.val = min;
            }
        }
        if (this.max != undefined && parseInt(this.max) != 'NaN') {
            var max = parseInt(this.max.toString().replace(/,/g, ""));
            if (parseInt(String(this.val).replace(/,/g, "")) > max) {
                this.val = max;
            }
        }
        this.val = parseInt(String(this.val).replace(/,/g, ""));
        this.val = String(this.val).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.$emit('input', this.val);

    },
    methods: {
        onFocus: function () {
            // this.focused = true;
        },
        onBlur: function () {
            this.$emit('onblurevent');
        },
        onChange: function () {
            // this.focused = false;
            this.$emit('onchangeevent');
        },
    },
    watch: {
        'val': function (newval, oldval) {
            if (newval != oldval) {

                if (this.val == '' || this.val == 'NaN') {
                    if (this.min != undefined) {
                        this.val = this.min;
                    } else {
                        this.val = 0;
                    }
                    this.$emit('input', this.val);
                    this.$emit('onchangeevent');
                    return;
                }
                if (this.min != undefined && parseInt(this.min) != 'NaN') {
                    var min = parseInt(this.min.toString().replace(/,/g, ""));
                    if (parseInt(String(this.val).replace(/,/g, "")) < min) {
                        this.val = min;
                        this.$emit('input', this.val);
                        this.$emit('onchangeevent');
                        return;
                    }
                }
                if (this.max != undefined && parseInt(this.max) != 'NaN') {
                    var max = parseInt(this.max.toString().replace(/,/g, ""));
                    if (parseInt(String(this.val).replace(/,/g, "")) > max) {
                        this.val = max;
                        this.$emit('input', this.val);
                        this.$emit('onchangeevent');
                        return;
                    }
                }
                this.val = parseInt(String(this.val).replace(/,/g, ""));
                this.val = String(this.val).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                this.$emit('input', this.val);
                this.$emit('onchangeevent');
            }
        },
        'value': function (newval, oldval) {
            if (newval != oldval) {
                this.val = newval;
            }
        },
    },
});
Vue.component("number1", {
    template: '<input type="text" :class="classname" :disabled="disabled" @focus="onFocus($event)" @keyup="onBlur" @blur="onChange" v-model="model">',
    props: {
        value: {},
        max: {},
        min : {
            default : 0,
        },
        disabled: {},
        classname: {},
        allow: {
            default: 'int'
        }
    },
    data: function () {
        return {
            // model : 0,
            focused: false,
        }
    },
    mounted : function(){
        // this.model = this.value;
        this.model = this.checkValid(this.value);
    },
    methods:{
        onFocus: function (event) {
            this.focused = true;
            setTimeout(function () {
                event.target.select()
            }, 0)
        },
        onBlur: function () {
            this.$emit('onchangeevent');
        },
        onChange: function () {
            // this.focused = false;
            // this.$emit('onchangeevent');
        },
        isNumber : function(str) {
            var pattern = /^\d+$/;
            return pattern.test(str); 
        },
        toNumber : function(value){
            var string  = String(value).replace(/,/g, "");
            if(this.allow=='float'){
                return string;
            }
            return parseInt(string);
        },
        toNumberFormat : function(value){
            return String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        checkMin : function(value){
            value = this.toNumber(value);
            if( value < this.min ){
                this.model = this.min;
                return false;
            }
            return true;
        },
        checkMax : function(value){
            value =this.toNumber(value);
            if( this.max != undefined && value > this.max ){
                this.model = this.max;
                return false;
            }
            return true;
        }, 
        checkValid : function(newValue){
            if( !this.isNumber(newValue)){
                if( newValue == null ){
                    newValue = this.min ;
                }else{
                    if(this.allow=='float'){
                        newValue = String(newValue).replace(/[^0-9.,]*/g, '');
                    }else{
                        newValue = String(newValue).replace(/[^0-9]/g, '');
                    }
                    if( newValue == ''){
                        newValue = this.min ;
                    }
                }
            }
            if( this.toNumber(newValue) < this.min){
                newValue = this.min ;
            }

            if( this.max != undefined && parseFloat(this.toNumber(newValue)) > parseFloat(this.toNumber(this.max))){
               newValue = this.max ;
            }
            return  this.toNumber(newValue);
        }
    },
    watch : {
        value : function(newval){
            if( newval == null || newval == undefined || newval == 'null' || newval == 'undefined'){
                newval = this.min ;
                this.$el.value = this.toNumberFormat(newval);
                this.$emit("input", this.toNumber(newval));
                return;
            }
            this.checkValid(newval);
        }
    },
    computed : {
        model : {
            get: function(){
                return String(this.value).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            set: function(newValue){
                newValue = this.checkValid(newValue);
                this.$el.value = newValue;
                this.$emit("input", newValue);
            }
        }
    },
    
});
Vue.component('pagination', {
    props: {
        current: {

        },
        total: {

        },
        pageshow: {
            type: Number,
            default: 5,
        }
    },
    template: '\n   <ul class="pagination pagination-split" v-if="total > 1">\n   <li class="page-item first" :class="{disabled : page == 1}"  @click.stop.prevent="first">\n   <a href="#" class="page-link"><i class="fa fa-angle-double-left"></i></a>\n   </li>\n   <li class="page-item prev" :class="{disabled : page == 1}" @click.stop.prevent="prev">\n   <a href="#" class="page-link"><i class="fa fa-angle-left"></i></a>\n   </li>\n   <li class="page-item"\n   v-for="item in total" @click.stop.prevent="setPage(item)"\n   v-if="show(item)"\n   :class=" { active : item == current} "\n   >\n   <a href="#" class="page-link">{{item}}</a>\n   </li>\n   <li class="page-item next" :class="{disabled : page == total}">\n   <a href="#" class="page-link" @click.stop.prevent="next"><i class="fa fa-angle-right"></i></a>\n   </li>\n   <li class="page-item last" :class="{disabled : page == total}" @click.stop.prevent="last">\n   <a href="#" class="page-link"><i class="fa fa-angle-double-right"></i></a>\n   </li>\n   </ul>\n   ',
    mounted: function () {
        var vm = this;
    },
    data: function () {
        return {
            page: (this.current) ? this.current : 1,
        };
    },
    methods: {
        setPage: function (index) {
            this.page = index;
        },
        show: function (index) {
            if (this.current <= 3) {
                if (index <= this.pageshow) {
                    return true;
                } else {
                    return false
                }
            } else if (this.current > this.total - 3) {
                if (index > this.total - this.pageshow) {
                    return true;
                } else {
                    return false
                }
            }
            return Math.abs(index - this.current) < 3;
        },
        prev: function () {
            if (this.page > 1) {
                this.page--;
            }
        },
        next: function () {
            if (this.page < this.total) {
                this.page++;
            }
        },
        first: function () {
            this.page = 1;
        },
        last: function () {
            this.page = this.total;
        }
    },
    watch: {
        'page': function (newval, oldval) {
            if (newval != oldval) {
                this.$emit('input', newval)
            }
        },
        'current': function (newval) {
            if (this.page != newval) {
                this.page = newval;
            }
        }
    }
});
