Vue.component('table-product', {
    props: {
        value: {
            required: true,
        },
    },
    template: '<div class="modal-dialog modal-lg" >\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">×</span>\
                        </button>\
                        <h4 class="modal-title">List product</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="row">\
                            <div class="col-xs-12 col-md-7">\
                                <p>Tìm thấy <strong>{{pagination.total_records | money}}</strong> sản phẩm</p>\
                            </div>\
                            <div class="col-xs-12 col-md-5">\
                                <div class="input-group input-group-sm hidden-xs" >\
                                    <input type="text" class="form-control" v-model="keyword">\
                                    <div class="input-group-btn">\
                                        <button type="button" @click="getProducts()" class="btn btn-default"><i class="fa fa-search"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="table-responsive" style="margin-top: 10px">\
                            <table class="table table-hover table-bordered">\
                                <thead>\
                                    <tr>\
                                        <th width="50" class="text-center" >\
                                            <input type="checkbox" v-model="selectAll">\
                                        </th>\
                                        <th>Mã sản phẩm</th>\
                                        <th>Tên sản phẩm</th>\
                                        <th>Quy cách</th>\
                                        <th>Đơn giá</th>\
                                        <th>Đơn vị</th>\
                                    </tr>\
                                </thead>\
                                <tbody>\
                                    <tr v-for="item,index in products" @click="selectItem(item)" :class="active(item)">\
                                        <td class="text-center">\
                                            <input type="checkbox" v-model="checkBox" :value="getValue(item)">\
                                        </td>\
                                        <td>\
                                            {{item.id}}\
                                        </td>\
                                        <td>{{item.name}}</td>\
                                        <td>{{item.specification}}</td>\
                                        <td>{{item.unit_price}}</td>\
                                        <td>{{ item.MB155 }}</td>\
                                    </tr>\
                                </tbody>\
                            </table>\
                        </div>\
                        <div class="row">\
                            <div class="col-xs-12">\
                                <div class="pull-right">\
                                        <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="modal-footer">\
                        <button type="button" class="btn btn-primary" @click="submit">Add</button>\
                    </div>\
                </div>\
            </div>',
    created: function () {
        
    },
    mounted: function () {
        this.getProducts();
    },

    data: function () {
        return {
            products: [],
            checkBox:[],
            pagination:{
                page:1,
                total:0,
                current:1
            },
            keyword:""
        }
    },
    methods: {
        getProducts: function(){
            var vm = this;
           $.get('/admin/products',{page:this.pagination.page,keyword: this.keyword},function(res){
                vm.products = res.products
                vm.pagination.current = res.page
                vm.pagination.total_records = res.total_records
                vm.pagination.total = res.total
           })
        },
        submit: function(){
            var products = [];
            this.checkBox.forEach(function(item){
                products.push({
                    product_id: item.id,
                    price: item.original_price,
                    original_price : item.original_price,
                    product_name: item.name,
                    quantity : 1,
                    specification : item.specification,
                    unit: item.MB155,
                    date: ''
                })    
            })
            this.$emit('input',products);
            $("#modal-product").modal('hide');
        },
        getValue: function(value){
            return {
                id: value.id,
                name:value.name,
                original_price:value.unit_price,
                specification : value.specification,
                MB155: value.MB155
            }
        },
        selectItem: function(item){
            vm = this;
            var index = _.findIndex( vm.checkBox , {id:item.id});
            if( index == -1){
                vm.checkBox.push(vm.getValue(item));
            }else{
                 vm.checkBox.splice(index, 1);
            }
        },
        active: function(item){
            if(_.findIndex( this.checkBox , {id:item.id}) == -1){
                return '';
            }else{
                return 'success';
            }
        }
    },
    watch: {
        'pagination.page': function(){
            this.getProducts();
        },
        'value' : function(){
            var vm = this;
            vm.checkBox = [];
            this.value.forEach(function(item){
                vm.checkBox.push({
                    id: item.product_id,
                    original_price:item.original_price,
                    name:item.product_name,
                    specification: item.specification,
                    MB155: item.unit
                });
            })
        }
    },
    computed: {
        selectAll: {
            get: function () {
                var vm = this;
                if( this.products.length ){
                    var check = true;
                    this.products.forEach(function(item){
                        if( _.findIndex( vm.checkBox , {id:item.id}) == -1){
                            check = false;
                        }
                    })
                    return check;
                }
                return false;
            },
            set: function (value) {
                var vm = this;
                if (value) {
                    this.products.forEach(function (item) {
                        var it = {
                            id: item.id,
                            name: item.name,
                            original_price:item.unit_price,
                            specification : item.specification,
                            MB155: item.MB155
                        }
                        if( _.findIndex( vm.checkBox , {id:item.id}) == -1){
                            vm.checkBox.push(it);
                        }
                    });
                }else{
                    this.products.forEach(function (item) {
                        var index = _.findIndex( vm.checkBox , {id:item.id});
                        if( index >= 0){
                            vm.checkBox.splice(index, 1);
                        }
                    });
                }
            }
        }
    },
})