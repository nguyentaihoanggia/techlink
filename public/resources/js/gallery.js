Vue.component('gallery', {
    props: {
        value: {
            required: true,
        },
    },
    template: '<div class="modal-dialog modal-lg" role="document">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h5 class="modal-title">Hình ảnh</h5>\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                        </button>\
                    </div>\
                    <div class="modal-body">\
                        <div class="nav-tabs-custom">\
                            <ul class="nav nav-tabs">\
                              <li :class="{active : tab === '+"'image'"+'}" @click="tab = '+"'image'"+'"><a>Hình ảnh</a></li>\
                              <li :class="{active : tab === '+"'upload'"+'}" @click="tab = '+"'upload'"+'"><a >Tải lên</a></li>\
                            </ul>\
                            <div class="tab-content">\
                                <div class="tab-pane" :class="{active : tab === '+"'image'"+'}" >\
                                    <div class="flex">\
                                        <div class="image-box" v-for="image,index in images">\
                                            <a :class="{active: select === image.path}" @click="select = image.path">\
                                                <img :src="image.path" >\
                                            </a>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-xs-12">\
                                            <div class="pull-right">\
                                                    <pagination v-model="pagination.page" :total="pagination.total" :current="pagination.current"></pagination>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="tab-pane"  :class="{active : tab === '+"'upload'"+'}"  >\
                                    <input type="file" @change="onChangeFile($event)" hidden id="modal_input_file"  accept="image/*">\
                                    <label for="modal_input_file" class="choose-file" v-if="!upload.data.length">\
                                        Choose Files\
                                    </label>\
                                    <div class="list-item-image" v-else>\
                                        <div class="item-image" v-for="(item,index) in upload.data" >\
                                            <img :src="item.src" alt="">\
                                            <a @click="removeFile(index)" class="item-image-remove"><i class="fa fa-times-circle"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="modal-footer" v-if="tab === '+"'image'"+'" >\
                        <button type="button" class="btn btn-primary" @click="submit">Save changes</button>\
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>\
                    </div>\
                     <div class="modal-footer" v-else >\
                        <button type="button" class="btn btn-primary" @click="uploadImage" :disabled="upload.is_loading">Upload</button>\
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>\
                    </div>\
                </div>\
            </div>',
    created: function () {
        
    },
    mounted: function () {
        this.getImages();
    },

    data: function () {
        return {
            tab: 'image',
            images: [],
            checkBox:[],
            pagination:{
                page:1,
                total:0,
                current:1
            },
            upload:{
                show :false,
                data : [],
                group_id : '',
                is_loading : false,
                formstate: false
            },
            select:''
        }
    },
    methods: {
        onChangeFile: function($event){
            if(this.isLoading) return
            var files = $event.target.files || $event.dataTransfer.files;
            this.getBase64({ files : files , config : this.config}).then((res)=>{
                if( res  && res.length){
                    this.upload.data = this.upload.data.concat(res);
                }
            })
        },
        getBase64 : async function({  files , config  } ){
            var arr = [] 
            function readFileData (file) {
                return new Promise((resolve , reject )=>{
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = new Image(); 
                        img.src = e.target.result;
                        img.onload = function() {
                            arr.push({
                                src : e.target.result,
                                name: name,
                                size: file.size,
                                type: file.name.replace(/^.*\./, '') ,
                                file : file
                            })
                            resolve()
                        }
                    };
                    reader.onerror = reject;
                    reader.readAsDataURL(file);
                })
            }
            if (files.length) {
                const promisesToAwait = [];
                for (let i = 0; i < files.length; i++) {
                    promisesToAwait.push(readFileData(files[i]));
                }
                const responses = await Promise.all(promisesToAwait);
            };
            
            
            return new Promise((resolve)=>{
                resolve(arr)
            })
        },
        removeFile: function(index){
            this.upload.data.splice(index,1);
        },
        uploadImage: function(){
            if( this.upload.is_loading == false){
                this.upload.is_loading = true
                var formdata = new FormData;
                for (let index = 0; index < this.upload.data.length; index++) {
                    formdata.append('images[]' ,this.upload.data[index].file);
                }
                var vm = this;
                $.ajax({
                    url: '/admin/images',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    success: function (res) {
                        vm.upload.is_loading = false
                        if(res.success){
                            helper.showNotification('Upload successfully',"success");
                            vm.upload.data = [];
                            if(vm.pagination.page == 1){
                                vm.getImages();
                            }else{
                                vm.pagination.page = 1;
                            }
                            
                            vm.tab = 'image';
                        }else{
                            helper.showNotification(res.message,"danger")
                        }
                    }
                });
            }
        },
        getImages: function(){
            var vm = this;
           $.get('/admin/images',{page:this.pagination.page},function(res){
                vm.images = res.data.data
                vm.pagination.current = res.data.current_page
                vm.pagination.total = res.data.last_page
           })
        },
        submit:function(){
            if(this.select == '') {
                helper.showNotification("Chưa có hình ảnh nào được chọn","danger")
                return
            }
            this.$emit('input', this.select)
            this.select = '';
            $("#modal-gallery").modal('hide');
        }
    },
    watch: {
        'pagination.page': function(){
            this.getImages();
        }
    },
})