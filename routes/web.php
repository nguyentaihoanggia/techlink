<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to(route('admin'));
});

Auth::routes();

Route::group(['prefix' => 'admin','middleware' => 'auth'], function (){
    Route::get('/','Admin\AdminController@index')->name('admin');
    Route::post('/updateData','Admin\AdminController@updateData')->name('updateData');
    Route::resource('user','Admin\UserController');
    Route::post('user/restore/{id}', 'Admin\UserController@restore')->name('user.restore');
    Route::get('order-request/search','Admin\OrderRequestController@search')->name('order-request.search');
    Route::resource('order-request','Admin\OrderRequestController');
    Route::get('order-request/{id}/detail','Admin\OrderRequestController@detail')->name('order-request.detail');


    Route::get('products','Admin\ProductController@index')->name('products.index');
    Route::get('suppliers','Admin\SupplierController@index')->name('suppliers.index');
    Route::get('order-request/{id}/approval','Admin\OrderRequestController@approval')->name('order-request.approval');
    Route::patch('order-request/{id}/approval','Admin\OrderRequestController@updateApproval');
    Route::get('order-request/{id}/supplier','Admin\OrderRequestController@supplier')->name('order-request.supplier');
    Route::post('order-request/{id}/supplier','Admin\OrderRequestController@addSupplier')->name('order-request.supplier-add');
    Route::get('order-request/{id}/supplier/edit','Admin\OrderRequestController@editSupplier')->name('order-request.supplier-edit');
    Route::post('order-request/{id}/supplier/edit','Admin\OrderRequestController@updateSupplier')->name('order-request.supplier-update');
    Route::get('order-request/{id}/supplier/detail','Admin\OrderRequestController@detailSupplier')->name('order-request.supplier-detail');

    Route::get('order-request/{id}/approval-supplier','Admin\OrderRequestController@approvalSupplier')->name('order-request.approval-supplier');
    Route::patch('order-request/{id}/approval-supplier','Admin\OrderRequestController@updateApprovalSupplier');

    Route::get('order-request/{id}/order','Admin\OrderRequestController@order')->name('order-request.order');
    Route::post('order-request/{id}/order-create','Admin\OrderRequestController@orderCreate')->name('order-request.orderCreate');
    Route::get('order-request/{id}/order/{order_id}/edit','Admin\OrderRequestController@editOrder')->name('order-request.order-edit');
    Route::get('order-request/{id}/order/{order_id}/detail','Admin\OrderRequestController@detailOrder')->name('order-request.order-detail');
    Route::post('order-request/{id}/order/{order_id}','Admin\OrderRequestController@updateOrder')->name('order-request.order-update');
    Route::get('order-request/{id}/order-approver/{order_id}','Admin\OrderRequestController@orderApproval')->name('order-request.approval-order');
    Route::patch('order-request/{id}/order-approver/{order_id}','Admin\OrderRequestController@updateOrderApproval');

    Route::get('order-request/{id}/print','Admin\OrderRequestController@printOrder')->name('order-request.order-print');

     Route::get('order-request/{id}/accountant','Admin\AccountantController@accountant')->name('order-request.accountant');
     Route::get('accountant','Admin\AccountantController@index')->name('order-request.accountant_index');
     Route::post('accountant/getOrderRequestAccountant','Admin\AccountantController@getOrderRequestAccountant')->name('order-request.getOrderRequestAccountant');
     Route::post('accountant/actionAccountant','Admin\AccountantController@action')->name('order-request.actionAccountant');
     Route::patch('order-request/{id}/approval-accountant','Admin\AccountantController@approval')->name('order-request.approval-accountant');

    Route::get('order-request/{id}/approval-advance','Admin\AdvanceController@advance')->name('order-request.approval-advance');
    Route::patch('order-request/{id}/approval-advance','Admin\AdvanceController@approval')->name('order-request.approval-submit-advance');

     Route::post('order-request/{id}/selectAdvanceUser','Admin\OrderRequestController@selectAdvanceUser')->name('order-request.selectAdvanceUser');
    Route::get('order-request/{id}/advance','Admin\AccountantController@accountant')->name('order-request.accountant');


    Route::resource('approval','Admin\ApprovalController');
    Route::post('approval/actionApprover','Admin\ApprovalController@actionApprover');

    Route::resource('images','Admin\ImageController');

    Route::post('/order-request/getOrderRequest', 'Admin\OrderRequestController@getOrderRequest');
    Route::post('/approval/getOrderRequestApproval', 'Admin\ApprovalController@getOrderRequestApproval');

    Route::get('notify', 'Admin\NotifyController@index')->name('notify.index');

    Route::post('supplier/history','Admin\SupplierController@history')->name('supplier.history');
    Route::post('product/history','Admin\ProductController@history')->name('product.history');
});
