<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_request_details', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('original_price');
            $table->dropColumn('inventory');
            $table->dropColumn('quantity');
            $table->dropColumn('unit');
            $table->dropColumn('specification');
            $table->dropColumn('product_name');
            $table->integer('expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_request_details', function (Blueprint $table) {
            $table->float('price');
            $table->float('original_price')->default(0);
            $table->float('inventory')->default(0);
            $table->integer('quantity');
            $table->string('unit')->nullable();
            $table->string('specification')->nullable();
            $table->string('product_name')->nullable();
            $table->dropColumn('expiry_date');
        });
    }
}
