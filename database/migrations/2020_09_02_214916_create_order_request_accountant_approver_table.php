<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequestAccountantApproverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_request_accountant_approver', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_request_id');
            $table->integer('user_id');
            $table->string('reason')->nullable();
            $table->string('status');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_request_accountant_approver');
    }
}
