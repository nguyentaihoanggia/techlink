<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_requests', function (Blueprint $table) {
            $table->dropColumn('total_cost');
            $table->dropColumn('request_department');
            $table->dropColumn('request_code');
            $table->dropColumn('use_department');
            $table->dropColumn('use_code');
            $table->dropColumn('request_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_requests', function (Blueprint $table) {
            $table->float('total_cost');
            $table->string('request_department')->nullable();
            $table->string('request_code')->nullable();
            $table->string('use_department')->nullable();
            $table->string('use_code')->nullable();
            $table->string('request_date')->nullable();
        });
    }
}
