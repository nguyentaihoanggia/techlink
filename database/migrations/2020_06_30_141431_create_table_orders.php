<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_request_id');
            $table->integer('supplier_id');
            $table->string('code');
            $table->string('supplier_code')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('country')->nullable();
            $table->string('area')->nullable();
            $table->string('phone1')->nullable();
            $table->string('address1')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->float('total_cost')->nullable();
            $table->string('status')->nullable();
            $table->integer('create_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
