<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_request_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_request_id');
            $table->string('product_id');
            $table->string('path')->nullable();
            $table->float('price');
            $table->float('original_price')->default(0);
            $table->float('inventory')->default(0);
            $table->integer('quantity');
            $table->string('unit')->nullable();
            $table->string('specification')->nullable();
            $table->string('product_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_request_details');
    }
}
