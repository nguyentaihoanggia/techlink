<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->float('total_cost');
            $table->string('request_department')->nullable();
            $table->string('request_code')->nullable();
            $table->string('use_department')->nullable();
            $table->string('use_code')->nullable();
            $table->string('request_date')->nullable();
            $table->string('note')->nullable();
            $table->string('reason')->nullable();
            $table->string('address_delivery')->nullable();
            $table->integer('create_user_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requests');
    }
}
