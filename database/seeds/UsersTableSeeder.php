<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'email' => 'admin@gmail.com',
            "name" => "Administrator",
            'password' => bcrypt('123123'),
            'role_id' => 1
        ]);
        $faker = \Faker\Factory::create();
        for($i = 0 ; $i < 10;$i++){
            \App\Models\User::create([
                'email' => $faker->email,
                "name" => $faker->firstName,
                'password' => bcrypt('123123'),
                'role_id' => rand(1,3)
            ]);
        }
    }
}
