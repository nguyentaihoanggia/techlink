<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Image::truncate();
        \App\Models\OrderRequest::truncate();
        \App\Models\OrderRequestDetail::truncate();
        \App\Models\OrderRequestNotify::truncate();
        \App\Models\OrderRequestApprover::truncate();
        \App\Models\OrderRequestSupplier::truncate();
        \App\Models\ORSupplierDeatil::truncate();
        \App\Models\Order::truncate();
        \App\Models\OrderDetail::truncate();
        \App\Models\OrderRequestAccountant::truncate();
         $this->call(UsersTableSeeder::class);
    }
}
